/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.io.InputStream;
import java.sql.Date;

/**
 *
 * @author Admin
 */
public class Schedule {

    private int ScheduleID;
    private int PaymentID;
    private int StudentID;
    private int TutorID;
    private Date Start;
    private int NumberOfSlot;
    private int SubjectID;
    private int SlotID;
    private Date TimeOfSlot;
    private String Location;
    private InputStream Attachment;
    private boolean Attend;

    public Schedule() {
    }

    public Schedule(int ScheduleID, int PaymentID, int StudentID, int TutorID, Date Start, int NumberOfSlot, int SubjectID, int SlotID, Date TimeOfSlot, String Location, InputStream Attachment, boolean Attend) {
        this.ScheduleID = ScheduleID;
        this.PaymentID = PaymentID;
        this.StudentID = StudentID;
        this.TutorID = TutorID;
        this.Start = Start;
        this.NumberOfSlot = NumberOfSlot;
        this.SubjectID = SubjectID;
        this.SlotID = SlotID;
        this.TimeOfSlot = TimeOfSlot;
        this.Location = Location;
        this.Attachment = Attachment;
        this.Attend = Attend;
    }
    

    public int getScheduleID() {
        return ScheduleID;
    }

    public void setScheduleID(int ScheduleID) {
        this.ScheduleID = ScheduleID;
    }

    public int getPaymentID() {
        return PaymentID;
    }

    public void setPaymentID(int PaymentID) {
        this.PaymentID = PaymentID;
    }

    public int getStudentID() {
        return StudentID;
    }

    public void setStudentID(int StudentID) {
        this.StudentID = StudentID;
    }

    public int getTutorID() {
        return TutorID;
    }

    public void setTutorID(int TutorID) {
        this.TutorID = TutorID;
    }

    public int getNumberOfSlot() {
        return NumberOfSlot;
    }

    public void setNumberOfSlot(int NumberOfSlot) {
        this.NumberOfSlot = NumberOfSlot;
    }

    public int getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(int SubjectID) {
        this.SubjectID = SubjectID;
    }

    public int getSlotID() {
        return SlotID;
    }

    public void setSlotID(int SlotID) {
        this.SlotID = SlotID;
    }

    public Date getTimeOfSlot() {
        return TimeOfSlot;
    }

    public void setTimeOfSlot(Date TimeOfSlot) {
        this.TimeOfSlot = TimeOfSlot;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String Location) {
        this.Location = Location;
    }

    public InputStream getAttachment() {
        return Attachment;
    }

    public void setAttachment(InputStream Attachment) {
        this.Attachment = Attachment;
    }

    public boolean isAttend() {
        return Attend;
    }

    public void setAttend(boolean Attend) {
        this.Attend = Attend;
    }

    public Date getStart() {
        return Start;
    }

    public void setStart(Date Start) {
        this.Start = Start;
    }

    @Override
    public String toString() {
        return "Schedule{" + "ScheduleID=" + ScheduleID + ", PaymentID=" + PaymentID + ", StudentID=" + StudentID + ", TutorID=" + TutorID + ", Start=" + Start + ", NumberOfSlot=" + NumberOfSlot + ", SubjectID=" + SubjectID + ", SlotID=" + SlotID + ", TimeOfSlot=" + TimeOfSlot + ", Location=" + Location + ", Attachment=" + Attachment + ", Attend=" + Attend + '}';
    }

    
    

}
