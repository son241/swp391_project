/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.io.InputStream;

/**
 *
 * @author Admin
 */
public class File {
    private int FileID;
    private InputStream FileData;

    public File(int FileID, InputStream FileData) {
        this.FileID = FileID;
        this.FileData = FileData;
    }

    public int getFileID() {
        return FileID;
    }

    public void setFileID(int FileID) {
        this.FileID = FileID;
    }

    public InputStream getFileData() {
        return FileData;
    }

    public void setFileData(InputStream FileData) {
        this.FileData = FileData;
    }
    
    
}
