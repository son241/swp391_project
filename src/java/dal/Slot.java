/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

/**
 *
 * @author quang
 */
public class Slot {
    private int SlotID;
    private String SlotName;

    public Slot() {
    }

    public Slot(int SlotID, String SlotName) {
        this.SlotID = SlotID;
        this.SlotName = SlotName;
    }

    public int getSlotID() {
        return SlotID;
    }

    public void setSlotID(int SlotID) {
        this.SlotID = SlotID;
    }

    public String getSlotName() {
        return SlotName;
    }

    public void setSlotName(String SlotName) {
        this.SlotName = SlotName;
    }

    @Override
    public String toString() {
        return "Slot{" + "SlotID=" + SlotID + ", SlotName=" + SlotName + '}';
    }
    
}
