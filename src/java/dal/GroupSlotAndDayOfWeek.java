/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Date;

/**
 *
 * @author quang
 */
public class GroupSlotAndDayOfWeek {
    private int SlotID;
    private String DayOfWeek;
    private Date TimeOfSlot;

    public GroupSlotAndDayOfWeek(int SlotID, String DayOfWeek) {
        this.SlotID = SlotID;
        this.DayOfWeek = DayOfWeek;
    }

    public GroupSlotAndDayOfWeek(int SlotID, String DayOfWeek, Date TimeOfSlot) {
        this.SlotID = SlotID;
        this.DayOfWeek = DayOfWeek;
        this.TimeOfSlot = TimeOfSlot;
    }
    

    public GroupSlotAndDayOfWeek() {
    }

    public int getSlotID() {
        return SlotID;
    }

    public void setSlotID(int SlotID) {
        this.SlotID = SlotID;
    }

    public String getDayOfWeek() {
        return DayOfWeek;
    }

    public void setDayOfWeek(String DayOfWeek) {
        this.DayOfWeek = DayOfWeek;
    }

    public Date getTimeOfSlot() {
        return TimeOfSlot;
    }

    public void setTimeOfSlot(Date TimeOfSlot) {
        this.TimeOfSlot = TimeOfSlot;
    }

    @Override
    public String toString() {
        return "GroupSlotAndDayOfWeek{" + "SlotID=" + SlotID + ", DayOfWeek=" + DayOfWeek + ", TimeOfSlot=" + TimeOfSlot + '}';
    }
    
    
}
