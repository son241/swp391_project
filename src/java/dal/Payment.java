/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Date;

/**
 *
 * @author Admin
 */
public class Payment {
    private int PaymentID;
    private int AccountID;
    private int StudentID;
    private Date PaymentDate;
    private int SubjectID;
    private int TutorID;
    private int NumberOfSlot;
    private boolean Comfirmation;

    public Payment() {
    }

    public Payment(int PaymentID, int AccountID, int StudentID, Date PaymentDate, int SubjectID, int TutorID, int NumberOfSlot, boolean Comfirmation) {
        this.PaymentID = PaymentID;
        this.AccountID = AccountID;
        this.StudentID = StudentID;
        this.PaymentDate = PaymentDate;
        this.SubjectID = SubjectID;
        this.TutorID = TutorID;
        this.NumberOfSlot = NumberOfSlot;
        this.Comfirmation = Comfirmation;
    }

    public int getPaymentID() {
        return PaymentID;
    }

    public void setPaymentID(int PaymentID) {
        this.PaymentID = PaymentID;
    }

    public int getAccountID() {
        return AccountID;
    }

    public void setAccountID(int AccountID) {
        this.AccountID = AccountID;
    }

    public int getStudentID() {
        return StudentID;
    }

    public void setStudentID(int StudentID) {
        this.StudentID = StudentID;
    }

    public Date getPaymentDate() {
        return PaymentDate;
    }

    public void setPaymentDate(Date PaymentDate) {
        this.PaymentDate = PaymentDate;
    }

    public int getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(int SubjectID) {
        this.SubjectID = SubjectID;
    }

    public int getTutorID() {
        return TutorID;
    }

    public void setTutorID(int TutorID) {
        this.TutorID = TutorID;
    }

    public int getNumberOfSlot() {
        return NumberOfSlot;
    }

    public void setNumberOfSlot(int NumberOfSlot) {
        this.NumberOfSlot = NumberOfSlot;
    }

    public boolean isComfirmation() {
        return Comfirmation;
    }

    public void setComfirmation(boolean Comfirmation) {
        this.Comfirmation = Comfirmation;
    }
    
    
}
