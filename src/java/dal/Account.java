/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Date;

/**
 *
 * @author Admin
 */
public class Account {
    private int AccountID;
    private String Username;
    private String Password;
    private int RoleID;
    private int TutorID;
    private int ParentID;
    private int StudentID;
    private Date CreationDate;
    private boolean Active;

    public Account() {
    }

    public Account(int AccountID, String Username, String Password, int RoleID, int TutorID, int ParentID, int StudentID, Date CreationDate, boolean Active) {
        this.AccountID = AccountID;
        this.Username = Username;
        this.Password = Password;
        this.RoleID = RoleID;
        this.TutorID = TutorID;
        this.ParentID = ParentID;
        this.StudentID = StudentID;
        this.CreationDate = CreationDate;
        this.Active = Active;
    }

    public int getAccountID() {
        return AccountID;
    }

    public void setAccountID(int AccountID) {
        this.AccountID = AccountID;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String Username) {
        this.Username = Username;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }

    public int getRoleID() {
        return RoleID;
    }

    public void setRoleID(int RoleID) {
        this.RoleID = RoleID;
    }

    public int getTutorID() {
        return TutorID;
    }

    public void setTutorID(int TutorID) {
        this.TutorID = TutorID;
    }

    public int getParentID() {
        return ParentID;
    }

    public void setParentID(int ParentID) {
        this.ParentID = ParentID;
    }

    public int getStudentID() {
        return StudentID;
    }

    public void setStudentID(int StudentID) {
        this.StudentID = StudentID;
    }

    public Date getCreationDate() {
        return CreationDate;
    }

    public void setCreationDate(Date CreationDate) {
        this.CreationDate = CreationDate;
    }

    public boolean isActive() {
        return Active;
    }

    public void setActive(boolean Active) {
        this.Active = Active;
    }
    
}
