/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Date;

/**
 *
 * @author Admin
 */
public class Student {
    private int StudentID;
    private int ParentID;
    private String FullName;
    private String Gender;
    private String Address;
    private Date BirthDate;
    private String Email;
    private String PhoneNumber;

    public Student() {
    }

    public Student(int StudentID, int ParentID, String FullName, String Gender, String Address, Date BirthDate, String Email, String PhoneNumber) {
        this.StudentID = StudentID;
        this.ParentID = ParentID;
        this.FullName = FullName;
        this.Gender = Gender;
        this.Address = Address;
        this.BirthDate = BirthDate;
        this.Email = Email;
        this.PhoneNumber = PhoneNumber;
    }

    public int getStudentID() {
        return StudentID;
    }

    public void setStudentID(int StudentID) {
        this.StudentID = StudentID;
    }

    public int getParentID() {
        return ParentID;
    }

    public void setParentID(int ParentID) {
        this.ParentID = ParentID;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String FullName) {
        this.FullName = FullName;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String Gender) {
        this.Gender = Gender;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public Date getBirthDate() {
        return BirthDate;
    }

    public void setBirthDate(Date BirthDate) {
        this.BirthDate = BirthDate;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String PhoneNumber) {
        this.PhoneNumber = PhoneNumber;
    }

    @Override
    public String toString() {
        return "Student{" + "StudentID=" + StudentID + ", ParentID=" + ParentID + ", FullName=" + FullName + ", Gender=" + Gender + ", Address=" + Address + ", BirthDate=" + BirthDate + ", Email=" + Email + ", PhoneNumber=" + PhoneNumber + '}';
    }
    
    
}
