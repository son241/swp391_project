/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

/**
 *
 * @author Admin
 */
public class Feedback {
    private int FeedbackID;
    private int StudentID;
    private int TutorID;
    private int Rate;
    private String Comment;
    private int ScheduleID;
    private int FeedbackRole;

    public Feedback() {
    }

    public Feedback(int FeedbackID, int StudentID, int TutorID, int Rate, String Comment, int ScheduleID, int FeedbackRole) {
        this.FeedbackID = FeedbackID;
        this.StudentID = StudentID;
        this.TutorID = TutorID;
        this.Rate = Rate;
        this.Comment = Comment;
        this.ScheduleID = ScheduleID;
        this.FeedbackRole = FeedbackRole;
    }

    public int getFeedbackID() {
        return FeedbackID;
    }

    public void setFeedbackID(int FeedbackID) {
        this.FeedbackID = FeedbackID;
    }

    public int getStudentID() {
        return StudentID;
    }

    public void setStudentID(int StudentID) {
        this.StudentID = StudentID;
    }

    public int getTutorID() {
        return TutorID;
    }

    public void setTutorID(int TutorID) {
        this.TutorID = TutorID;
    }

    public int getRate() {
        return Rate;
    }

    public void setRate(int Rate) {
        this.Rate = Rate;
    }

    public String getComment() {
        return Comment;
    }

    public void setComment(String Comment) {
        this.Comment = Comment;
    }

    public int getScheduleID() {
        return ScheduleID;
    }

    public void setScheduleID(int ScheduleID) {
        this.ScheduleID = ScheduleID;
    }

    public int getFeedbackRole() {
        return FeedbackRole;
    }

    public void setFeedbackRole(int FeedbackRole) {
        this.FeedbackRole = FeedbackRole;
    }
    
    
}
