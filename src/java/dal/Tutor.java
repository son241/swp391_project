/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.io.InputStream;
import java.sql.Blob;
import java.sql.Date;

/**
 *
 * @author Admin
 */
public class Tutor {
    private int TutorID;
    private String FullName;
    private String Gender;
    private String Address;
    private Date BirthDate;
    private String Email ;
    private String PhoneNumber;
    private int ProfileImage;
    private int CVFile;
    private String Description;
    private int SubjectID;
    private int LevelID;

    public Tutor() {
    }

    public Tutor(int TutorID, String FullName, String Gender, String Address, Date BirthDate, String Email, String PhoneNumber, int ProfileImage, int CVFile, String Description, int SubjectID, int LevelID) {
        this.TutorID = TutorID;
        this.FullName = FullName;
        this.Gender = Gender;
        this.Address = Address;
        this.BirthDate = BirthDate;
        this.Email = Email;
        this.PhoneNumber = PhoneNumber;
        this.ProfileImage = ProfileImage;
        this.CVFile = CVFile;
        this.Description = Description;
        this.SubjectID = SubjectID;
        this.LevelID = LevelID;
    }

    public int getTutorID() {
        return TutorID;
    }

    public void setTutorID(int TutorID) {
        this.TutorID = TutorID;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String FullName) {
        this.FullName = FullName;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String Gender) {
        this.Gender = Gender;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public Date getBirthDate() {
        return BirthDate;
    }

    public void setBirthDate(Date BirthDate) {
        this.BirthDate = BirthDate;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String PhoneNumber) {
        this.PhoneNumber = PhoneNumber;
    }

    public int getProfileImage() {
        return ProfileImage;
    }

    public void setProfileImage(int ProfileImage) {
        this.ProfileImage = ProfileImage;
    }

    public int getCVFile() {
        return CVFile;
    }

    public void setCVFile(int CVFile) {
        this.CVFile = CVFile;
    }

    
    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public int getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(int SubjectID) {
        this.SubjectID = SubjectID;
    }

    public int getLevelID() {
        return LevelID;
    }

    public void setLevelID(int LevelID) {
        this.LevelID = LevelID;
    }

    @Override
    public String toString() {
        return "Tutor{" + "TutorID=" + TutorID + ", FullName=" + FullName + ", Gender=" + Gender + ", Address=" + Address + ", BirthDate=" + BirthDate + ", Email=" + Email + ", PhoneNumber=" + PhoneNumber + ", ProfileImage=" + ProfileImage + ", CVFile=" + CVFile + ", Description=" + Description + ", SubjectID=" + SubjectID + ", LevelID=" + LevelID + '}';
    }

    
    
    
}
