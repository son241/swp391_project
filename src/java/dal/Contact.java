/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Date;

/**
 *
 * @author Admin
 */
public class Contact {
    private int ContactID;
    private String GuestName;
    private String GuestEmail;
    private String GuestSubject;
    private String Message;
    private Date ContcactDate;

    public Contact(int ContactID, String GuestName, String GuestEmail, String GuestSubject, String Message) {
        this.ContactID = ContactID;
        this.GuestName = GuestName;
        this.GuestEmail = GuestEmail;
        this.GuestSubject = GuestSubject;
        this.Message = Message;
    }

    public Contact() {
    }

    public int getContactID() {
        return ContactID;
    }

    public void setContactID(int ContactID) {
        this.ContactID = ContactID;
    }

    public String getGuestName() {
        return GuestName;
    }

    public void setGuestName(String GuestName) {
        this.GuestName = GuestName;
    }

    public String getGuestEmail() {
        return GuestEmail;
    }

    public void setGuestEmail(String GuestEmail) {
        this.GuestEmail = GuestEmail;
    }

    public String getGuestSubject() {
        return GuestSubject;
    }

    public void setGuestSubject(String GuestSubject) {
        this.GuestSubject = GuestSubject;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public Contact(int ContactID, String GuestName, String GuestEmail, String GuestSubject, String Message, Date ContcactDate) {
        this.ContactID = ContactID;
        this.GuestName = GuestName;
        this.GuestEmail = GuestEmail;
        this.GuestSubject = GuestSubject;
        this.Message = Message;
        this.ContcactDate = ContcactDate;
    }

    public Date getContcactDate() {
        return ContcactDate;
    }

    public void setContcactDate(Date ContcactDate) {
        this.ContcactDate = ContcactDate;
    }
    
    
}
