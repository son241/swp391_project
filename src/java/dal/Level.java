/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

/**
 *
 * @author Admin
 */
public class Level {
    private int LevelID;
    private String LevelName;

    public Level() {
    }

    public Level(int LevelID, String LevelName) {
        this.LevelID = LevelID;
        this.LevelName = LevelName;
    }

    public int getLevelID() {
        return LevelID;
    }

    public void setLevelID(int LevelID) {
        this.LevelID = LevelID;
    }

    public String getLevelName() {
        return LevelName;
    }

    public void setLevelName(String LevelName) {
        this.LevelName = LevelName;
    }

    @Override
    public String toString() {
        return "Level{" + "LevelID=" + LevelID + ", LevelName=" + LevelName + '}';
    }
    
    
}
