/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Date;

/**
 *
 * @author Admin
 */
public class BlogDiscussion {
    private int CommentID;
    private int BlogID;
    private int AccountID;
    private String Comment;
    private Date CommentDate;

    public BlogDiscussion() {
    }

    public BlogDiscussion(int CommentID, int BlogID, int AccountID, String Comment, Date CommentDate) {
        this.CommentID = CommentID;
        this.BlogID = BlogID;
        this.AccountID = AccountID;
        this.Comment = Comment;
        this.CommentDate = CommentDate;
    }

    public int getCommentID() {
        return CommentID;
    }

    public void setCommentID(int CommentID) {
        this.CommentID = CommentID;
    }

    public int getBlogID() {
        return BlogID;
    }

    public void setBlogID(int BlogID) {
        this.BlogID = BlogID;
    }

    public int getAccountID() {
        return AccountID;
    }

    public void setAccountID(int AccountID) {
        this.AccountID = AccountID;
    }

    public String getComment() {
        return Comment;
    }

    public void setComment(String Comment) {
        this.Comment = Comment;
    }

    public Date getCommentDate() {
        return CommentDate;
    }

    public void setCommentDate(Date CommentDate) {
        this.CommentDate = CommentDate;
    }
    
    
}
