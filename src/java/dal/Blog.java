/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.io.InputStream;

/**
 *
 * @author Admin
 */
public class Blog {
    private int BlogID;
    private String BlogTitle;
    private String BlogDesciption;
    private int CategoryID;
    private int Image;
    public Blog() {
        
    }

    public Blog(int BlogID, String BlogTitle, String BlogDesciption, int CategoryID, int Image) {
        this.BlogID = BlogID;
        this.BlogTitle = BlogTitle;
        this.BlogDesciption = BlogDesciption;
        this.CategoryID = CategoryID;
        this.Image = Image;
    }

    
    
    public Blog(int BlogID, String BlogTitle, String BlogDesciption, int CategoryID) {
        this.BlogID = BlogID;
        this.BlogTitle = BlogTitle;
        this.BlogDesciption = BlogDesciption;
        this.CategoryID = CategoryID;
    }

    public int getImage() {
        return Image;
    }

    public void setImage(int Image) {
        this.Image = Image;
    }

    public int getBlogID() {
        return BlogID;
    }

    public void setBlogID(int BlogID) {
        this.BlogID = BlogID;
    }

    public String getBlogTitle() {
        return BlogTitle;
    }

    public void setBlogTitle(String BlogTitle) {
        this.BlogTitle = BlogTitle;
    }

    public String getBlogDesciption() {
        return BlogDesciption;
    }

    public void setBlogDesciption(String BlogDesciption) {
        this.BlogDesciption = BlogDesciption;
    }

    public int getCategoryID() {
        return CategoryID;
    }

    public void setCategoryID(int CategoryID) {
        this.CategoryID = CategoryID;
    }
    
    
}
