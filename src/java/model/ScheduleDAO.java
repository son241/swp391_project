/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import dal.*;
import java.sql.Date;
import java.sql.PreparedStatement;

/**
 *
 * @author quang
 */
public class ScheduleDAO extends DBContext {

    public int SetSchedule(Schedule S) {
        int result = 0;
        try {
            String sql = "Insert into Schedule(StudentID, TutorID, Start, NumberOfSlot, SubjectID, SlotID, TimeOfSlot)\n"
                    + "Values(?,?,?,?,?,?,?)";
            PreparedStatement ps =connection.prepareStatement(sql);
            ps.setInt(1, S.getStudentID());
            ps.setInt(2, S.getStudentID());
            ps.setDate(3, S.getStart());
            ps.setInt(4, S.getNumberOfSlot());
            ps.setInt(5, S.getSubjectID());
            ps.setInt(6, S.getSlotID());
            ps.setDate(7, S.getTimeOfSlot());
            result = ps.executeUpdate();
        } catch (Exception e) {
        }
        return result;
    }
    public static void main(String[] args) {
        Schedule S = new Schedule(0, 0, 1, 1, Date.valueOf("2023-02-08"), 30, 1, 1, Date.valueOf("2023-02-08"), null, null, true);
        System.out.println(new ScheduleDAO().SetSchedule(S));
    }
}
