/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;
import dal.Contact;
import dal.DBContext;
import java.sql.Date;
import java.sql.PreparedStatement;

/**
 *
 * @author Admin
 */
public class ContactDAO extends DBContext{
       public int sendContact(Contact C) {
        int result = 0;
        try {
            String sql = "insert into contact(GuestName, GuestEmail, GuestSubject, Message, Date)\n"
                    + "values(?,?,?,?,?)";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, C.getGuestName());
            ps.setString(2, C.getGuestEmail());
            ps.setString(3, C.getGuestSubject());
            ps.setString(4, C.getMessage());
            ps.setDate(5, C.getContcactDate());
            result =  ps.executeUpdate();
        } catch (Exception e) {
        }
        return result;
    }
}
