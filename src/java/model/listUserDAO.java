/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import dal.Account;
import dal.DBContext;
import dal.Parent;
import dal.Role;
import dal.Student;
import dal.Tutor;
import jakarta.servlet.http.HttpServlet;
import java.sql.Blob;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class listUserDAO extends DBContext {

    public ArrayList<Account> getAccountList() {
        ArrayList<Account> list = new ArrayList<Account>();
        try {
            String sql = "select * from Account";

            //Buoc 2: Tạo đối tượng PrepareStatement
            PreparedStatement ps = connection.prepareStatement(sql);

            //Buoc 3: Thực thi truy vấn
            ResultSet rs = ps.executeQuery();

            //Buoc 4: Xử lý kết quả trả về
            while (rs.next()) {
                //Lấy dữ liệu từ ResultSet gán cho các biến cục bộ

                int AccountID = rs.getInt("AccountID");
                String Username = rs.getString("Username");
                String Password = rs.getString("Password");
                int RoleID = rs.getInt("RoleID");
                int TutorID = rs.getInt("TutorID");
                int ParentID = rs.getInt("ParentID");
                int StudentID = rs.getInt("StudentID");
                Date CreationDate = rs.getDate("CreationDate");
                boolean Active = rs.getBoolean("Active");

                list.add(new Account(AccountID, Username, Password, RoleID, TutorID, ParentID, StudentID, CreationDate, Active));
            }
        } catch (SQLException e) {
        }
        return list;
    }

    public ArrayList<Role> getRoleList() {
        ArrayList<Role> list = new ArrayList<Role>();
        try {
            String sql = "select * from Role";

            //Buoc 2: Tạo đối tượng PrepareStatement
            PreparedStatement ps = connection.prepareStatement(sql);

            //Buoc 3: Thực thi truy vấn
            ResultSet rs = ps.executeQuery();

            //Buoc 4: Xử lý kết quả trả về
            while (rs.next()) {
                //Lấy dữ liệu từ ResultSet gán cho các biến cục bộ

                int RoleID = rs.getInt("RoleID");
                String RoleName = rs.getString("RoleName");

                list.add(new Role(RoleID, RoleName));
            }
        } catch (SQLException e) {
        }
        return list;
    }

    public Student getStu(int id) {
        Student c = null;
        try {
            String sql = "select * from student where StudentID = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int StudentID = rs.getInt("StudentID");
                int ParentID = rs.getInt("ParentID");
                String FullName = rs.getString("FullName");
                String Gender = rs.getString("Gender");
                String Address = rs.getString("Address");
                Date BirthDate = rs.getDate("BirthDate");
                String Email = rs.getString("Email");
                String PhoneNumber = rs.getString("PhoneNumber");
                c = new Student(StudentID, ParentID, FullName, Gender, Address, BirthDate, Email, PhoneNumber);
            }
        } catch (SQLException e) {
        }
        return c;
    }

    public Parent getPar(int id) {
        Parent c = null;
        try {
            String sql = "select * from parent where ParentID = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int ParentID = rs.getInt("ParentID");
                String FullName = rs.getString("FullName");
                String Gender = rs.getString("Gender");
                String Address = rs.getString("Address");
                Date BirthDate = rs.getDate("BirthDate");
                String Email = rs.getString("Email");
                String PhoneNumber = rs.getString("PhoneNumber");
                c = new Parent(ParentID, FullName, Gender, Address, BirthDate, Email, PhoneNumber);
            }
        } catch (SQLException e) {
        }
        return c;
    }

    public Tutor getTur(int id) {
        Tutor c = null;
        try {
            String sql = "select * from tutor where TutorID = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int TutorID = rs.getInt("TutorID");
                String FullName = rs.getString("FullName");
                String Gender = rs.getString("Gender");
                String Address = rs.getString("Address");
                Date BirthDate = rs.getDate("BirthDate");
                String Email = rs.getString("Email");
                String PhoneNumber = rs.getString("PhoneNumber");
                int ProfileImage = rs.getInt("ProfileImage");
                int CVFile = rs.getInt("CVFile");
                String Description = rs.getString("Description");
                int SubjectID = rs.getInt("SubjectID");
                int LevelID = rs.getInt("LevelID");
                c = new Tutor(TutorID, FullName, Gender, Address, BirthDate, Email, PhoneNumber, ProfileImage, CVFile, Description, SubjectID, LevelID);
            }
        } catch (SQLException e) {
        }
        return c;
    }

    public Account getAcc(int accID) {
        Account acc = null;
        try {
            String sql = "select * from Account where AccountID = ?";

            //Buoc 2: Tạo đối tượng PrepareStatement
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, accID);
            //Buoc 3: Thực thi truy vấn
            ResultSet rs = ps.executeQuery();

            //Buoc 4: Xử lý kết quả trả về
            while (rs.next()) {
                //Lấy dữ liệu từ ResultSet gán cho các biến cục bộ

                int AccountID = rs.getInt("AccountID");
                String Username = rs.getString("Username");
                String Password = rs.getString("Password");
                int RoleID = rs.getInt("RoleID");
                int TutorID = rs.getInt("TutorID");
                int ParentID = rs.getInt("ParentID");
                int StudentID = rs.getInt("StudentID");
                Date CreationDate = rs.getDate("CreationDate");
                boolean Active = rs.getBoolean("Active");

                acc = new Account(AccountID, Username, Password, RoleID, TutorID, ParentID, StudentID, CreationDate, Active);
            }
        } catch (SQLException e) {
        }
        return acc;
    }

    public Role getRole(int roleID) {
        Role role = null;
        try {
            String sql = "select * from Role where RoleID = ?";

            //Buoc 2: Tạo đối tượng PrepareStatement
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, roleID);
            //Buoc 3: Thực thi truy vấn
            ResultSet rs = ps.executeQuery();

            //Buoc 4: Xử lý kết quả trả về
            while (rs.next()) {
                //Lấy dữ liệu từ ResultSet gán cho các biến cục bộ

                int RoleID = rs.getInt("RoleID");
                String RoleName = rs.getString("RoleName");

                role = new Role(RoleID, RoleName);
            }
        } catch (SQLException e) {
        }
        return role;
    }

    public ArrayList<Account> getAccByKeyword(String keyword, int roleID, int i, int n) {
        ArrayList<Account> accList = new ArrayList<>();
        try {
            PreparedStatement ps = null;
            if (i == 2) {
                String sql = "SELECT * FROM Account WHERE UserName LIKE ? and RoleID=?";
                if (n==1) {
                    sql+=" ORDER BY CreationDate ASC";
                }
                if (n==2) {
                    sql+=" ORDER BY CreationDate DESC";
                }
                 ps = connection.prepareStatement(sql);
                ps.setString(1, "%" + keyword + "%");
                ps.setInt(2, roleID);
            }
            if (i == 1) {
                String sql = "SELECT * FROM Account WHERE UserName LIKE ?";
                if (n==1) {
                    sql+="ORDER BY CreationDate ASC";
                }
                if (n==2) {
                    sql+="ORDER BY CreationDate DESC";
                }
                 ps = connection.prepareStatement(sql);
                ps.setString(1, "%" + keyword + "%");
            }
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int AccountID = rs.getInt("AccountID");
                String Username = rs.getString("Username");
                String Password = rs.getString("Password");
                int RoleID = rs.getInt("RoleID");
                int TutorID = rs.getInt("TutorID");
                int ParentID = rs.getInt("ParentID");
                int StudentID = rs.getInt("StudentID");
                Date CreationDate = rs.getDate("CreationDate");
                boolean Active = rs.getBoolean("Active");

                accList.add(new Account(AccountID, Username, Password, RoleID, TutorID, ParentID, StudentID, CreationDate, Active));
            }
        } catch (SQLException e) {
        }
        return accList;
    }
    
//    public ArrayList<Account> getAccAtPage(int pageNumber, int itemsPerPage, int id, int opt) {
//        ArrayList<Account> accAtPage = new ArrayList<>();
//        String sql = "";
//        PreparedStatement ps = null;
//        try {
//            if (opt == 1) {//OFFSET ? ROWS FETCH NEXT ? ROWS ONLY
//                sql = "SELECT * FROM Account ORDER BY AccountID asc ";
//                ps = connection.prepareStatement(sql);
////            ps.setInt(1, id);
////                ps.setInt(1, (pageNumber - 1) * itemsPerPage);
////                ps.setInt(2, itemsPerPage);
//            } else if (opt == 2) {
//                sql = "SELECT * FROM Products where CategoryID=? ORDER BY ProductID asc OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
//                ps = connection.prepareStatement(sql);
//                ps.setInt(1, id);
//                ps.setInt(2, (pageNumber - 1) * itemsPerPage);
//                ps.setInt(3, itemsPerPage);
//            }else if(opt == 3){
//                sql = "SELECT * FROM Products where ProductName like ? ORDER BY ProductID asc OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
//                ps = connection.prepareStatement(sql);
////            ps.setInt(1, id);
//                
//                ps.setInt(2, (pageNumber - 1) * itemsPerPage);
//                ps.setInt(3, itemsPerPage);
//            }
//
//            ResultSet rs = ps.executeQuery();
//            while (rs.next()) {
//                int AccountID = rs.getInt("AccountID");
//                String Username = rs.getString("Username");
//                String Password = rs.getString("Password");
//                int RoleID = rs.getInt("RoleID");
//                int TutorID = rs.getInt("TutorID");
//                int ParentID = rs.getInt("ParentID");
//                int StudentID = rs.getInt("StudentID");
//                Date CreationDate = rs.getDate("CreationDate");
//                boolean Active = rs.getBoolean("Active");
//
//                accAtPage.add(new Account(AccountID, Username, Password, RoleID, TutorID, ParentID, StudentID, CreationDate, Active));
//            }
//        } catch (SQLException e) {
//        }
//        return accAtPage;
//    }
}
