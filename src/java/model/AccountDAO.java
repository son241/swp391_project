/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import dal.Account;
import dal.DBContext;
import dal.Parent;
import dal.Student;
import dal.Tutor;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author quang
 */
public class AccountDAO extends DBContext {

    public Account getAccount(String Username, String Password) {
        Account acc = null;
        try {
            String sql = "select * from Account where Username = ? and Password =?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, Username);
            ps.setString(2, Password);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                acc = new Account(rs.getInt("AccountID"), rs.getString("UserName"), rs.getString("Password"), rs.getInt("RoleID"), rs.getInt("TutorID"), rs.getInt("ParentID"), rs.getInt("StudentID"), rs.getDate("CreationDate"), rs.getBoolean("Active"));
            }
        } catch (Exception e) {
        }
        return acc;
    }
      public Object getSpecificUser(int RoleID, Account account) {
        switch (RoleID) {
            case 2:
                return new TutorDAO().getTutorbyAccount(account);
            case 3:
                return new ParentDAO().getParentbyAccount(account);
            case 4:
                return new StudentDAO().getStudentbyAccount(account);
            default:
                return null;
        }
    }

    public Account getAccount(String username) {
        Account acc = null;
        try {
            String sql = "Select * from Account where Username = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, username);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                acc = new Account(rs.getInt("AccountID"), rs.getString("UserName"), rs.getString("Password"), rs.getInt("RoleID"), rs.getInt("TutorID"), rs.getInt("ParentID"), rs.getInt("StudentID"), rs.getDate("CreationDate"), rs.getBoolean("Active"));
            }
        } catch (Exception e) {
        }
        return acc;
    }

    public Parent getParent(String email) {
        Parent P = null;
        try {
            String sql = "select * from Parent where Email = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, email);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int ParentID = rs.getInt("ParentID");
                String FullName = rs.getString("FullName");
                String Gender = rs.getString("Gender");
                String Address = rs.getString("Address");
                Date BirthDate = rs.getDate("BirthDate");
                String Email = rs.getString("Email");
                String PhoneNumber = rs.getString("PhoneNumber");
                P = new Parent(ParentID, FullName, Gender, Address, BirthDate, Email, PhoneNumber);
            }
        } catch (SQLException e) {
        }
        return P;
    }

    public int setAccountParent(String Username, String Password, Parent P) {
        int result = 0;
        try {
            long millis = System.currentTimeMillis();
            java.sql.Date date = new java.sql.Date(millis);
            String sql = "insert into Account(Username, Password, RoleID,ParentID, CreationDate, Active)\n"
                    + "values(?,?,?,?,?,?)";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, Username);
            ps.setString(2, Password);
            ps.setInt(3, 3);
            ps.setInt(4, getParent(P.getEmail()).getParentID());
            ps.setDate(5, date);
            ps.setInt(6, 1);
            result = ps.executeUpdate();
        } catch (Exception e) {
        }
        return result;
    }

    public int setParent(Parent P) {
        int result = 0;
        try {
            String sql = "insert into Parent( FullName, Gender, Address, BirthDate, Email, PhoneNumber)\n"
                    + "values(?,?,?,?,?,?)\n";
            long millis = System.currentTimeMillis();
            java.sql.Date date = new java.sql.Date(millis);
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, P.getFullName());
            ps.setString(2, P.getGender());
            ps.setString(3, P.getAddress());
            ps.setDate(4, P.getBirthDate());
            ps.setString(5, P.getEmail());
            ps.setString(6, P.getPhoneNumber());

            result = ps.executeUpdate();

        } catch (Exception e) {
        }
        return result;
    }

    public int setTutor(Tutor T, int is1, int is2) {
        int result = 0;
        try {
            String sql = "insert into tutor (FullName, Gender, Address, BirthDate, Email, PhoneNumber,ProfileImage,CVFile,Description, SubjectID, LevelID)"
                    + "values(?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, T.getFullName());
            ps.setString(2, T.getGender());
            ps.setString(3, T.getAddress());
            ps.setDate(4, T.getBirthDate());
            ps.setString(5, T.getEmail());
            ps.setString(6, T.getPhoneNumber());
            ps.setInt(7, is1);
            ps.setInt(8, is2);
            ps.setString(9, T.getDescription());
            ps.setInt(10, T.getSubjectID());
            ps.setInt(11, T.getLevelID());
            result = ps.executeUpdate();
        } catch (Exception e) {
        }
        return result;
    }

    public int setAccountTutor(String Username, String Password, Tutor T) {
        int result = 0;
        try {
            long millis = System.currentTimeMillis();
            java.sql.Date date = new java.sql.Date(millis);
            String sql = "insert into Account(Username, Password, RoleID,TutorID, CreationDate, Active)\n"
                    + "values(?,?,?,?,?,?)";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, Username);
            ps.setString(2, Password);
            ps.setInt(3, 2);
            ps.setInt(4, getTutor(T.getEmail()).getTutorID());
            ps.setDate(5, date);
            ps.setInt(6, 0);
            result = ps.executeUpdate();
        } catch (Exception e) {
        }
        return result;
    }

    public Tutor getTutor(String Email) {
        Tutor T = null;
        try {
            String sql = "select * from Tutor where Email = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, Email);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int TutorID = rs.getInt("TutorID");
                String FullName = rs.getString("FullName");
                String Gender = rs.getString("Gender");
                String Address = rs.getString("Address");
                Date BirthDate = rs.getDate("BirthDate");
                String PhoneNumber = rs.getString("PhoneNumber");
                int ProfileImage = rs.getInt("ProfileImage");
                int CVFile = rs.getInt("CVFile");
                String Description = rs.getString("Description");
                int SubjectID = rs.getInt("SubjectID");
                int LevelID = rs.getInt("LevelID");
                T = new Tutor(TutorID, FullName, Gender, Address, BirthDate, Email, PhoneNumber, ProfileImage, CVFile, Description, SubjectID, LevelID);
            }
        } catch (Exception e) {
        }
        return T;
    }

//    public int getID(String sql, String str, String str2) {
//        int id = 0;
//        try {
//            PreparedStatement ps = connection.prepareStatement(sql);
//            ps.setString(1, str);
//            ResultSet rs = ps.executeQuery();
//            while (rs.next()) {
//                id = rs.getInt(str2);
//            }
//        } catch (Exception e) {
//        }
//        return id;
//    }
    public Boolean checkPhoneNumber(String Phone, String sql) {
        String str = null;
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, Phone);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                str = rs.getString("PhoneNumber");
            }
        } catch (Exception e) {
        }
        if (str == null) {
            return true;
        } else {
            return false;
        }
    }

    public int updatePasswordbyAccountID(String password, int ID) {
        int index = 0;
        try {
            String sql = "UPDATE  account a SET a.Password = ? WHERE a.AccountID = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, password);
            ps.setInt(2, ID);
            index = ps.executeUpdate();
        } catch (SQLException e) {
        }
        return index;
    }

    public Account getAccountbyRoleID(int RoleID, Object object) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Account account = null;
        try {
            String sql = "";
            switch (RoleID) {
                case 2:
                    sql = "SELECT * FROM account a WHERE a.RoleID = 2 AND a.TutorID = ?";
                    ps = connection.prepareCall(sql);
                    int tID = ((Tutor) object).getTutorID();
                    ps.setInt(1, tID);
                    rs = ps.executeQuery();
                    break;
                case 3:
                    sql = "SELECT * FROM account a WHERE a.RoleID = 3 AND a.ParentID = ?";
                    ps = connection.prepareCall(sql);
                    int pID = ((Parent) object).getParentID();
                    ps.setInt(1, pID);
                    rs = ps.executeQuery();
                    break;
                case 4:
                    sql = "Select * from account a WHERE a.RoleID = 4 AND a.StudentID = ?";
                    ps = connection.prepareCall(sql);
                    int sID = ((Student) object).getStudentID();
                    ps.setInt(1, sID);
                    rs = ps.executeQuery();
                    break;
                default:
                    throw new AssertionError();
            }
            while (rs.next()) {
                account = new Account(rs.getInt("AccountID"), rs.getString("UserName"), rs.getString("Password"), rs.getInt("RoleID"), rs.getInt("TutorID"), rs.getInt("ParentID"), rs.getInt("StudentID"), rs.getDate("CreationDate"), rs.getBoolean("Active"));
            }
        } catch (SQLException e) {
        }
        return account;
    }

    public static void main1(String[] args) {
        Tutor tutor = new TutorDAO().getTutorbyEmail("hoainamnd1@gmail.com");
//        Parent parent = new ParentDAO().getParentbyEmail("hainv@gmail.com");
        System.out.println((new AccountDAO().getAccountbyRoleID(2, tutor).getUsername()));
    }

    public static void main(String[] args) {
        System.out.println(new AccountDAO().updatePasswordbyAccountID("hina", 1));
    }

    Account getAccountBySql(String sql) {
        Account acc = null;
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                acc = new Account(rs.getInt("AccountID"), rs.getString("UserName"), rs.getString("Password"), rs.getInt("RoleID"), rs.getInt("TutorID"), rs.getInt("ParentID"), rs.getInt("StudentID"), rs.getDate("CreationDate"), rs.getBoolean("Active"));
            }
        } catch (Exception e) {
        }
        return acc;
    }

    public int setAccountStudent(String Username, String Password, Student T) {
        int result = 0;
        try {
            long millis = System.currentTimeMillis();
            java.sql.Date date = new java.sql.Date(millis);
            String sql = "insert into Account(Username, Password, RoleID,StudentID, CreationDate, Active)\n"
                    + "values(?,?,?,?,?,?)";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, Username);
            ps.setString(2, Password);
            ps.setInt(3, 4);
            ps.setInt(4, new StudentDAO().getStudentbyEmail(T.getEmail()).getStudentID());
            ps.setDate(5, date);
            ps.setInt(6, 1);
            result = ps.executeUpdate();
        } catch (Exception e) {
        }
        return result;
    }

    public int setStudent(Student T, int ParentID) {
        int result = 0;
        try {
            String sql = "insert into Student( ParentID,FullName, Gender, Address, BirthDate, Email, PhoneNumber)\n"
                    + "values(?,?,?,?,?,?,?)\n";
            long millis = System.currentTimeMillis();
            java.sql.Date date = new java.sql.Date(millis);
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, ParentID);
            ps.setString(2, T.getFullName());
            ps.setString(3, T.getGender());
            ps.setString(4, T.getAddress());
            ps.setDate(5, T.getBirthDate());
            ps.setString(6, T.getEmail());
            ps.setString(7, T.getPhoneNumber());

            result = ps.executeUpdate();

        } catch (Exception e) {
        }
        return result;
    }

    public int updateAccount(String sql){
        int result = 0;
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            result = ps.executeUpdate();
        } catch (Exception e) {
        }
        return result;
    }
}
