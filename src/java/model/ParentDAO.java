/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import dal.*;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Admin
 */
public class ParentDAO extends DBContext {

    public Parent getParentBySql(String sql) {
        Parent parent = null;
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int ParentID = rs.getInt("ParentID");
                String FullName = rs.getString("FullName");
                String Gender = rs.getString("Gender");
                String Address = rs.getString("Address");
                Date BirthDate = rs.getDate("BirthDate");
                String Email = rs.getString("Email");
                String PhoneNumber = rs.getString("PhoneNumber");

                parent = new Parent(ParentID, FullName, Gender, Address, BirthDate, Email, PhoneNumber);
            }
        } catch (Exception e) {
        }

        return parent;
    }
    public Parent getParentbyEmail(String email) {
        Parent parent = null;
        try {
            
        String sql = "SELECT * FROM parent p WHERE p.Email = ? ";
        PreparedStatement ps = connection.prepareStatement(sql);
        ps.setString(1, email);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
                int ParentID = rs.getInt("ParentID");
                String FullName = rs.getString("FullName");
                String Gender = rs.getString("Gender");
                String Address = rs.getString("Address");
                Date BirthDate = rs.getDate("BirthDate");
                String Email = rs.getString("Email");
                String PhoneNumber = rs.getString("PhoneNumber");

               parent = new Parent(ParentID, FullName, Gender, Address, BirthDate, Email, PhoneNumber);
               
            }
        } catch (SQLException e) {
        }
        return parent;
    }
       public Parent getParentbyAccount(Account account) {
        Parent parent = null;
        try {
            String sql = "SELECT * FROM parent p WHERE p.ParentID = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, account.getParentID());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int ParentID = rs.getInt("ParentID");
                String FullName = rs.getString("FullName");
                String Gender = rs.getString("Gender");
                String Address = rs.getString("Address");
                Date BirthDate = rs.getDate("BirthDate");
                String Email = rs.getString("Email");
                String PhoneNumber = rs.getString("PhoneNumber");

                parent = new Parent(ParentID, FullName, Gender, Address, BirthDate, Email, PhoneNumber);
            }
        } catch (SQLException e) {
        }
        return parent;
    }
    public void changeParentProfile(String sql) {
//        String sql = "update Parent\n"
//                + "set \n"
//                + "	FullName = " + currentParent.getFullName() + ",\n"
//                + "    Gender = " + currentParent.getGender() + ",\n"
//                + "    Address = " + currentParent.getAddress() + ",\n"
//                + "    BirthDate = " + currentParent.getBirthDate().toString() + ",\n"
//                + "    Email = " + currentParent.getEmail() + ",\n"
//                + "    PhoneNumber = " + currentParent.getPhoneNumber() + "\n"
//                + "where \n"
//                + "	ParentID = " + currentParent.getParentID();

        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }
     public String getParentEmailbyAccount(Account account) {
        String email = "";
        try {
            String sql = "SELECT p.Email FROM parent p WHERE p.ParentID = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, account.getParentID());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
            email = rs.getString("Email");
            }
        } catch (SQLException e) {
        }
        return email;
    }

}
