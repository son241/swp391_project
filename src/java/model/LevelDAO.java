/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import dal.DBContext;
import dal.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Admin
 */
public class LevelDAO extends DBContext {

    public Level getLevel(int leveID) {
        String sql = "select * from level where LevelID = " + leveID;
        Level level = null;
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String LevelName = rs.getString("LevelName");
                level = new Level(leveID, LevelName);
            }
        } catch (Exception e) {
        }
        return level;
    }
    public ArrayList<Level> getListLevel(){
        ArrayList<Level> list = new ArrayList<>();
        try {
            String sql = "Select * from level";
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {                
                Level L = new Level(rs.getInt("LevelID"), rs.getString("LevelName"));
                list.add(L);
            }
        } catch (Exception e) {
        }
        return list;
    }
    public static void main(String[] args) {
        ArrayList<Level> list = new LevelDAO().getListLevel();
        for (Level item : list) {
            System.out.println(item);
        }
    }
}
