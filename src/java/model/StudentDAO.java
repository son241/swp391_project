/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import dal.*;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Admin
 */
public class StudentDAO extends DBContext {

    public Student getStudentBySql(String sql) {
        Student student = null;
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int StudentID = rs.getInt("StudentID");
                int ParentID = rs.getInt("ParentID");
                String FullName = rs.getString("FullName");
                String Gender = rs.getString("Gender");
                String Address = rs.getString("Address");
                Date BirthDate = rs.getDate("BirthDate");
                String Email = rs.getString("Email");
                String PhoneNumber = rs.getString("PhoneNumber");

                student = new Student(StudentID, ParentID, FullName, Gender, Address, BirthDate, Email, PhoneNumber);
            }
        } catch (Exception e) {
        }

        return student;
    }

    public Student getStudentbyEmail(String email) {
        Student student = null;
        try {

            String sql = "SELECT * FROM student s WHERE s.Email = ? ";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, email);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int StudentID = rs.getInt("StudentID");
                int ParentID = rs.getInt("ParentID");
                String FullName = rs.getString("FullName");
                String Gender = rs.getString("Gender");
                String Address = rs.getString("Address");
                Date BirthDate = rs.getDate("BirthDate");
                String Email = rs.getString("Email");
                String PhoneNumber = rs.getString("PhoneNumber");

                student = new Student(StudentID, ParentID, FullName, Gender, Address, BirthDate, Email, PhoneNumber);

            }
        } catch (SQLException e) {
        }
        return student;
    }

    public Student getStudentbyAccount(Account account) {
        Student student = null;
        try {
            String sql = "SELECT * FROM student s WHERE s.StudentID = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, account.getStudentID());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int StudentID = rs.getInt("StudentID");
                int ParentID = rs.getInt("ParentID");
                String FullName = rs.getString("FullName");
                String Gender = rs.getString("Gender");
                String Address = rs.getString("Address");
                Date BirthDate = rs.getDate("BirthDate");
                String Email = rs.getString("Email");
                String PhoneNumber = rs.getString("PhoneNumber");

                student = new Student(StudentID, ParentID, FullName, Gender, Address, BirthDate, Email, PhoneNumber);

            }
        } catch (SQLException e) {
        }
        return student;
    }

    public int changeStudentProfile(String sql) {
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            //            ps.setString(1, currentStudent.getFullName());
            //            ps.setString(2, currentStudent.getGender());
            //            ps.setString(3, currentStudent.getAddress());
            //            ps.setDate(4, currentStudent.getBirthDate());
            //            ps.setString(5, currentStudent.getEmail());
            //            ps.setString(6, currentStudent.getPhoneNumber());
            //            ps.setString(7, String.valueOf(currentStudent.getStudentID()));
            return ps.executeUpdate();
        } catch (Exception e) {
        }
        return 0;
    }

    public String getStudentEmailbyAccount(Account account) {
        String email = "";
        try {
            String sql = "SELECT s.Email FROM student s WHERE s.StudentID = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, account.getStudentID());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                email = rs.getString("Email");
            }
        } catch (SQLException e) {
        }
        return email;
    }

    public ArrayList<Student> getListChildren(int ParentID) {
        ArrayList<Student> list = new ArrayList<>();
        try {
            String sql = "Select * from Student where ParentID = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, ParentID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int StudentID = rs.getInt("StudentID");
                String FullName = rs.getString("FullName");
                String Gender = rs.getString("Gender");
                String Address = rs.getString("Address");
                Date BirthDate = rs.getDate("BirthDate");
                String Email = rs.getString("Email");
                String PhoneNumber = rs.getString("PhoneNumber");
                Student student = new Student(StudentID, ParentID, FullName, Gender, Address, BirthDate, Email, PhoneNumber);
                list.add(student);
            }
        } catch (Exception e) {
        }
        return list;
    }
    public static void main(String[] args) {
        ArrayList<Student> list = new StudentDAO().getListChildren(1);
        for (Student student : list) {
            System.out.println(student);
        }
    }

}
