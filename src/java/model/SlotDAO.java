/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import dal.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author quang
 */
public class SlotDAO extends DBContext{
    public ArrayList<Slot> getListSlot(){
        ArrayList<Slot> list = new ArrayList<>();
        try {
            String sql = "Select * from Slot";
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {                
                Slot S = new Slot(rs.getInt("SlotID"), rs.getString("SlotName"));
                list.add(S);
            }
        } catch (Exception e) {
        }
        return list;
    }
    public static void main(String[] args) {
        ArrayList<Slot> list = new SlotDAO().getListSlot();
        for (Slot slot : list) {
            System.out.println(slot);
        }
    }
}
