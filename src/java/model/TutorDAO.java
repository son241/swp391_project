/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import dal.DBContext;
import dal.*;
import java.io.InputStream;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Admin
 */
public class TutorDAO extends DBContext {
    
    public Tutor getTutorBySql(String sql) {
        Tutor tutor = null;
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int TutorID = rs.getInt("TutorID");
                String FullName = rs.getString("FullName");
                String Gender = rs.getString("Gender");
                String Address = rs.getString("Address");
                Date BirthDate = rs.getDate("BirthDate");
                String Email = rs.getString("Email");
                String PhoneNumber = rs.getString("PhoneNumber");
                int ProfileImage = rs.getInt("ProfileImage");
                int CVFile = rs.getInt("CVFile");
                String Description = rs.getString("Description");
                int SubjectID = rs.getInt("SubjectID");
                int LevelID = rs.getInt("LevelID");
                
                tutor = new Tutor(TutorID, FullName, Gender, Address, BirthDate, Email, PhoneNumber, ProfileImage, CVFile, Description, SubjectID, LevelID);
            }
        } catch (Exception e) {
        }
        
        return tutor;
    }

    public Tutor getTutorbyEmail(String email) {
        Tutor tutor = null;
        try {
            
            String sql = "SELECT * FROM tutor t WHERE t.Email = ? ";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, email);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int TutorID = rs.getInt("TutorID");
                String FullName = rs.getString("FullName");
                String Gender = rs.getString("Gender");
                String Address = rs.getString("Address");
                Date BirthDate = rs.getDate("BirthDate");
                String Email = rs.getString("Email");
                String PhoneNumber = rs.getString("PhoneNumber");
                int ProfileImage = rs.getInt("ProfileImage");
                int CVFile = rs.getInt("CVFile");
                String Description = rs.getString("Description");
                int SubjectID = rs.getInt("SubjectID");
                int LevelID = rs.getInt("LevelID");
                
                tutor = new Tutor(TutorID, FullName, Gender, Address, BirthDate, Email, PhoneNumber, ProfileImage, CVFile, Description, SubjectID, LevelID);
                
            }
        } catch (SQLException e) {
        }
        return tutor;
    }
    
    public void changeTutorProfile(String sql) {
//        String sql = "update Tutor\n"
//                + "set \n"
//                + "	FullName = " + currentTutor.getFullName() + ",\n"
//                + "    Gender = " + currentTutor.getGender() + ",\n"
//                + "    Address = " + currentTutor.getAddress() + ",\n"
//                + "    BirthDate = " + currentTutor.getBirthDate().toString() + ",\n"
//                + "    Email = " + currentTutor.getEmail() + ",\n"
//                + "    PhoneNumber = " + currentTutor.getPhoneNumber() + ",\n"
//                + "    Description = " + currentTutor.getDescription() + "\n"
//                + "where \n"
//                + "	TutorID = " + currentTutor.getTutorID();
//        
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    public Tutor getTutorbyAccount(Account account) {
        Tutor tutor = null;
        try {
            String sql = "SELECT * FROM tutor t WHERE t.TutorID = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, account.getTutorID());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int TutorID = rs.getInt("TutorID");
                String FullName = rs.getString("FullName");
                String Gender = rs.getString("Gender");
                String Address = rs.getString("Address");
                Date BirthDate = rs.getDate("BirthDate");
                String Email = rs.getString("Email");
                String PhoneNumber = rs.getString("PhoneNumber");
                int ProfileImage = rs.getInt("ProfileImage");
                int CVFile = rs.getInt("CVFile");
                String Description = rs.getString("Description");
                int SubjectID = rs.getInt("SubjectID");
                int LevelID = rs.getInt("LevelID");
                
                tutor = new Tutor(TutorID, FullName, Gender, Address, BirthDate, Email, PhoneNumber, ProfileImage, CVFile, Description, SubjectID, LevelID);
                
            }
        } catch (SQLException e) {
        }
        return tutor;
    }
    
    public ArrayList<Tutor> getTutorListBySql(String sql) {
        ArrayList<Tutor> tutors = new ArrayList<Tutor>();
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int TutorID = rs.getInt("TutorID");
                String FullName = rs.getString("FullName");
                String Gender = rs.getString("Gender");
                String Address = rs.getString("Address");
                Date BirthDate = rs.getDate("BirthDate");
                String Email = rs.getString("Email");
                String PhoneNumber = rs.getString("PhoneNumber");
                int ProfileImage = rs.getInt("ProfileImage");
                int CVFile = rs.getInt("CVFile");
                String Description = rs.getString("Description");
                int SubjectID = rs.getInt("SubjectID");
                int LevelID = rs.getInt("LevelID");
                
                Tutor tutor = new Tutor(TutorID, FullName, Gender, Address, BirthDate, Email, PhoneNumber, ProfileImage, CVFile, Description, SubjectID, LevelID);
                tutors.add(tutor);
            }
        } catch (Exception e) {
        }
        
        return tutors;
    }
     public String getTutorEmailbyAccount(Account account) {
        String email = "";
        try {
            String sql = "SELECT t.Email FROM tutor t WHERE t.TutorID = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, account.getTutorID());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
            email = rs.getString("Email");
            }
        } catch (SQLException e) {
        }
        return email;
    }
 

}
