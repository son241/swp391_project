/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import dal.DBContext;
import dal.Level;
import dal.Subject;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Admin
 */
public class SubjectDAO extends DBContext {

    public Subject getSubject(int SubjectID) {
        String sql = "select * from subject where SubjectID = " + SubjectID;
        Subject subject = null;
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                SubjectID = rs.getInt("SubjectID");
                String SubjectName = rs.getString("SubjectName");
                subject = new Subject(SubjectID, SubjectName);
            }
        } catch (Exception e) {
        }
        return subject;
    }
    public ArrayList<Subject> getListSubject(){
        ArrayList<Subject> list = new ArrayList<>();
        try {
            String sql = "select * from subject";
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {                
                Subject S = new Subject(rs.getInt("SubjectID"), rs.getString("SubjectName"));
                list.add(S);
            }
        } catch (Exception e) {
        }
        return  list;
    }
    public static void main(String[] args) {
        ArrayList<Subject> list = new SubjectDAO().getListSubject();
        for (Subject item : list) {
            System.out.println(item);
        }
    }
}
