/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import dal.*;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class BlogDAO extends DBContext {

    public ArrayList<Blog> getBlogListBySql(String sql) {
        ArrayList<Blog> blogs = new ArrayList<Blog>();
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int BlogID = rs.getInt("BlogID");
                String BlogTitle = rs.getString("BlogTitle");
                String BlogDesciption = rs.getString("BlogDescription");
                int CategoryID = rs.getInt("CategoryID");
                int Image = rs.getInt("Image");
                Blog blog = new Blog(BlogID, BlogTitle, BlogDesciption, CategoryID, Image);
                blogs.add(blog);
            }
        } catch (Exception e) {
        }
        return blogs;
    }

    public Blog getBlogBySql(String sql) {
        Blog blog = null;
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int BlogID = rs.getInt("BlogID");
                String BlogTitle = rs.getString("BlogTitle");
                String BlogDesciption = rs.getString("BlogDescription");
                int CategoryID = rs.getInt("CategoryID");
                int Image = rs.getInt("Image");
                blog = new Blog(BlogID, BlogTitle, BlogDesciption, CategoryID, Image);
            }
        } catch (Exception e) {
        }
        return blog;
    }

    public ArrayList<BlogPart> getBlogPart(int BlogID) {
        ArrayList<BlogPart> blogParts = new ArrayList<BlogPart>();
        try {
            String sql = "select * from BlogPart where BlogID = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, BlogID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int BlogPartID = rs.getInt("BlogPartID");
                int Part = rs.getInt("Part");
                String PartHeader = rs.getString("PartHeader");
                String PartContent = rs.getString("PartContent");
                int Image = rs.getInt("Image");
                BlogPart plogPart = new BlogPart(BlogPartID, BlogID, Part, PartHeader, PartContent, Image);
                blogParts.add(plogPart);
            }
        } catch (Exception e) {
        }
        return blogParts;
    }

    public ArrayList<BlogCategory> getBlogCategoryListBySql(String sql) {
        ArrayList<BlogCategory> BlogCategoryList = new ArrayList<BlogCategory>();
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int CategoryID = rs.getInt("CategoryID");
                String CategoryName = rs.getString("CategoryName");
                BlogCategory b = new BlogCategory(CategoryID, CategoryName);
                BlogCategoryList.add(b);
            }
        } catch (Exception e) {
        }
        return BlogCategoryList;
    }

    public BlogCategory getBlogCategoryBySql(String sql) {
        BlogCategory b = null;
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int CategoryID = rs.getInt("CategoryID");
                String CategoryName = rs.getString("CategoryName");
                b = new BlogCategory(CategoryID, CategoryName);
            }
        } catch (Exception e) {
        }
        return b;
    }

    public int getNumberBySql(String sql) {
        int result = 0;
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                result = rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return result;
    }

    public ArrayList<BlogDiscussion> getBlogDiscussionListBySql(String sql) {
        ArrayList<BlogDiscussion> commentList = new ArrayList<BlogDiscussion>();
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int CommentID = rs.getInt("CommentID");
                int BlogID = rs.getInt("BlogID");
                int AccountID = rs.getInt("AccountID");
                String Comment = rs.getString("Comment");
                Date CommentDate = rs.getDate("CommentDate");
                BlogDiscussion bl = new BlogDiscussion(CommentID, BlogID, AccountID, Comment, CommentDate);
                commentList.add(bl);
            }
        } catch (Exception e) {
        }
        return commentList;
    }

    public Object getUserByAcountID(int AccountID) {
        Account acc = new AccountDAO().getAccountBySql("Select * from Account where AccountID = " + AccountID);
        int accountRole = acc.getRoleID();
        Object user = null;

        switch (accountRole) {
            case 2:
                user = new TutorDAO().getTutorBySql("select * from tutor\n"
                        + "where TutorID = " + acc.getTutorID());
                break;
            case 3:
                user = (Parent) new ParentDAO().getParentBySql("select * from Parent\n"
                        + "where ParentID = " + acc.getParentID());
                break;
            case 4:
                user = (Student) new StudentDAO().getStudentBySql("select * from Student\n"
                        + "where StudentID = " + acc.getStudentID());
                break;
            default:
                user = null;
        }
        return user;
    }

    public static void main(String[] args) {
        ArrayList<Blog> blogs = new BlogDAO().getBlogListBySql("select * from Blog");
        for (Blog item : blogs) {
            System.out.println(item.getBlogTitle());
        }
    }

    public int addComment(BlogDiscussion bl) {
        String sql = "Insert into BlogDiscussion(BlogID, AccountID, Comment, CommentDate)\n"
                + "values(?, ?, ?, ?)";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, bl.getBlogID());
            ps.setInt(2, bl.getAccountID());
            ps.setString(3, bl.getComment());
            ps.setDate(4, bl.getCommentDate());
            return ps.executeUpdate();
        } catch (Exception e) {
        }
        return 0;
    }

    public void updateBlog(Blog blog) {
        String sql = "update Blog\n"
                + "set BlogTitle = ?, BlogDescription = ?\n"
                + "where BlogID = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, blog.getBlogTitle());
            ps.setString(2, blog.getBlogDesciption());
            ps.setInt(3, blog.getBlogID());
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    public void updateBlogPart(ArrayList<BlogPart> blogParts) {
        String sql;
        try {
            for (BlogPart item : blogParts) {
                sql = "update BlogPart\n"
                        + "set PartHeader = ?, PartContent = ?\n"
                        + "where BlogID = ? and Part = ?";
                PreparedStatement ps = connection.prepareStatement(sql);
                ps.setString(1, item.getPartHeader());
                ps.setString(2, item.getPartContent());
                ps.setInt(3, item.getBlogID());
                ps.setInt(4, item.getPart());
                ps.executeUpdate();
            }
        } catch (Exception e) {
        }
    }

    public Blog insertBlog(Blog blog) {
        String sql = "insert into Blog(BlogTitle, BlogDescription, CategoryID, Image)\n"
                + "values(?, ?, ?, ?)";
        dal.Blog newestBlog = null;
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, blog.getBlogTitle());
            ps.setString(2, blog.getBlogDesciption());
            ps.setInt(3, blog.getCategoryID());
            ps.setInt(4, blog.getImage());
            int result = ps.executeUpdate();
            if (result > 0) {
                newestBlog = new BlogDAO().getBlogBySql("select * from Blog \n"
                        + "order by BlogID desc\n"
                        + "limit 1");
            }
        } catch (Exception e) {
        }
        return newestBlog;
    }

    public void insertBlogPart(BlogPart blogPart) {
        String sql;
        try {
            sql = "insert into BlogPart(BlogID, Part, PartHeader, PartContent, Image)\n"
                    + "values(?, ?, ?, ?, ?)";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, blogPart.getBlogID());
            ps.setInt(2, blogPart.getPart());
            ps.setString(3, blogPart.getPartHeader());
            ps.setString(4, blogPart.getPartContent());
            ps.setInt(5, blogPart.getImage());
            ps.executeUpdate();

        } catch (Exception e) {
        }
    }

    public int deleteBlog(int BlogID) {
        String deleteBlogSql = "delete from Blog where BlogID = " + BlogID;
        int result = 0;
        try {
            PreparedStatement ps = connection.prepareStatement(deleteBlogSql);
            result = ps.executeUpdate();
        } catch (Exception e) {
        }
        return result;
    }

    public void deleteBlogPart(int BlogPartID) {

        String sql = "delete from BlogPart where BlogPartID = " + BlogPartID;
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

}
