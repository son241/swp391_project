/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import dal.DBContext;
import dal.File;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author Admin
 */
public class FileDAO extends DBContext {

    public File insertFile(File file) {
        File latestFile = null;
        int result = 0;
        try {
            String sql = "insert into File(FileData)\n"
                    + "values(?)";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setBlob(1, file.getFileData());
            result = ps.executeUpdate();
        } catch (Exception e) {
        }
        if (result > 0) {
            latestFile = new FileDAO().getFileBySql("select * from File order by FileID desc limit 1");
        }
        return latestFile;
    }

    public File getFileBySql(String sql) {
        File latestFile = null;
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int FileID = rs.getInt("FileID");
                InputStream FileData = rs.getBinaryStream("FileData");
                latestFile = new File(FileID, FileData);
            }
        } catch (Exception e) {
        }
        return latestFile;
    }
    
    public int deleteImage(int ImageID){
        String sql = "delete from File where FileID = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, ImageID);
            return ps.executeUpdate();
        } catch (Exception e) {
        }
        return 0;
    }
}
