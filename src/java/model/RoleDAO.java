/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import dal.DBContext;
import dal.Role;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author hoain
 */
public class RoleDAO extends DBContext{
    public Role getRolebyID(int id){
        Role role = null;
        try {
            String sql = "SELECT * FROM role r WHERE r.RoleID = ? ";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                role = new Role(rs.getInt("RoleID"), rs.getString("RoleName"));
            }
        } catch (SQLException e) {
        }
        return role;
        
    }
    public static void main(String[] args) {
        System.out.println(new RoleDAO().getRolebyID(3).getRoleName());
    }
}
