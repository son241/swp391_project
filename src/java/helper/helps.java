/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package helper;

import dal.Account;
import jakarta.mail.Authenticator;
import jakarta.mail.Message;
import jakarta.mail.MessagingException;
import jakarta.mail.PasswordAuthentication;
import jakarta.mail.Session;
import jakarta.mail.Transport;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeMessage;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import java.util.*;
import java.lang.*;
import model.ParentDAO;
import model.StudentDAO;
import model.TutorDAO;

/**
 *
 * @author hoain
 */
public class helps extends HttpServlet {

    public String randomCode() {
        int leftLimit = 48;
        int rightLimit = 122;
        int targetStringLength = 7;
        Random random = new Random();

        return random.ints(leftLimit, rightLimit + 1)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }

    public Object autoDetectUser(int RoleID, String email) {
        switch (RoleID) {
            case 2:
                return new TutorDAO().getTutorbyEmail(email);
            case 3:
                return new ParentDAO().getParentbyEmail(email);
            case 4:
                return new StudentDAO().getStudentbyEmail(email);
            default:
                return null;
        }
    }

    public String autoDetectEmail(int RoleID, Account account) {
        switch (RoleID) {
            case 2:
                return new TutorDAO().getTutorEmailbyAccount(account);
            case 3:
                return new ParentDAO().getParentEmailbyAccount(account);
            case 4:
                return new StudentDAO().getStudentEmailbyAccount(account);
            default:
                return null;
        }
    }

    private String encode(String url) {
        String encodeUrl = Base64.getEncoder().encodeToString(url.getBytes());
        return encodeUrl;
    }

    public String decode(String url) {
        byte[] decodedBytes = Base64.getDecoder().decode(url);
        String decodedString = new String(decodedBytes);
        return decodedString;
    }

    public void sendtoUserEmail(String to, String Npass, Account account, HttpServletRequest req) {
        final String sender = "hoainamnd1@gmail.com";
        final String password = "etscsjlntausykiw";

        Properties prop = new Properties();
        prop.put("mail.smtp.host", "smtp.gmail.com");
        prop.put("mail.smtp.port", "587");
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.starttls.enable", "true");

        Session session = Session.getInstance(prop,
                new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(sender, password);
            }
        });
        try {
            String url = req.getScheme() + "://" + req.getServerName() + ":" + req.getServerPort() + req.getContextPath() + "/changepassword?email=" + encode(autoDetectEmail(account.getRoleID(), account));
            String content = "<div style=\" font-size:1.1rem \"><p style=\"font-size:1.2rem\"><strong>Sensei,</strong></p><p>Your new password is: <strong>" + Npass + "</strong>. You can access here to reset your own password:</p><p>" + url + "</p>";
            MimeMessage msg = new MimeMessage(session);
            msg.setSubject("Forgotten password");
            msg.setText(content, "utf-8", "html");
            msg.setFrom(new InternetAddress(sender));
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
            Transport.send(msg);
        } catch (MessagingException e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        System.out.println(new helps().encode("hoainamnd1@gmail.com"));

    }

}
