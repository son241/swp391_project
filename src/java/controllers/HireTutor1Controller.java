/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controllers;

import dal.GroupSlotAndDayOfWeek;
import dal.Schedule;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import model.ScheduleDAO;

/**
 *
 * @author quang
 */
@WebServlet(name = "HireTutor1", urlPatterns = {"/hiretutor1"})
public class HireTutor1Controller extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("hireTutor1.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        int Action = Integer.parseInt(req.getParameter("action"));
        int Part = 1;
        HttpSession Session = req.getSession();
        if (Action == 1) {
            int StudentID = Integer.parseInt(req.getParameter("txtChildren"));
            String Level = req.getParameter("txtLevel");
            int SubjectID = Integer.parseInt(req.getParameter("txtSubject"));
            int NumberOfSlot = Integer.parseInt(req.getParameter("txtNumberOfSlot"));
            Date Start = Date.valueOf(req.getParameter("txtStart"));
            int NumberOfSessionsAWeek = Integer.parseInt(req.getParameter("txtNumberOfSessionsAWeek"));
            Session.setAttribute("StudentID", StudentID);
            Session.setAttribute("Level", Level);
            Session.setAttribute("SubjectID", SubjectID);
            Session.setAttribute("NumberOfSlot", NumberOfSlot);
            Session.setAttribute("Start", Start);
            Session.setAttribute("NumberOfSessionsAWeek", NumberOfSessionsAWeek);
            req.setAttribute("NumberOfSlot", NumberOfSlot);
            req.setAttribute("Date", Start);
            req.setAttribute("NumberOfSessionsAWeek", NumberOfSessionsAWeek);
            req.setAttribute("Part", Part);
            req.getRequestDispatcher("hireTutor1.jsp").forward(req, resp);
        } else if (Action == 2) {
            Integer StudentIDSession = (Integer) Session.getAttribute("StudentID");
            String LevelSession = (String) Session.getAttribute("Level");
            Integer SubjectIDSession = (Integer) Session.getAttribute("SubjectID");
            Integer NumberOfSlotSession = (Integer) Session.getAttribute("NumberOfSlot");
            Date StartSession = (Date) Session.getAttribute("Start");
            Integer NumberOfSessionsAWeekSession = (Integer) Session.getAttribute("NumberOfSessionsAWeek");
//            int StudentIDSession = Integer.parseInt((String) req.getSession().getAttribute("StudentID"));
//            int LevelSession = Integer.parseInt((String) req.getSession().getAttribute("Level"));
//            int SubjectIDSession = Integer.parseInt((String) req.getSession().getAttribute("SubjectID"));
//            int NumberOfSlotSession = Integer.parseInt((String) req.getSession().getAttribute("NumberOfSlot"));
//            Date StartSession = Date.valueOf((String) req.getSession().getAttribute("Start"));
//            int NumberOfSessionsAWeekSession = Integer.parseInt((String) req.getSession().getAttribute("NumberOfSessionsAWeek"));
            Session.removeAttribute("StudentID");
            Session.removeAttribute("Level");
            Session.removeAttribute("SubjectID");
            Session.removeAttribute("NumberOfSlot");
            Session.removeAttribute("Start");
            Session.removeAttribute("NumberOfSessionsAWeek");
            ArrayList<GroupSlotAndDayOfWeek> listSD = new ArrayList<>();
            for (int i = 1; i <= NumberOfSessionsAWeekSession; i++) {
                String DayOfWeek = req.getParameter("txtDayOfWeek" + i);
                int SlotID = Integer.parseInt(req.getParameter("txtSlot" + i));
                GroupSlotAndDayOfWeek G = new GroupSlotAndDayOfWeek(SlotID, DayOfWeek);
                listSD.add(G);

            }
            listSD = setTime(listSD, NumberOfSlotSession, StartSession);
            int result = 0;
            for (GroupSlotAndDayOfWeek item : listSD) {
                Schedule S = new Schedule(0, 0, StudentIDSession, 1, StartSession, NumberOfSlotSession, SubjectIDSession, item.getSlotID(), item.getTimeOfSlot(), null, null, true);
                result = new ScheduleDAO().SetSchedule(S);
            }
            if (result == 0) {
                req.getRequestDispatcher("index.jsp").forward(req, resp);
            } else {
                req.getRequestDispatcher("blog.jsp").forward(req, resp);
            }

        }
    }

    public static ArrayList<GroupSlotAndDayOfWeek> setTime(ArrayList<GroupSlotAndDayOfWeek> listSD, int NumberOfSlot, Date Start) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//        Calendar c = Calendar.getInstance();
//        c.setTime(Start);
//        c.add(Calendar.DATE, AddDay(listSD.get(0).getDayOfWeek(), Start));
//        Date newDate = Date.valueOf(sdf.format(c.getTime()));
        listSD.get(0).setTimeOfSlot(UpdateTime(Start, listSD.get(0).getDayOfWeek()));
        for (int i = 1; i < listSD.size(); i++) {
            if (checkSlot(listSD, i) >= 0) {
                listSD.get(i).setTimeOfSlot(listSD.get(checkSlot(listSD, i)).getTimeOfSlot());
            } else {
                listSD.get(i).setTimeOfSlot(UpdateTime(listSD.get(i - 1).getTimeOfSlot(), listSD.get(i).getDayOfWeek()));
            }
        }
        int count = NumberOfSlot - listSD.size();
        int z = 0;
        while (count >= 0) {
            int SlotID = listSD.get(z).getSlotID();
            String DayOfWeek = listSD.get(z).getDayOfWeek();
            Date TimeOfSlot = listSD.get(z).getTimeOfSlot();
            Date d = UpdateTime(TimeOfSlot, DayOfWeek);
            listSD.add(new GroupSlotAndDayOfWeek(SlotID, DayOfWeek, d));
            z++;
            count--;
        }
        sortListGSAD(listSD);
        return listSD;
    }

    public static Date UpdateTime(Date TimeNow, String DayOfWeek) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c1 = Calendar.getInstance();
        c1.setTime(TimeNow);
        c1.add(Calendar.DATE, AddDay(TimeNow, DayOfWeek));
        Date newDate1 = Date.valueOf(sdf.format(c1.getTime()));
        return newDate1;
    }

    public static int checkSlot(ArrayList<GroupSlotAndDayOfWeek> listSD, int k) {
        for (int i = 0; i < k; i++) {
            if (listSD.get(i).getDayOfWeek().compareTo(listSD.get(k).getDayOfWeek()) == 0) {
                return i;
            }
        }
        return -1;
    }

    public static int AddDay(Date now, String DayOfWeek) {
        int dow = 0;
        if (DayOfWeek.compareTo("Monday") == 0) {
            dow = 2;
        } else if (DayOfWeek.compareTo("Tuesday") == 0) {
            dow = 3;
        } else if (DayOfWeek.compareTo("Wednesday") == 0) {
            dow = 4;
        } else if (DayOfWeek.compareTo("Thursday") == 0) {
            dow = 5;
        } else if (DayOfWeek.compareTo("Friday") == 0) {
            dow = 6;
        } else if (DayOfWeek.compareTo("Saturday") == 0) {
            dow = 7;
        } else {
            dow = 1;
        }
        Calendar c1 = Calendar.getInstance();
        c1.setTime(now);
        if (dow - c1.get(Calendar.DAY_OF_WEEK) > 0) {
            return dow - c1.get(Calendar.DAY_OF_WEEK);
        } else {
            return 7 + dow - c1.get(Calendar.DAY_OF_WEEK);
        }

    }
    public static void sortListGSAD(ArrayList<GroupSlotAndDayOfWeek> listSD){
        for (int i = 0; i < listSD.size(); i++) {
            for (int j = i+1; j < listSD.size(); j++) {
                if(listSD.get(i).getTimeOfSlot().after(listSD.get(j).getTimeOfSlot())){
                    GroupSlotAndDayOfWeek GSAD = listSD.get(i);
                    listSD.set(i, listSD.get(j));
                    listSD.set(j, GSAD);
                }
            }
        }
        for (int i = 0; i < listSD.size(); i++) {
            for (int j = i+1; j < listSD.size(); j++) {
                if(listSD.get(i).getSlotID()>listSD.get(j).getSlotID()&&listSD.get(i).getTimeOfSlot().equals(listSD.get(j).getTimeOfSlot())){
                    GroupSlotAndDayOfWeek GSAD = listSD.get(i);
                    listSD.set(i, listSD.get(j));
                    listSD.set(j, GSAD);
                }
            }
        }
    }

    public static void main1(String[] args) throws ParseException {

        ArrayList<GroupSlotAndDayOfWeek> listSD = new ArrayList<>();
        listSD.add(new GroupSlotAndDayOfWeek(1, "Monday"));
        listSD.add(new GroupSlotAndDayOfWeek(2, "Tuesday"));
        listSD.add(new GroupSlotAndDayOfWeek(3, "Monday"));
        Date Start = Date.valueOf("2023-02-08");
        int NumberOfSlot = 30;
        listSD = setTime(listSD, NumberOfSlot, Start);

//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//        String s = "2023-02-08";
//        Date Start = Date.valueOf(s);
//        Calendar c = Calendar.getInstance();
//        c.setTime(Start);
//        c.add(Calendar.DATE, AddDay(listSD.get(0).getDayOfWeek(), Start));
//        Date newDate = Date.valueOf(sdf.format(c.getTime()));
//        listSD.get(0).setTimeOfSlot(newDate);
//        for (int i = 1; i < listSD.size(); i++) {
//            if (checkSlot(listSD, i) >= 0) {
//                listSD.get(i).setTimeOfSlot(listSD.get(checkSlot(listSD, i)).getTimeOfSlot());
//            } else {
//                listSD.get(i).setTimeOfSlot(SetTime(listSD.get(i - 1).getTimeOfSlot(), listSD.get(i).getDayOfWeek()));
//            }
//        }
//        int count = 30 - listSD.size();
//        int z=0;
//        while (count >= 0) {
//            int SlotID = listSD.get(z).getSlotID();
//            String DayOfWeek = listSD.get(z).getDayOfWeek();
//            Date TimeOfSlot = listSD.get(z).getTimeOfSlot();
//            Date d = SetTime(TimeOfSlot, DayOfWeek);
//            listSD.add(new GroupSlotAndDayOfWeek(SlotID, DayOfWeek, TimeOfSlot));
//            z++;
//            count--;
//        }
        for (int i = 0; i < listSD.size(); i++) {
            System.out.println(listSD.get(i));
            System.out.println(i);
        }

    }
    public static void main(String[] args) {
        Date d1 = Date.valueOf("2023-02-08");
        Date d2 = Date.valueOf("2023-02-08");
        System.out.println(d1.equals(d2));
    }

}
