/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controllers;

import dal.*;
import helper.ImageHelper;
import model.*;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;

/**
 *
 * @author Admin
 */
@WebServlet(name = "ProfileServlet", urlPatterns = {"/profile"})
public class Profile extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Cookie[] cookieList = req.getCookies();
        String username = "";
        for (Cookie item : cookieList) {
            if (item.getName().equals("username")) {
                username = item.getValue();
                break;
            }
        }

        Account acc = new AccountDAO().getAccount(username);

        int accountRole = acc.getRoleID();
        Object user = null;

        switch (accountRole) {
            case 2:
                req.setAttribute("SubjectDAO", new SubjectDAO());
                req.setAttribute("LevelDAO", new LevelDAO());
                user = new TutorDAO().getTutorBySql("select * from tutor\n"
                        + "where TutorID = " + acc.getTutorID());
                break;
            case 3:
                user = (Parent) new ParentDAO().getParentBySql("select * from Parent\n"
                        + "where ParentID = " + acc.getParentID());
                break;
            case 4:
                user = (Student) new StudentDAO().getStudentBySql("select * from Student\n"
                        + "where StudentID = " + acc.getStudentID());
                break;
            default:
                user = null;
        }

        req.setAttribute("Acc", acc);
        req.setAttribute("User", user);
        req.getRequestDispatcher("/profile.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Cookie[] cookieList = req.getCookies();
        String username = "";
        for (Cookie item : cookieList) {
            if (item.getName().equals("username")) {
                username = item.getValue();
                break;
            }
        }

        Account acc = new AccountDAO().getAccount(username);
        switch (acc.getRoleID()) {
            case 2:
                editTutorProfile(req, resp, acc);
                break;
            case 3:
                editParentProfile(req, resp, acc);
                break;
            default:
                editStudentProfile(req, resp, acc);
                break;
        }
    }

    private void editTutorProfile(HttpServletRequest req, HttpServletResponse resp, Account acc) throws ServletException, IOException {
        Tutor currentTutor = new TutorDAO().getTutorBySql("select * from tutor\n"
                + "where TutorID = " + acc.getTutorID());
        String sql = "update Tutor\n"
                + "set \n"
                + "    FullName = '" + req.getParameter("FullName") + "',\n"
                + "    Gender = '" + req.getParameter("Gender") + "',\n"
                + "    Address = '" + req.getParameter("Address") + "',\n"
                + "    BirthDate = '" + Date.valueOf(req.getParameter("BirthDate")) + "',\n"
                + "    Email = '" + req.getParameter("Email") + "',\n"
                + "    PhoneNumber = '" + req.getParameter("PhoneNumber") + "',\n"
                + "    Description = '" + req.getParameter("Description") + "'\n"
                + "where \n"
                + "	TutorID = " + currentTutor.getTutorID() + "";

//        resp.getWriter().print(sql);
        new TutorDAO().changeTutorProfile(sql);
        doGet(req, resp);
    }

    private void editParentProfile(HttpServletRequest req, HttpServletResponse resp, Account acc) throws ServletException, IOException {
        Parent currentParent = new ParentDAO().getParentBySql("select * from parent\n"
                + "where ParentID = " + acc.getParentID());

        String sql = "update Parent\n"
                + "set \n"
                + "	FullName = '" + req.getParameter("FullName") + "',\n"
                + "    Gender = '" + req.getParameter("Gender") + "',\n"
                + "    Address = '" + req.getParameter("Address") + "',\n"
                + "    BirthDate = '" + Date.valueOf(req.getParameter("BirthDate")) + "',\n"
                + "    Email = '" + req.getParameter("Email") + "',\n"
                + "    PhoneNumber = '" + req.getParameter("PhoneNumber") + "'\n"
                + "where \n"
                + "	ParentID = " + currentParent.getParentID() + "";

        new ParentDAO().changeParentProfile(sql);
        resp.sendRedirect(req.getContextPath() + "/profile");
    }

    private void editStudentProfile(HttpServletRequest req, HttpServletResponse resp, Account acc) throws ServletException, IOException {
        Student currentStudent = new StudentDAO().getStudentBySql("select * from student\n"
                + "where StudentID = " + acc.getStudentID());

        String sql = "update Student\n"
                + "set \n"
                + "	FullName = '" + req.getParameter("FullName") + "',\n"
                + "    Gender = '" + req.getParameter("Gender") + "',\n"
                + "    Address = '" + req.getParameter("Address") + "',\n"
                + "    BirthDate = '" + Date.valueOf(req.getParameter("BirthDate")) + "',\n"
                + "    Email = '" + req.getParameter("Email") + "',\n"
                + "    PhoneNumber = '" + req.getParameter("PhoneNumber") + "'\n"
                + "where \n"
                + "	StudentID = " + currentStudent.getStudentID() + "";
//        resp.getWriter().print(sql);
        new StudentDAO().changeStudentProfile(sql);
        resp.sendRedirect(req.getContextPath() + "/profile");

    }
}
