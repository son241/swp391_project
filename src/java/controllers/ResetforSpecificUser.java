/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controllers;

import dal.Account;
import dal.Role;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.AccountDAO;
import model.RoleDAO;

/**
 *
 * @author hoain
 */
@WebServlet(name = "ResetPass", urlPatterns = {"/reset"})
public class ResetforSpecificUser extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
        try {
            String email = req.getParameter("userEmail");
            int roleID = (int) req.getSession(false).getAttribute("IDSession");
            Object userRole = new helper.helps().autoDetectUser(roleID, email);
            Account account = new AccountDAO().getAccountbyRoleID(roleID, userRole);
            req.getSession().setAttribute("email", email);
            String newPassword = new helper.helps().randomCode();
            int check = new AccountDAO().updatePasswordbyAccountID(newPassword, account.getAccountID());
            if (account == null || check != 1) {
                req.setAttribute("aa", "msg-error");
                req.setAttribute("bb", email + " didn't signuped!");
            } else {
                new helper.helps().sendtoUserEmail(email, newPassword, account, req);
                req.setAttribute("aa", "msg-success");
                req.setAttribute("bb", "Password has been sent to " + email);
            }
            req.getRequestDispatcher("./resetPassword.jsp").forward(req, resp);
        } catch (Exception e) {
            req.setAttribute("aa", "msg-error");
            req.setAttribute("bb", "Send email fail");
            req.getRequestDispatcher("./resetPassword.jsp").forward(req, resp);
        }
    }
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getSession().getAttribute("IDSession") == null) {
            resp.sendRedirect(req.getContextPath() + "/signin");
            return;
        }
        Role role = new RoleDAO().getRolebyID((int) req.getSession().getAttribute("IDSession"));
        HttpSession session = req.getSession();
        session.setAttribute("role", role);
        req.getRequestDispatcher("/resetPassword.jsp").forward(req, resp);
    }

}
