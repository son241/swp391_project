/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controllers;

import dal.Account;
import dal.Tutor;
import helper.ImageHelper;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import model.AccountDAO;
import model.LevelDAO;
import model.MyGeneric;
import model.SubjectDAO;
import model.TutorDAO;

/**
 *
 * @author Admin
 */
@WebServlet(name = "TutorRequest", urlPatterns = {"/tutor/request"})
@MultipartConfig
public class TutorRequest extends HttpServlet {

    boolean view;
    boolean accept;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Cookie[] cookieList = request.getCookies();
        String username = "";
        for (Cookie item : cookieList) {
            if (item.getName().equals("username")) {
                username = item.getValue();
                break;
            }
        }

        Account acc = new AccountDAO().getAccount(username);
        if (acc == null || acc.getRoleID() != 1) {
            response.sendError(401);
        } else {
            if (Boolean.parseBoolean(request.getParameter("accept"))) {
                activateTutorAccount(request, response);
            } else if (Boolean.parseBoolean(request.getParameter("view"))) {
                Tutor user = new TutorDAO().getTutorBySql("select * from tutor\n"
                        + "where TutorID = " + request.getParameter("TutorID"));

                request.setAttribute("SubjectDAO", new SubjectDAO());
                request.setAttribute("LevelDAO", new LevelDAO());
                request.setAttribute("imageHelper", new ImageHelper());
                request.setAttribute("User", user);
                request.getRequestDispatcher("../viewTutorRequest.jsp").forward(request, response);
            } else {
                int pageTh = 1;
                try {
                    pageTh = Integer.parseInt(request.getParameter("page"));
                } catch (Exception e) {
                    pageTh = 1;
                };
                String sql;
                String search;
                try {
                    search = request.getParameter("txtSearch").trim();
                    sql = "select t.TutorID, t.FullName, t.Gender, t.Address, t.BirthDate, t.Email, t.PhoneNumber, t.ProfileImage, t.CVFile, t.Description, t.SubjectID, t.LevelID from Tutor t, Account a\n"
                            + "where FullName like '%" + search + "%' and t.TutorID = a.TutorID and a.Active = false";
                } catch (Exception e) {
                    sql = "select t.TutorID, t.FullName, t.Gender, t.Address, t.BirthDate, t.Email, t.PhoneNumber, t.ProfileImage, t.CVFile, t.Description, t.SubjectID, t.LevelID from Tutor t, Account a\n"
                            + "where t.TutorID = a.TutorID and a.Active = false\n"
                            + "order by t.TutorID desc";
                };

                ArrayList<dal.Tutor> allItem = new TutorDAO().getTutorListBySql(sql);
                int numberOfPage = allItem.size() % 6 == 0 ? allItem.size() / 6 : allItem.size() / 6 + 1;
                ArrayList<dal.Tutor> tutors = new MyGeneric<dal.Tutor>(allItem).dataPaging(pageTh, 6);

                request.setAttribute("imageHelper", new ImageHelper());
                request.setAttribute("currentPage", pageTh);
                request.setAttribute("tutors", tutors);
                request.setAttribute("numberOfPage", numberOfPage);
                request.getRequestDispatcher("../tutorRequest.jsp").forward(request, response);
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    private void activateTutorAccount(HttpServletRequest request, HttpServletResponse response) throws IOException {
        int activate = new AccountDAO().updateAccount("update Account\n"
                + "set Active = true\n"
                + "where TutorID = " + request.getParameter("tutorID"));
        if (activate > 0) {
            response.sendRedirect(request.getContextPath() + "/tutor/request");
        }
    }

}
