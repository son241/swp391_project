/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controllers;

import dal.Account;
import dal.*;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import model.AccountDAO;

/**
 *
 * @author hoain
 */
@WebServlet(name = "ChangePass", urlPatterns = {"/changepassword"})
public class Resetpasswordlogin extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int check = 0;
        resp.setContentType("application/json");
        String oldPass = req.getParameter("oldpass");
        String newPass = req.getParameter("newpass");
        String renewPass = req.getParameter("renewpass");
        Account account = (Account) req.getSession().getAttribute("accSession");
        if (account == null && newPass.equalsIgnoreCase(renewPass) && req.getSession().getAttribute("email") != null) {
            String email = (String) req.getSession().getAttribute("email");
            int roleid = (int) req.getSession(false).getAttribute("IDSession");
            Object userRole = new helper.helps().autoDetectUser(roleid, email);
            Account acc = new AccountDAO().getAccountbyRoleID(roleid, userRole);

            if (!acc.getPassword().equalsIgnoreCase(oldPass)) {
                req.setAttribute("aa", "msg-error");
                req.setAttribute("bb", "Password change failed");
                req.getRequestDispatcher("/changePassword.jsp").forward(req, resp);
            } else {
                check = new AccountDAO().updatePasswordbyAccountID(newPass, acc.getAccountID());
                if (check == 1) {
                    req.setAttribute("aa", "msg-success");
                    req.setAttribute("bb", "Password change successful");
                }
            }

        } else if (account != null && newPass.equalsIgnoreCase(renewPass) && account.getPassword().equalsIgnoreCase(oldPass)) {
            Object object = new AccountDAO().getSpecificUser(account.getRoleID(), account);
            req.setAttribute("user", object);
            check = new AccountDAO().updatePasswordbyAccountID(newPass, account.getAccountID());
            if (check == 1) {
                req.setAttribute("aa", "msg-success");
                req.setAttribute("bb", "Password change successful");
            }

        } else {
            req.setAttribute("aa", "msg-error");
            req.setAttribute("bb", "Password change failed");
            req.getRequestDispatcher("/changePassword.jsp").forward(req, resp);
        }
        req.getRequestDispatcher("/changePassword.jsp").forward(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Cookie[] cookieList = req.getCookies();
        String username = "";
        for (Cookie item : cookieList) {
            if (item.getName().equals("username")) {
                username = item.getValue();
                break;
            }
        }
        
        Account account = new AccountDAO().getAccount(username);
        if (account == null && !req.getParameterMap().containsKey("email")) {
            resp.sendRedirect(req.getContextPath());
            return;
        } else if (account == null && req.getParameterMap().containsKey("email")) {
            req.getRequestDispatcher("/changePassword.jsp").forward(req, resp);
        } else if (account != null) {
            Object object = new AccountDAO().getSpecificUser(account.getRoleID(), account);
            req.setAttribute("user", object);
        }
        req.getRequestDispatcher("/changePassword.jsp").forward(req, resp);

    }

}
