/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controllers;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import dal.*;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import model.*;
import java.sql.Date;

/**
 *
 * @author Admin
 */
@WebServlet(name = "BlogServlet", urlPatterns = {"/blog"})
public class BlogController extends HttpServlet {
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Cookie[] cookieList = req.getCookies();
        String username = "";
        for (Cookie item : cookieList) {
            if (item.getName().equals("username")) {
                username = item.getValue();
                break;
            }
        }

        Account acc = new AccountDAO().getAccount(username);
        int BlogID = Integer.parseInt(req.getParameter("BlogID"));
        int AccountID = acc.getAccountID();
        String Comment = req.getParameter("comment").trim();
        long millis = System.currentTimeMillis();
        Date CommentDate = new Date(millis);
        BlogDiscussion bl = new BlogDiscussion(0, BlogID, AccountID, Comment, CommentDate);
        if (Comment.isEmpty()) {
            req.setAttribute("emptyComment", "yes");
            req.getRequestDispatcher("/single.jsp#comment?BlogID=" + bl.getBlogID()).forward(req, resp);
        } else {
            if (new BlogDAO().addComment(bl) > 0) {
                req.getRequestDispatcher("/single.jsp?BlogID=" + bl.getBlogID()).forward(req, resp);
            }
        }
    }

    public static void main(String[] args) {

    }

}
