/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controllers;

import dal.File;
import dal.Tutor;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Date;
import model.AccountDAO;
import model.FileDAO;

/**
 *
 * @author quang
 */
@MultipartConfig
@WebServlet(name = "SignupTutor", urlPatterns = {"/signuptutor"})
public class SignUpTutorController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/Tutor/signupTutor.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String Name = req.getParameter("txtName");
        Date BOD = null;
        try {
            BOD = Date.valueOf(req.getParameter("txtDate"));
        } catch (Exception e) {
        }
        String Gender = req.getParameter("txtGender");
        String Address = req.getParameter("txtAddress");
        String Phone = req.getParameter("txtPhone");
        String Email = req.getParameter("txtEmail");
        String Username = req.getParameter("txtUsername");
        String Password = req.getParameter("txtPassword");
        String RePassword = req.getParameter("txtRePassword");
        int LevelID = Integer.parseInt(req.getParameter("txtLevel"));
        int SubjectID = Integer.parseInt(req.getParameter("txtSubject"));
        Part ImageProfile = req.getPart("txtImageProfile");
        Part ImageCV = req.getPart("txtImageCV");
//        String ImageProfileName = Paths.get(uploadImageProfile.getSubmittedFileName()).getFileName().toString();
        InputStream is1 = ImageProfile.getInputStream();
        File file1 = new FileDAO().insertFile(new File(0, is1));
        InputStream is2 = ImageCV.getInputStream();
        File file2 = new FileDAO().insertFile(new File(0, is2));
        String Description = req.getParameter("txtDescription");
        if (checkPassword(Password, RePassword) == true && new AccountDAO().getTutor(Email) == null && new AccountDAO().getAccount(Username, Password) == null && Phone.length() == 10 && new AccountDAO().checkPhoneNumber(Phone, "Select PhoneNumber from Tutor where PhoneNumber = ?")) {

            Tutor T = new Tutor(0, Name, Gender, Address, BOD, Email, Phone, 0, 0, Description, SubjectID, LevelID);
            int result1 = new AccountDAO().setTutor(T, file1.getFileID(), file2.getFileID());
            int result2 = new AccountDAO().setAccountTutor(Username, Password, T);
            if (result1 != 0 && result2 != 0) {
                req.getRequestDispatcher("/signin.jsp").forward(req, resp);
            } else {
                req.getRequestDispatcher("/Tutor/signupTutor.jsp").forward(req, resp);
            }
        } else {
            req.setAttribute("Name", Name);
            req.setAttribute("BOD", BOD);
            req.setAttribute("Gender", Gender);
            req.setAttribute("Address", Address);
            req.setAttribute("Phone", Phone);
            req.setAttribute("Email", Email);
            req.setAttribute("Username", Username);
            req.setAttribute("Password", Password);
            req.setAttribute("RePassword", RePassword);
            req.setAttribute("Level", LevelID);
            req.setAttribute("Subject", SubjectID);

            if (checkPassword(Password, RePassword) == false) {
                req.setAttribute("error-Re-Password", "Re-Password is wrong!");
            }
            if (new AccountDAO().checkPhoneNumber(Phone, "Select PhoneNumber from Tutor where PhoneNumber = ?") == false) {
                req.setAttribute("error-phone-exist", "Phone is exist!");
            }
            if (new AccountDAO().getTutor(Email) != null) {
                req.setAttribute("error-email", "Email is exist!");
            }
            if (new AccountDAO().getAccount(Username, Password) != null) {
                req.setAttribute("error-username", "Username is exist!");
            }
            if (Phone.length() != 10) {
                req.setAttribute("error-Phone", "Numbers must be 10 digits long");
            }
            req.getRequestDispatcher("/Tutor/signupTutor.jsp").forward(req, resp);
        }
    }

    public boolean checkPassword(String Password, String RePassword) {
        if (Password.compareTo(RePassword) == 0) {
            return true;
        } else {
            return false;
        }
    }

}
