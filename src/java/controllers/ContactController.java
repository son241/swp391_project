/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controllers;

import dal.Contact;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.Date;
import java.time.LocalDate;
import model.ContactDAO;

/**
 *
 * @author Admin
 */
@WebServlet(name = "ContactControllers", urlPatterns = {"/contact"})
public class ContactController extends HttpServlet {
   
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("contact.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String Name = req.getParameter("txtName");
        String Email = req.getParameter("txtEmail");
        String Subject = req.getParameter("txtSubject");
        String Message = req.getParameter("txtMessage");
        Date date = Date.valueOf(LocalDate.now());
        Contact C = new Contact(0, Name, Email, Subject, Message, date);
//        resp.getWriter().println(C.getContcactDate() + C.getGuestEmail() + C.getGuestName() + C.getMessage() + C.getGuestSubject());
        if(new ContactDAO().sendContact(C) > 0){
            req.setAttribute("success", "true");
            req.getRequestDispatcher("/contact.jsp").forward(req, resp);
        }else{
            req.setAttribute("fail", "true");
            req.getRequestDispatcher("contact.jsp").forward(req, resp);
        }
    }
 
}
