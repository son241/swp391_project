/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controllers;

import dal.*;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Date;
import model.*;

/**
 *
 * @author quang
 */
@WebServlet(name = "SignupStudent", urlPatterns = {"/signupstudent"})
public class SignupStudentController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/Signin.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int Part = 1;
        int Aciton = Integer.parseInt(req.getParameter("action"));
        HttpSession ss = req.getSession();
        Account acc = (Account) ss.getAttribute("accSession");
        Parent P = new ParentDAO().getParentBySql("Select * from Parent where ParentID = " + acc.getParentID());
        if (Aciton == 1) {
            String Email = req.getParameter("txtEmail");
            if (P.getEmail().compareTo(Email) == 0) {
                Part = 2;
                String Code = new helper.MakeStudentAccount().randomCode();
                new helper.MakeStudentAccount().sendMail(Email, Code);
                req.setAttribute("Part", Part);
                req.getSession().setAttribute("Code", Code);
            } else {
                String msg = "This is not your Email";
                req.setAttribute("msg", msg);
            }
            req.getRequestDispatcher("/Student/SignupStudent.jsp").forward(req, resp);
        } else if (Aciton == 2) {
            String Code = (String) req.getSession().getAttribute("Code");
            String txtCode = req.getParameter("txtCode");
            if (Code.equals(txtCode)) {
                Part = 3;
                req.setAttribute("Part", Part);
            } else {
                Part=2;
                String msg = "Code Active is wrong";
                req.setAttribute("msg", msg);
                req.setAttribute("Part", Part);
            }
            req.getRequestDispatcher("/Student/SignupStudent.jsp").forward(req, resp);
        } else {
            Part = 3;
            req.setAttribute("Part", Part);
            String Name = req.getParameter("txtName");
            Date BOD = null;
            try {
                BOD = Date.valueOf(req.getParameter("txtDate"));
            } catch (Exception e) {
            }
            String Gender = req.getParameter("txtGender");
            String Address = req.getParameter("txtAddress");
            String Phone = req.getParameter("txtPhone");
            String Email = req.getParameter("txtEmail");
            String Username = req.getParameter("txtUsername");
            String Password = req.getParameter("txtPassword");
            String RePassword = req.getParameter("txtRePassword");
            if (Password.compareTo(RePassword) == 0 && new StudentDAO().getStudentbyEmail(Email) == null
                    && new AccountDAO().getAccount(Username, Password) == null && Phone.length() == 10
                    && new AccountDAO().checkPhoneNumber(Phone, "Select PhoneNumber from Parent where PhoneNumber = ?")) {
                Student T = new Student(0, P.getParentID(), Name, Gender, Address, BOD, Email, Phone);
                int result1 = new AccountDAO().setStudent(T, acc.getParentID());
                int result2 = new AccountDAO().setAccountStudent(Username, Password, T);
                if (result1 != 0 && result2 != 0) {
                    req.getRequestDispatcher("/signin.jsp").forward(req, resp);
                } else {
                    req.getRequestDispatcher("/Student/SignupStudent.jsp").forward(req, resp);
                }
            } else {
                req.setAttribute("Name", Name);
                req.setAttribute("BOD", BOD);
                req.setAttribute("Gender", Gender);
                req.setAttribute("Address", Address);
                req.setAttribute("Phone", Phone);
                req.setAttribute("Email", Email);
                req.setAttribute("Username", Username);
                req.setAttribute("Password", Password);
                req.setAttribute("RePassword", RePassword);
                if (Password.compareTo(RePassword) != 0) {
                    req.setAttribute("error-Re-Password", "Re-Password is wrong!");
                }
                if (new AccountDAO().checkPhoneNumber(Phone, "Select PhoneNumber from Parent where PhoneNumber = ?") == false) {
                    req.setAttribute("error-phone-exist", "Phone is exist!");
                }
                if (new AccountDAO().getParent(Email) != null) {
                    req.setAttribute("error-email", "Email is exist!");
                }
                if (new AccountDAO().getAccount(Username, Password) != null) {
                    req.setAttribute("error-username", "Username is exist!");
                }
                if (Phone.length() != 10) {
                    req.setAttribute("error-Phone", "Numbers must be 10 digits long");
                }
                req.getRequestDispatcher("/Student/SignupStudent.jsp").forward(req, resp);
            }
        }
    }

    public static void main(String[] args) {
        System.out.println(new ParentDAO().getParentBySql("Select * from Parent where ParentID =1"));
    }

}
