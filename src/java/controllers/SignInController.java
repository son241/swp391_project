/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controllers;

import dal.Account;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import model.AccountDAO;

/**
 *
 * @author quang
 */
@WebServlet(name = "SininServlet", urlPatterns = {"/signin"})
public class SignInController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Cookie[] cookieList = req.getCookies();
        String username = "";
        for (Cookie item : cookieList) {
            if (item.getName().equals("username")) {
                username = item.getValue();
                break;
            }
        }

        Account acc = new AccountDAO().getAccount(username);
        if (acc != null) {
            Cookie ck = new Cookie("username", "");
            ck.setMaxAge(0);
            resp.addCookie(ck);
        }
        req.getRequestDispatcher("signin.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String Username;
        try {
            Username = req.getParameter("txtUsername").trim();
        } catch (Exception e) {
            Username = "";
        }
        String Password;
        try {
            Password = req.getParameter("txtPassword").trim();
        } catch (Exception e) {
            Password = "";
        }
        req.setAttribute("Username", Username);
        req.setAttribute("Password", Password);
        Account acc = new AccountDAO().getAccount(Username, Password);
        if (acc != null && acc.isActive()) {
            Cookie ck = new Cookie("username", acc.getUsername());
            HttpSession session = req.getSession();
            session.setAttribute("accSession", acc);
            ck.setMaxAge(3600);
            resp.addCookie(ck);
            if (acc.getRoleID() == 3) {
                resp.sendRedirect(req.getContextPath() + "/Parent/ParentOption.jsp");
            } else if (acc.getRoleID() == 1) {
                resp.sendRedirect(req.getContextPath() + "/adminHomepage.jsp");
            } else {
                resp.sendRedirect(req.getContextPath() + "/index.jsp");
            }

        } else if (!Username.equals("") && !Password.equals("")) {
            if (acc != null) {
                if (!acc.isActive() && acc != null) {
                    req.setAttribute("inActive", "yes");
                }
            } else {
                req.setAttribute("errorMsg", "Yes");
            }

            req.getRequestDispatcher("/signin.jsp").forward(req, resp);
        } else {
            if (Username.equals("")) {
                req.setAttribute("emptyUsername", "yes");
            }

            if (Password.equals("")) {
                req.setAttribute("emptyPass", "yes");
            }

            req.getRequestDispatcher("/signin.jsp").forward(req, resp);
        }
    }
}
