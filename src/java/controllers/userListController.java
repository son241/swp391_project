/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controllers;

import dal.Account;
import dal.Role;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import model.listUserDAO;

/**
 *
 * @author Mien Tinh
 */
@WebServlet(name = "UserList", urlPatterns = {"/userList"})
public class userListController extends HttpServlet {

    private final int itemsPerPage = 2;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String keyword = "";
        if (req.getParameter("keyword") != null) {
            keyword = req.getParameter("keyword");
        }
        int RoleID = -1;
        if (req.getParameter("RoleID") != null) {
            RoleID = Integer.parseInt(req.getParameter("RoleID"));
        }

        int sort = 0;
        if (req.getParameter("sort") != null) {
            sort = Integer.parseInt(req.getParameter("sort"));
        }
        Role role = null;
        ArrayList<Account> accList = null;
        if (RoleID != -1) {
            role = new listUserDAO().getRole(RoleID);

            if (sort == 0 || sort == 1) {
                accList = new listUserDAO().getAccByKeyword(keyword, RoleID, 2, 1);
            } else {
                accList = new listUserDAO().getAccByKeyword(keyword, RoleID, 2, 2);
            }

        } else {
            if (sort == 0 || sort == 1) {
                accList = new listUserDAO().getAccByKeyword(keyword, RoleID, 1, 1);
            } else {
                accList = new listUserDAO().getAccByKeyword(keyword, RoleID, 1, 2);
            }
        }

        ArrayList<Account> accList2 = new ArrayList<Account>();
        for (Account a : accList) {
            if (a.getRoleID() != 1) {
                accList2.add(a);
            }
        }

        ArrayList<Role> roleList = new listUserDAO().getRoleList();

        int pageQuantity = (int) Math.ceil(accList2.size() / ((double) itemsPerPage));
        int pageNumber = 1;
        try {
            pageNumber = Integer.parseInt(req.getParameter("page"));
        } catch (Exception e) {
        }
//        ArrayList<Account> accInPage = new listUserDAO().getAccAtPage(pageNumber, itemsPerPage,1,1);

//        req.setAttribute("accInPage", accInPage);
//int index =1;
        ArrayList<Account> accAPage = new ArrayList<Account>();

//        for(int i = 1; i <=accList.size();i++){
//            if(i>itemsPerPage*pageNumber&&i<=itemsPerPage*(pageNumber+1)){
//                accAPage.add(accList.get(i));
//            }
//        }
        for (int i = (pageNumber - 1) * itemsPerPage; i < Math.min((pageNumber) * itemsPerPage, accList2.size()); i++) {
            accAPage.add(accList2.get(i));
        }

        req.setAttribute("sort", sort);
        req.setAttribute("accAPage", accAPage);
        req.setAttribute("pageQuantity", pageQuantity);
        req.setAttribute("pageNumber", pageNumber);
        req.setAttribute("dao", new listUserDAO());
        req.setAttribute("role", role);
        req.setAttribute("RoleID", RoleID);
        req.setAttribute("roleList", roleList);
        req.setAttribute("keyword", keyword);
        req.setAttribute("option", "search");
        req.setAttribute("accList", accList2);

        req.getRequestDispatcher("/userList.jsp").forward(req, resp);

    }

}
