/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controllers;

import dal.Account;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import model.AccountDAO;

/**
 *
 * @author Admin
 */
@WebServlet(name = "SingoutServlet", urlPatterns = {"/signout"})
public class SignOut extends HttpServlet{

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Cookie[] cookieList = req.getCookies();
        String username = "";
        for (Cookie item : cookieList) {
            if (item.getName().equals("username")) {
                username = item.getValue();
                break;
            }
        }
        
        Account acc = new AccountDAO().getAccount(username);
        if (acc != null) {
            Cookie ck = new Cookie("username", "");
            ck.setMaxAge(0);
            resp.addCookie(ck);
            resp.sendRedirect(req.getContextPath() + "/index.jsp");
        }
    }
    
}
