/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controllers;

import dal.Account;
import dal.Parent;
import dal.Student;
import dal.Tutor;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import model.AccountDAO;
import model.LevelDAO;
import model.ParentDAO;
import model.StudentDAO;
import model.SubjectDAO;
import model.TutorDAO;

/**
 *
 * @author Mien Tinh
 */
@WebServlet(name = "editUser", urlPatterns = {"/editUserList"})
public class editUserListController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        Cookie[] cookieList = req.getCookies();
//        String username = "";
//        for (Cookie item : cookieList) {
//            if (item.getName().equals("username")) {
//                username = item.getValue();
//                break;
//            }
//        }
        String userName = req.getParameter("userName");
        Account acc = new AccountDAO().getAccount(userName);

//        int accountRole = acc.getRoleID();
        Object user = null;
        int roleID = Integer.parseInt(req.getParameter("roleId"));
        switch (roleID) {
            case 2:
                user = new TutorDAO().getTutorBySql("select * from tutor\n"
                        + "where TutorID = " + acc.getTutorID());
                break;
            case 3:
                user = (Parent) new ParentDAO().getParentBySql("select * from Parent\n"
                        + "where ParentID = " + acc.getParentID());
                break;
            case 4:
                user = (Student) new StudentDAO().getStudentBySql("select * from Student\n"
                        + "where StudentID = " + acc.getStudentID());
                break;
            default:
                user = null;
        }

        //Test login Session
//        req.getSession().setAttribute("AccSession", acc);
        //Test login Session
        req.setAttribute("LevelDAO", new LevelDAO());
        req.setAttribute("SubjectDAO", new SubjectDAO());
        req.setAttribute("User", user);
        req.setAttribute("roleID", roleID);
        req.setAttribute("Acc", acc);
        req.setAttribute("userName", userName);

        req.getRequestDispatcher("editUserList.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        Account acc = (Account) req.getSession().getAttribute("AccSession");

//        if (req.getParameter("Gender").equalsIgnoreCase("F") || req.getParameter("Gender").equalsIgnoreCase("M") || req.getParameter("PhoneNumber").matches("[0-9]{10}")) {
//            if (req.getParameter("Gender").equalsIgnoreCase("F") || req.getParameter("Gender").equalsIgnoreCase("M")) {
//                 req.setAttribute("error-gender", "F or M only!");
//            }
//            if (req.getParameter("PhoneNumber").matches("[0-9]{10}")) {
//                req.setAttribute("error-phone", "Invalid phone number!");
//            }
//            
//            req.getRequestDispatcher("/profile.jsp").forward(req, resp);
//        }
//        
            int roleID = Integer.parseInt(req.getParameter("roleID"));
        String userName = req.getParameter("userName");
        Account acc = new AccountDAO().getAccount(userName);
        switch (roleID) {
            case 2:
                editTutorProfile(req, resp, acc);
                break;
            case 3:
                editParentProfile(req, resp, acc);
                break;
            default:
                editStudentProfile(req, resp, acc);
                break;
        }
    }

    private void editTutorProfile(HttpServletRequest req, HttpServletResponse resp, Account acc) throws ServletException, IOException {
        Tutor currentTutor = new TutorDAO().getTutorBySql("select * from tutor\n"
                + "where TutorID = " + acc.getTutorID());
        String sql = "update Tutor\n"
                + "set \n"
                + "    FullName = '" + req.getParameter("FullName") + "',\n"
                + "    Gender = '" + req.getParameter("Gender") + "',\n"
                + "    Address = '" + req.getParameter("Address") + "',\n"
                + "    BirthDate = '" + Date.valueOf(req.getParameter("BirthDate")) + "',\n"
                + "    Email = '" + req.getParameter("Email") + "',\n"
                + "    PhoneNumber = '" + req.getParameter("PhoneNumber") + "',\n"
                + "    Description = '" + req.getParameter("Description") + "'\n"
                + "where \n"
                + "	TutorID = " + currentTutor.getTutorID() + "";

//        resp.getWriter().print(sql);
        new TutorDAO().changeTutorProfile(sql);
        
        resp.sendRedirect(req.getContextPath() + "/userList");
    }

    private void editParentProfile(HttpServletRequest req, HttpServletResponse resp, Account acc) throws ServletException, IOException {
        Parent currentParent = new ParentDAO().getParentBySql("select * from parent\n"
                + "where ParentID = " + acc.getParentID());

        String sql = "update Parent\n"
                + "set \n"
                + "	FullName = '" + req.getParameter("FullName") + "',\n"
                + "    Gender = '" + req.getParameter("Gender") + "',\n"
                + "    Address = '" + req.getParameter("Address") + "',\n"
                + "    BirthDate = '" + Date.valueOf(req.getParameter("BirthDate")) + "',\n"
                + "    Email = '" + req.getParameter("Email") + "',\n"
                + "    PhoneNumber = '" + req.getParameter("PhoneNumber") + "'\n"
                + "where \n"
                + "	ParentID = " + currentParent.getParentID() + "";

        new ParentDAO().changeParentProfile(sql);
        resp.sendRedirect(req.getContextPath() + "/userList");
    }

    private void editStudentProfile(HttpServletRequest req, HttpServletResponse resp, Account acc) throws ServletException, IOException {
        Student currentStudent = new StudentDAO().getStudentBySql("select * from student\n"
                + "where StudentID = " + acc.getStudentID());

        String sql = "update Student\n"
                + "set \n"
                + "	FullName = '" + req.getParameter("FullName") + "',\n"
                + "    Gender = '" + req.getParameter("Gender") + "',\n"
                + "    Address = '" + req.getParameter("Address") + "',\n"
                + "    BirthDate = '" + Date.valueOf(req.getParameter("BirthDate")) + "',\n"
                + "    Email = '" + req.getParameter("Email") + "',\n"
                + "    PhoneNumber = '" + req.getParameter("PhoneNumber") + "'\n"
                + "where \n"
                + "	StudentID = " + currentStudent.getStudentID() + "";
//        resp.getWriter().print(sql);
        new StudentDAO().changeStudentProfile(sql);
        resp.sendRedirect(req.getContextPath() + "/userList");
    }
}
