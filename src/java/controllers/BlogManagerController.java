/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controllers;

import dal.*;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.*;
import model.*;

/**
 *
 * @author Admin
 */
@WebServlet(name = "BlogManagerServlet", urlPatterns = {"/blog/manage"})
@MultipartConfig
public class BlogManagerController extends HttpServlet {

    boolean edit;
    boolean post;
    boolean addPart;
    boolean removePart;
    boolean delete;
    private static int parts = 1;
    ArrayList<BlogPart> postBlogParts = new ArrayList<BlogPart>();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Cookie[] cookieList = request.getCookies();
        String username = "";
        for (Cookie item : cookieList) {
            if (item.getName().equals("username")) {
                username = item.getValue();
                break;
            }
        }

        Account acc = new AccountDAO().getAccount(username);
        if (acc == null || acc.getRoleID() != 1) {
            response.sendError(401);
        } else {
            delete = Boolean.parseBoolean(request.getParameter("delete"));
            addPart = Boolean.parseBoolean(request.getParameter("addPart"));
            removePart = Boolean.parseBoolean(request.getParameter("removePart"));
            if (addPart) {
                parts += 1;
            }

            if (removePart) {
                if (parts > 1) {
                    parts -= 1;
                }
            }

            if (delete) {
                deleteBlog(request, response);
            }

            request.setAttribute("Categories", new BlogDAO().getBlogCategoryListBySql("Select * from BlogCategory"));
            request.setAttribute("Parts", parts);
            if (Boolean.parseBoolean(request.getParameter("post"))) {
                request.getRequestDispatcher("../postBlog.jsp").forward(request, response);
            } else {
                response.sendRedirect("../manageBlog.jsp");
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Cookie[] cookieList = request.getCookies();
        String username = "";
        for (Cookie item : cookieList) {
            if (item.getName().equals("username")) {
                username = item.getValue();
                break;
            }
        }

        Account acc = new AccountDAO().getAccount(username);
        if (acc == null || acc.getRoleID() != 1) {
            response.sendError(401);
        } else {
            edit = Boolean.parseBoolean(request.getParameter("edit"));
            post = Boolean.parseBoolean(request.getParameter("post"));

            if (post) {
                postBlog(request, response);
            }

            if (edit) {
                editBlog(request, response);
            }
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void editBlog(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        dal.Blog blog = new BlogDAO().getBlogBySql("Select * from Blog where BlogID = " + request.getParameter("BlogID"));
        ArrayList<BlogPart> blogParts = new BlogDAO().getBlogPart(blog.getBlogID());
        blog.setBlogTitle(request.getParameter("BlogTitle"));
        blog.setBlogDesciption(request.getParameter("BlogDescription"));

        for (BlogPart item : blogParts) {
            item.setPartHeader(request.getParameter("part-" + item.getPart() + "-header"));
            item.setPartContent(request.getParameter("part-" + item.getPart() + "-content"));
        }

        new BlogDAO().updateBlog(blog);
        new BlogDAO().updateBlogPart(blogParts);
        request.getRequestDispatcher("../editBlog.jsp?BlogID=" + blog.getBlogID()).forward(request, response);
    }

    private void postBlog(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        Part BlogImg = request.getPart("BlogImage");
        File file = new FileDAO().insertFile(new File(0, BlogImg.getInputStream()));
        String BlogTitle = request.getParameter("BlogTitle");
        String BlogDesciption = request.getParameter("BlogDescription");
        int BlogCategory = Integer.parseInt(request.getParameter("BlogCategory"));

        dal.Blog blog = new BlogDAO().insertBlog(new dal.Blog(0, BlogTitle, BlogDesciption, BlogCategory, file.getFileID()));
        for (int i = 1; i <= parts; i++) {
            int BlogID = blog.getBlogID();
            int Part = i;
            String PartHeader = request.getParameter("part-" + i + "-header");
            String PartContent = request.getParameter("part-" + i + "-content");
            Part Image = request.getPart("part-" + i + "-image");
            file = new FileDAO().insertFile(new File(0, Image.getInputStream()));
            BlogPart b = new BlogPart(0, BlogID, Part, PartHeader, PartContent, file.getFileID());
            new BlogDAO().insertBlogPart(b);
        }
        parts = 1;
        request.getRequestDispatcher("../editBlog.jsp?BlogID=" + blog.getBlogID()).forward(request, response);
    }

    private void deleteBlog(HttpServletRequest request, HttpServletResponse response) throws IOException {

        String BlogID = request.getParameter("BlogID");
        Blog blog = new BlogDAO().getBlogBySql("select * from Blog where BlogID = " + BlogID);
        ArrayList<BlogPart> PartList = new BlogDAO().getBlogPart(Integer.parseInt(BlogID));
        for (BlogPart item : PartList) {
            new BlogDAO().deleteBlogPart(item.getBlogPartID());
            new FileDAO().deleteImage(item.getImage());
        }
        new BlogDAO().deleteBlog(blog.getBlogID());
        new FileDAO().deleteImage(blog.getImage());
        
    }

}
