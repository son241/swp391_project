/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controllers;

import dal.Account;
import dal.Parent;
import dal.Student;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import model.AccountDAO;
import model.ParentDAO;
import model.StudentDAO;
import model.TutorDAO;

@WebServlet(name = "deleteUser", urlPatterns = {"/deleteUser"})
public class deleteUserController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userName = req.getParameter("userName");
        Account acc = new AccountDAO().getAccount(userName);
        lockAcc(req, resp, acc);

//        req.getRequestDispatcher("editUserList.jsp").forward(req, resp);
//        resp.sendRedirect("./signin");
    }

    private void lockAcc(HttpServletRequest req, HttpServletResponse resp, Account acc) throws ServletException, IOException {
        
        String sql = "UPDATE account\n"
                + "SET Active = false\n"
                + "WHERE AccountID="+acc.getAccountID();
        
        new ParentDAO().changeParentProfile(sql);
        resp.sendRedirect(req.getContextPath() + "/userList");
    }

}
