<%--<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>--%>
<%@include file="./template/header.jsp" %>
<%
        String path = request.getContextPath();
        request.setAttribute("path", path);
        request.setAttribute("AccountDAO", new AccountDAO());
        int pageTh = 1;
        try {
            pageTh = Integer.parseInt(request.getParameter("page"));
        } catch (Exception e) {
            pageTh = 1;
        }
        String sql;
        int categoryID;
        String search;
       
        try {
            categoryID = Integer.parseInt(request.getParameter("categoryID"));
        } catch (Exception e) {
            categoryID = 0;
        }
       
        try {
            search = request.getParameter("search").trim();     
        } catch (Exception e) {
            search = "";
        }
        
        sql = "select * from Blog where CategoryID = " + categoryID + " and BlogTitle like '%"+search+"%'";
        
        if(!search.isEmpty() && categoryID != 0){
             sql = "select * from Blog where CategoryID = " + categoryID + " and BlogTitle like '%"+search+"%'";
        }else if(!search.isEmpty() && categoryID == 0){
             sql = "select * from Blog where BlogTitle like '%"+search+"%'";
        }else if(search.isEmpty() && categoryID != 0){   
             sql = "select * from Blog where CategoryID = " + categoryID;
        }else{   
             sql = "select * from Blog order by BlogID desc";           
        }
        
        ArrayList<BlogCategory> category = new BlogDAO().getBlogCategoryListBySql("select * from BlogCategory");       
        ArrayList<dal.Blog> allItem = new BlogDAO().getBlogListBySql(sql);
        int numberOfPage = allItem.size() % 6 == 0 ? allItem.size() / 6 : allItem.size() / 6 + 1;
        ArrayList<dal.Blog> blogs = new MyGeneric<Blog>(allItem).dataPaging(pageTh, 6);
        
        request.setAttribute("category", categoryID);
        request.setAttribute("search", search);
        request.setAttribute("Categories", category);
        request.setAttribute("currentPage", pageTh);
        request.setAttribute("blogs", blogs);
        request.setAttribute("numberOfPage", numberOfPage);

%>     
<c:if test="${AccountDAO.getAccount(cookie.username.getValue()).getRoleID() ne 1}">
    <c:redirect url="/401Page.jsp"/>
</c:if>
<div class="container-fluid bg-primary mb-5">
    <div class="d-flex flex-column align-items-center justify-content-center" style="min-height: 400px">
        <h3 class="display-3 font-weight-bold text-white">Blog Manager</h3>
    </div>
</div>
<!-- Header End -->


<!-- Blog Start -->
<div class="container-fluid pt-5">
    <div class="container">
        <form  action="manageBlog.jsp">
            <select name="categoryID" class="btn btn-light px-4 mx-auto my-2 border-primary">
                <option value="0">None</option>
                <c:forEach items="${Categories}" var="item">
                    <option <c:out value="${item.getCategoryID() eq category ? 'selected' : ''}"/> value="${item.getCategoryID()}">${item.getCategoryName()}</option>
                </c:forEach>             
            </select>
            <input type="submit" class="btn btn-primary px-4 mx-auto my-2" value="Filter">
            <input style="cursor: text" class="btn btn-light px-4 mx-auto my-2 border-primary pl-xl-5 w-50 " value="${search}" type="text" name="search" placeholder="Search by title" >
            <input type="submit" class="btn btn-primary px-4 mx-auto my-2" value="Search">
        </form>
        <a href="<c:url value="/blog/manage?post=true"/>" class="btn btn-light px-4 mx-auto my-2 text-primary float-right">&#9535; Post Blog</a>

        <div class="text-center pb-2">
            <p class="section-title px-5"><span class="px-2">Latest Blog</span></p>
            <h1 class="mb-4">Latest Articles From Blog</h1>
        </div>
        <div class="row pb-3">
            <c:forEach items="${blogs}" var="item">
                <div class="col-lg-4 mb-4">
                    <div class="card border-0 shadow-sm mb-2">
                        <img class="card-img-top mb-2" src="<c:url value="/displayImage?Type=jpeg&FileID=${item.getImage()}"/>" alt="">
                        <div class="card-body bg-light text-center p-4">
                            <h4 class="">${item.getBlogTitle()}</h4>                           
                            <p>${item.getBlogDesciption()}</p>
                            <a href="${path}/editBlog.jsp?BlogID=${item.getBlogID()} " class="btn btn-primary px-4 mx-auto my-2">Edit</a>
                        </div>
                    </div>
                </div>
            </c:forEach>      
        </div>
        <div class="col-md-12 mb-4">
            <nav aria-label="Page navigation">
                <ul class="pagination justify-content-center mb-0">
                    <li class="page-item">
                        <c:if test="${currentPage ne 1}">
                            <a class="page-link" href="${path}/blog.jsp?page=${i - 1}" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                                <span class="sr-only">Previous</span>
                            </a>
                        </c:if>
                    </li>
                    <c:forEach begin="1" end="${numberOfPage}" var="i">                            
                        <c:choose>
                            <c:when test="${i eq currentPage}">
                                <li class="page-item active"><a class="page-link" href="${path}/blog.jsp?page=${i}">${i}</a></li>
                                </c:when>
                                <c:otherwise>
                                <li class="page-item"><a class="page-link" href="${path}/blog.jsp?page=${i}">${i}</a></li>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>

                    <c:if test="${currentPage ne numberOfPage}">
                        <li class="page-item">
                            <a class="page-link" href="${path}/blog.jsp?page=${currentPage + 1}" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                                <span class="sr-only">Next</span>
                            </a>
                        </li>
                    </c:if>

                </ul>
            </nav>
        </div>
    </div>
</div>
</div>
<!-- Blog End -->
<%@include file="template/footer.jsp" %>
