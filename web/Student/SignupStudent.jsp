<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <title>title</title>
        <style>
            /*signin*/
            @import url('https://fonts.googleapis.com/css?family=Montserrat:400,800');

            * {
                box-sizing: border-box;
            }

            body {
                font-family: 'Montserrat', sans-serif;
                background-color: rgb(20,163,184);
                display: flex;
                flex-direction: column;
                justify-content: center;
                align-items: center;
                height: 100vh;
                margin: -20px 0 50px;
                margin-top: 20px;
            }

            h1 {
                font-weight: bold;
                margin: 0;
            }

            p {
                font-size: 14px;
                font-weight: 100;
                /*                line-height: 20px;*/
                letter-spacing: .5px;
                margin: 20px 0 30px;
            }

            span {
                font-size: 12px;
            }

            a {
                color: #333;
                font-size: 14px;
                text-decoration: none;
                margin: 15px 0;
            }

            .container {
                background: #fff;
                border-radius: 10px;
                box-shadow: 0 14px 28px rgba(0, 0, 0, .2), 0 10px 10px rgba(0, 0, 0, .2);
                position: relative;
                overflow: hidden;
                width: 768px;
                max-width: 100%;
                min-height: 480px;
            }

            .form-container form {
                background: #fff;
                display: flex;
                flex-direction: column;
                padding:  0 50px;
                height: 100%;
                justify-content: center;
                align-items: center;
                text-align: center;
            }

            .social-container {
                margin: 20px 0;
            }

            .social-container a {
                border: 1px solid #ddd;
                border-radius: 50%;
                display: inline-flex;
                justify-content: center;
                align-items: center;
                margin: 0 5px;
                height: 40px;
                width: 40px;
            }

            .form-container input {
                background: #eee;
                border: none;
                padding: 12px 15px;
                margin: 8px 0;
                width: 100%;
            }

            button {
                border-radius: 20px;
                border: 1px solid #ff4b2b;
                background: rgb(1,57,79);
                color: #fff;
                font-size: 12px;
                font-weight: bold;
                padding: 12px 45px;
                letter-spacing: 1px;
                text-transform: uppercase;
                transition: transform 80ms ease-in;
            }

            input[type=submit]{
                cursor: pointer;
                text-align: center;
                width: 140px;
                border-radius: 20px;
                border: 1px solid #ff4b2b;
                background: rgb(1,57,79);
                color: #fff;
                font-size: 12px;
                font-weight: bold;
                padding: 12px 45px;
                letter-spacing: 1px;
                text-transform: uppercase;
                transition: transform 80ms ease-in;
            }



            .footer {
                margin-top: 25px;
                text-align: center;
            }


            .icons {
                display: flex;
                width: 30px;
                height: 30px;
                letter-spacing: 15px;
                align-items: center;
            }
            .signupRole{
                font-family: "Handlee", cursive;
                justify-content: center;
                margin: auto 0;
                padding-top: 70px;
            }
            .choose-role p{
                font-size: 20px;
            }
            .choose-role a{
                font-size: 35px;
            }
            .form-container form label{
                text-align:left;
            }
        </style>
    </head>
    <body>
        <%
            String path = request.getContextPath();
            request.setAttribute("path", path);
        %>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
              integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf"
              crossorigin="anonymous">

        <div class="container" id="container">

            <c:if test="${Part==null}">   
                <div class="form-container sign-in-container">
                    <form action="${path}/signupstudent?action=1" method="post">
                        <p style="text-align: center">Please enter your email to get the code active account</p>
                        <input style="width: 80%; margin: 0 auto" type="email" placeholder="Enter Email" name="txtEmail" /> <br/><!-- comment -->   
                        <c:if test="${msg!=null}">
                            <c:out value="This is not your Email"></c:out><br/> 
                        </c:if>
                        <input type="submit" style="cursor: pointer; padding: 15px" value="Get Code">
                    </form>
                </div>             
            </c:if>
            <c:if test="${Part==2}">
                <div class="form-container sign-in-container text-center">
                    <form action="${path}/signupstudent?action=2" method="post">
                        <p>Enter your code active</p>
                        <input type="text" placeholder="Enter Code" name="txtCode" /> <br/><!-- comment --> 
                        <c:if test="${msg!=null}">
                            <c:out value="Code is wrong"></c:out><br/> 
                        </c:if>
                        <input type="submit" style="cursor: pointer; padding: 15px" value="Enter Code">
                    </form>
                </div>
            </c:if>
            <c:if test="${Part==3}">
                <div class="form-container sign-up-container">
                    <form action="${path}/signupstudent?action=3" method="post">
                        <h1>Create Account For Student</h1>
                        <div class="social-container">
                            <a href="#" class="social"><i class="fab fa-facebook-f"></i></a>
                            <a href="#" class="social"><i class="fab fa-google-plus-g"></i></a>
                            <a href="#" class="social"><i class="fab fa-linkedin-in"></i></a>
                        </div>
                        <span>or use your email for registration</span>
                        <input type="text" placeholder="Name" name="txtName" value="${Name}" required/>
                        <input type="date" placeholder="Birth of Date" name="txtDate" value="${BOD}"  required">
                        <select style="background: #eee;
                            border: none;
                            padding: 12px 15px;
                            margin: 8px 0;
                            width: 100%;" name="txtGender" required>
                        <option value="M">Male</option>
                        <option value="F">Female</option>
                    </select>
                        <input type="text" placeholder="Address" name="txtAddress" value="${Address}" required/>
                        <input type="text" placeholder="Phone Number" name="txtPhone" value="${Phone}" required/>
                        <%
                           if((String)request.getAttribute("error-Phone") != null){
                        %>
                        <span class="msg-error">Numbers Phone must be 10 digits long</span><br/>
                        <%
                            }
                           if((String)request.getAttribute("error-phone-exist") != null){
                        %>
                        <span class="msg-error">Numbers Phone is exist !</span><br/>
                        <%
                            }
                        %>
                        <input type="email" placeholder="Email" name="txtEmail" value="${Email}" required/>

                        <%
                            if((String)request.getAttribute("error-email") != null){
                        %>
                        <span class="msg-error">Email is exist!</span><br/>
                        <%
                            }
                        %>
                        <input type="text" placeholder="Username" name="txtUsername" value="${Username}" required/>
                        <%
                            if((String)request.getAttribute("error-username") != null){
                        %>
                        <span class="msg-error">Username is exist!</span><br/>
                        <%
                            }
                        %>
                        <input type="password" placeholder="Password" name="txtPassword" value="${Password}" required/>
                        <input type="password" placeholder="Re-Password" name="txtRePassword" value="${RePassword}" required/>
                        <%
                            if((String)request.getAttribute("error-Re-Password") != null){
                        %>
                        <span class="msg-error">Re-Password is wrong</span><br/>
                        <%
                            }
                        %>
                        <input style="background-color: rgb(1,57,79); color: white; width: 50%; border-radius:5px" type="submit" value="Sign up" />
                        <!--                    <button>Sign Up</button>-->
                        <a href="<%=path%>/signin.jsp">Sign in</a>
                    </form>
                </div>
            </c:if>
        </div>

        <div class="footer">
            <b>	Follow me on </b>
            <div class="icons">
                <a href="https://github.com/kvaibhav01" target="_blank" class="social"><i class="fab fa-github"></i></a>
                <a href="https://www.instagram.com/vaibhavkhulbe143/" target="_blank" class="social"><i class="fab fa-instagram"></i></a>
                <a href="https://medium.com/@vaibhavkhulbe" target="_blank" class="social"><i class="fab fa-medium"></i></a>
                <a href="https://twitter.com/vaibhav_khulbe" target="_blank" class="social"><i class="fab fa-twitter-square"></i></a>
                <a href="https://linkedin.com/in/vaibhav-khulbe/" target="_blank" class="social"><i class="fab fa-linkedin"></i></a>
            </div>
        </div>
    </body>
</html>
