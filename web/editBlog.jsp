<%@include file="./template/header.jsp" %>
<%
        int blogID = Integer.parseInt(request.getParameter("BlogID"));
        ArrayList<BlogPart> BlogPartList = new BlogDAO().getBlogPart(blogID);
        dal.Blog blog = new BlogDAO().getBlogBySql("Select * from Blog where BlogID = " + blogID);
                      
        request.setAttribute("Blog", blog);
        request.setAttribute("BlogParts", BlogPartList);
%>
<!-- Header Start -->
<div class="container-fluid bg-primary mb-5">
    <div class="d-flex flex-column align-items-center justify-content-center" style="min-height: 400px">
        <h3 class="display-3 font-weight-bold text-white">Blog Editor</h3>
    </div>
</div>
<!-- Header End -->


<!-- Detail Start -->
<div class="container py-5">
    <form action="${path}/blog/manage?edit=true" method="post">
        <input type="text" name="BlogID" value="${Blog.getBlogID()}" hidden>
        <div class="row pt-5">
            <div class="col-lg-10">
                <div class="d-flex flex-column text-left mb-3">                  
                    <p class="section-title pr-5"><span class="pr-2">Blog Detail Page</span></p>
                    Blog title
                    <h1 class="mb-3"><textarea id="id" name="BlogTitle" rows="1" cols="50">${Blog.getBlogTitle()}</textarea>                </h1>            
                    <h1 class="mb-3"></h1>                    
                </div>

                Blog description
                <h2 class="mb-3" style="font-size: 24px"><textarea id="id" name="BlogDescription" rows="1" cols="80">${Blog.getBlogDesciption()}</textarea> 
                    <div class="mb-5">
                        <img class="img-fluid rounded w-75 mb-4" src="<c:url value="/displayImage?FileID=${Blog.getImage()}&Type=jpeg"/>" alt="Image"><br/>
                        <c:forEach items="${BlogParts}" var="item">
                            Part ${item.getPart()}
                            <p style="font-size: 20px">Part ${item.getPart()} header</p>
                            <h2 class="mb-4"><textarea id="id" name="part-${item.getPart()}-header" rows="1" cols="60">${item.getPartHeader()}</textarea></h2>                 
                            <p style="font-size: 20px">Part ${item.getPart()} image</p>
                            <img class="img-fluid rounded w-75  mr-4 mb-3" src="<c:url value="/displayImage?FileID=${item.getImage()}&Type=jpeg"/>" alt="Image">                             
                            <p style="font-size: 20px">Part ${item.getPart()} content</p>
                            <p style="font-size:24px"><textarea class="" id="id" name="part-${item.getPart()}-content" rows="10" cols="80">${item.getPartContent()}</textarea></p>
                            </c:forEach>               
                    </div>        
            </div>

        </div>
        <a style="width: 200px; background-color: #ff4d4d; color: white" href="<c:url value="/blog/manage?delete=true&BlogID=${Blog.getBlogID()}"/>" class="btn btn-light px-4 mx-auto my-2 mb-4">&#9644; Delete Blog</a><br/>
        <input class="btn btn-primary px-4 mx-auto w-25 ml-3" type="submit" value="Save changes">
        <a class="btn btn-primary px-4 mx-auto w-25 ml-3" href="<c:url value="/manageBlog.jsp"/>">Cancel</a>
    </form>
</div>


<!-- Detail End -->
<%@include file="./template/footer.jsp" %>
