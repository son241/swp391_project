<%@include file="./template/header.jsp" %>
<%--<c:if test="${cookie.username.getValue() eq ''}">
    <c:redirect url="/401Page.jsp"/>
</c:if>--%>
<div class="container-fluid pt-5">
    <div class="container">
        <div class="text-center pb-2">
            <p class="section-title px-5"><span class="px-2">Welcome, <c:out value="${user eq null ? 'somebody' : user.fullName }"/>...</span></p>
            <h1 class="mb-4">Just a few steps, you will get a new password</h1>
        </div>
    </div>

    <div class="col-lg-4 mb-4" style="padding: 4rem; border: 2px dashed black; border-radius: 10px; background-color: #f8f9fa; margin: 0 auto">

        <h3 class="text-primary mb-5" style="text-align: center">Change password</h3>
        <form action="<c:url value="/changepassword"/>" method="post">
            <span class="${aa}">${bb}</span>
            <div class="form-group" >
                <input style="background-color: #cbe2ff" name="oldpass" type="password" class="form-control border-0 py-4" placeholder="Your old Password" required="required" />
            </div>
            <div class="form-group">
                <input style="background-color: #cbe2ff" name="newpass" type="password" class="form-control border-0 py-4" placeholder="Your new Password"
                       required="required" />
            </div>
            <div class="form-group">
                <input style="background-color: #cbe2ff" name="renewpass" type="password" class="form-control border-0 py-4" placeholder="Confirm your new Password"
                       required="required" />
            </div>
            <br>
            <div style="width: 50%; margin: 0 auto">
                <button class="btn btn-primary btn-block border-0 py-3" type="submit">Change</button>
            </div>
        </form>
    </div>
</div>
<%@include file="./template/footer.jsp" %>


<!-- Back to Top -->
<a href="#" class="btn btn-primary p-3 back-to-top"><i class="fa fa-angle-double-up"></i></a>


<!-- JavaScript Libraries -->
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
<script src="lib/easing/easing.min.js"></script>
<script src="lib/owlcarousel/owl.carousel.min.js"></script>
<script src="lib/isotope/isotope.pkgd.min.js"></script>
<script src="lib/lightbox/js/lightbox.min.js"></script>

<!-- Contact Javascript File -->
<script src="mail/jqBootstrapValidation.min.js"></script>
<script src="mail/contact.js"></script>

<!-- Template Javascript -->
<script src="js/main.js"></script>
</body>

</html>