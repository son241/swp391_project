<%@include file="./template/header.jsp" %>

<style>
    .account-settings .user-profile {
        margin: 0 0 1rem 0;
        padding-bottom: 1rem;
        text-align: center;
    }
    .account-settings .user-profile .user-avatar {
        margin: 0 0 1rem 0;
    }
    .account-settings .user-profile .user-avatar img {
        width: 90px;
        height: 90px;
        -webkit-border-radius: 100px;
        -moz-border-radius: 100px;
        border-radius: 100px;
    }
    .account-settings .user-profile h5.user-name {
        margin: 0 0 0.5rem 0;
    }
    .account-settings .user-profile h6.user-email {
        margin: 0;
        font-size: 0.8rem;
        font-weight: 400;
    }
    .account-settings .about {
        margin: 1rem 0 0 0;
        font-size: 0.8rem;
        text-align: center;
    }
    .card {
        background: #2596be;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border-radius: 5px;
        border: 0;
        margin-bottom: 1rem;
    }
    .form-control {
        border: 1px solid #596280;
        -webkit-border-radius: 2px;
        -moz-border-radius: 2px;
        border-radius: 2px;
        font-size: .825rem;
        background: white;
    }
</style>

<div class="container">
    <div class="row gutters mt-md-3">
        <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
            <div class="card h-100">
                <div class="card-body">
                    <div class="account-settings">
                        <div class="user-profile">
                            <div class="user-avatar">
                                <img src="<c:url value="/displayImage?FileID=${User.getProfileImage()}&Type=jpeg"/>" alt="Image">
                            </div>
                            <h5 class="user-name text-light">${User.getFullName()}</h5>
                        </div>
                        <div class="about text-light">
                            <h5 class="mb-2 text-light">Description</h5><br/>
                            <p class="text-light">${User.getDescription()}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">
            <div class="card h-100">
                <div class="card-body">
                    <div class="row gutters">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <h4 class="mb-3 text-light">Personal Details</h4>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="fullName" class="text-light">Gender: <c:out value="${User.getGender() eq 'M' ? 'Male' : 'Female'}"/></label><br/>
                                <p class="text-light"></p>     
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="eMail" class="text-light">Email: ${User.getEmail()}</label><br/>

                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="phone" class="text-light">Phone: ${User.getPhoneNumber()}</label><br/>

                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="Birth Date" class="text-light">Birth Date: ${User.getBirthDate()}</label><br/>

                            </div>
                        </div>
                    </div>
                    <div class="row gutters">                       
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="Street" class="text-light">Address: ${User.getAddress()}</label><br/>

                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="ciTy" class="text-light">Subject: ${SubjectDAO.getSubject(User.getSubjectID()).getSubjectName()}</label><br/>

                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="sTate" class="text-light">Level: ${LevelDAO.getLevel(User.getLevelID()).getLevelName()}</label><br/>

                            </div>
                        </div>   

                    </div>
                    <div class="row gutters">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">

                            <div class="text-right">
                                <a href="<c:url value="/displayImage?FileID=${User.getCVFile()}&Type=pdf"/>"  class="btn btn-light pl-4 pr-4 mr-3">View CV</a>
                                <a href="<c:url value="/tutor/request?accept=true&tutorID=${User.getTutorID()}"/>" class="btn btn-light">Accept Tutor</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<%@include file="./template/footer.jsp" %>