<%@page import="model.*" %>
<%@page import="controllers.*" %>
<%@page import="dal.*" %>
<%@page import="java.util.*" %>
<%@include file="./template/header.jsp" %>
<%
            String path = request.getContextPath();
            request.setAttribute("path", path);
            ArrayList<Level> listLevel = new LevelDAO().getListLevel();
            ArrayList<Subject> listSubject = new SubjectDAO().getListSubject();
            ArrayList<Slot> listSlot = new SlotDAO().getListSlot();
            ArrayList<Student> listChildren = new StudentDAO().getListChildren(1);
            request.setAttribute("listLevel",listLevel);
            request.setAttribute("listSubject",listSubject);
            request.setAttribute("listSlot",listSlot);
            request.setAttribute("listChildren",listChildren);
%>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
      integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf"
      crossorigin="anonymous">

<div class="container" id="container">

        <div class="form-container sign-up-container">
            <h3>Register to choose a subject</h3>
            <form action="hiretutor1?action=1" method="post">
                <label>Registration for</label>
                <select name="txtChildren">
                    <c:forEach items="${listChildren}" var="i">
                        <option value="${i.getStudentID()}">${i.getFullName()}</option>
                    </c:forEach>
                </select><br/>

                <label>Level</label>
                <select name="txtLevel" >
                    <c:forEach items="${listLevel}" var="i">
                        <option value="${i.getLevelID()}">${i.getLevelName()}</option>
                    </c:forEach>
                </select><br/>
                <label>Subject</label>
                <select name="txtSubject">
                    <c:forEach items="${listSubject}" var="i">
                        <option value="${i.getSubjectID()}">${i.getSubjectName()}</option>
                    </c:forEach>
                </select><br/>
                <label>Number Of Slot</label>
                <input style="width: 37%" type="number" name="txtNumberOfSlot" placeholder="number of slots greater than 0 is less than 30" value="${NumberOfSlot}"><br/>
                <input type="date" name="txtStart" value="${Date}">
                <label>Number Of Sessions A Week</label>
                <select name="txtNumberOfSessionsAWeek">
                    <option value="1">1 Sessions</option>
                    <option value="2">2 Sessions</option>
                    <option value="3">3 Sessions</option>
                    <option value="4">4 Sessions</option>
                    <option value="5">5 Sessions</option>
                </select>  <br/>
                <c:if test="${Part==null}">
                    <input type="submit" value="Submit">
                </c:if>
            </form>    
        </div>
    <c:if test="${Part==1}">
        <form action="hiretutor1?action=2" method="post">
            <c:forEach begin="1" end="${NumberOfSessionsAWeek}" step="1" var="i">
                <label>Sessions ${i}</label>
                <select name="txtDayOfWeek${i}">
                    <option value="Monday">Monday</option>
                    <option value="Tuesday">Tuesday</option>
                    <option value="Wednesday">Wednesday</option>
                    <option value="Thursday">Thursday</option>
                    <option value="Friday">Friday</option>
                    <option value="Saturday">Saturday</option>
                    <option value="Sunday">Sunday</option>
                </select>
                <select name="txtSlot${i}">
                    <c:forEach items="${listSlot}" var="i">
                        <option value="${i.getSlotID()}">${i.getSlotName()}</option><br/>
                    </c:forEach>
                </select><br/>
            </c:forEach>
                <input type="submit" value="Submit">
        </form>
    </c:if>
</div>
<%@include file="./template/footer.jsp" %>


<!-- Back to Top -->
<a href="#" class="btn btn-primary p-3 back-to-top"><i class="fa fa-angle-double-up"></i></a>


<!-- JavaScript Libraries -->
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
<script src="lib/easing/easing.min.js"></script>
<script src="lib/owlcarousel/owl.carousel.min.js"></script>
<script src="lib/isotope/isotope.pkgd.min.js"></script>
<script src="lib/lightbox/js/lightbox.min.js"></script>

<!-- Contact Javascript File -->
<script src="mail/jqBootstrapValidation.min.js"></script>
<script src="mail/contact.js"></script>

<!-- Template Javascript -->
<script src="js/main.js"></script>
</body>

</html>
