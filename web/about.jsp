<%@include file="./template/header.jsp" %>
<!-- Header Start -->
<div class="container-fluid bg-primary mb-5">
    <div class="d-flex flex-column align-items-center justify-content-center" style="min-height: 400px">
        <h3 class="display-3 font-weight-bold text-white">About Us</h3>
        <div class="d-inline-flex text-white">
            <p class="m-0"><a class="text-white" href="">Home</a></p>
            <p class="m-0 px-2">/</p>
            <p class="m-0">About Us</p>
        </div>
    </div>
</div>
<script src="https://kit.fontawesome.com/9d7371b486.js" crossorigin="anonymous"></script>
<!-- Header End -->


<!-- About Start -->
<div class="container-fluid py-5">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-5">
                <img class="img-fluid rounded mb-5 mb-lg-0" src="${path}/assets/img/about-1.jpg" alt="">
                
            </div>
            <div class="col-lg-7">
                <p class="section-title pr-5"><span class="pr-2">Learn About Us</span></p>
                <h1 class="mb-4">The best tutoring system for you</h1>
                <p>Established on July 1, 2023, ABC company is considered as one of the world's leading quality tutoring companies. With a team of professional, experienced, well-known tutors, and unique teaching methods, ABC company will give you have a solid knowledge base and easily pass the subjects at school.</p>
                <div class="row pt-2 pb-4">
                    <div class="col-6 col-md-4">
                        <img class="img-fluid rounded" src="${path}/assets/img/aboutTutorImg22.jpg" alt="">
                    </div>
                    <div class="col-6 col-md-8">
                        <ul class="list-inline m-0">
                            <li class="py-2 border-top border-bottom"><i class="fa fa-check text-primary mr-3"></i>High quality, experienced tutors</li>
                            <li class="py-2 border-bottom"><i class="fa fa-check text-primary mr-3"></i>Smart learning and progress tracking system</li>
                            <li class="py-2 border-bottom"><i class="fa fa-check text-primary mr-3"></i>Top 100 tutoring systems in the world</li>
                        </ul>
                    </div>
                </div>
                <a href="" class="btn btn-primary mt-2 py-2 px-4">Learn More</a>
            </div>
        </div>
    </div>
</div>
<!-- About End -->


<!-- Facilities Start -->
<div class="container-fluid pt-5">
    <div class="container pb-3">
        <div class="row">
            <div class="col-lg-4 col-md-6 pb-1">
                <div class="d-flex bg-light shadow-sm border-top rounded mb-4" style="padding: 40px;">
                    <svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-star"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>

                    <div class="pl-4">
                        <h4>Rating tutors</h4>
                        <p class="m-0">During the learning process, students can rate the tutor in the form of star votes and comments</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 pb-1">
                <div class="d-flex bg-light shadow-sm border-top rounded mb-4" style="padding: 40px;">
                    <svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bar-chart"><line x1="12" y1="20" x2="12" y2="10"></line><line x1="18" y1="20" x2="18" y2="4"></line><line x1="6" y1="20" x2="6" y2="16"></line></svg>
                    <div class="pl-4">
                        <h4>Study process</h4>
                        <p class="m-0">Tutors can assess student's level, and parents can monitor their child's learning progress</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 pb-1">
                <div class="d-flex bg-light shadow-sm border-top rounded mb-4" style="padding: 40px;">
                    <svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-list"><line x1="8" y1="6" x2="21" y2="6"></line><line x1="8" y1="12" x2="21" y2="12"></line><line x1="8" y1="18" x2="21" y2="18"></line><line x1="3" y1="6" x2="3.01" y2="6"></line><line x1="3" y1="12" x2="3.01" y2="12"></line><line x1="3" y1="18" x2="3.01" y2="18"></line></svg>
                    <div class="pl-4">
                        <h4>Effective learning path</h4>
                        <p class="m-0">Experienced tutors will offer the most effective and appropriate learning path for students</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 pb-1">
                <div class="d-flex bg-light shadow-sm border-top rounded mb-4" style="padding: 40px;">
                    <svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-message-circle"><path d="M21 11.5a8.38 8.38 0 0 1-.9 3.8 8.5 8.5 0 0 1-7.6 4.7 8.38 8.38 0 0 1-3.8-.9L3 21l1.9-5.7a8.38 8.38 0 0 1-.9-3.8 8.5 8.5 0 0 1 4.7-7.6 8.38 8.38 0 0 1 3.8-.9h.5a8.48 8.48 0 0 1 8 8v.5z"></path></svg>
                    <div class="pl-4">
                        <h4>Interact with tutors</h4>
                        <p class="m-0">Students can interact with tutors outside of school hours and do their assigned assignments on the web at their convenience</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 pb-1">
                <div class="d-flex bg-light shadow-sm border-top rounded mb-4" style="padding: 40px;">
                    <svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-up"><path d="M14 9V5a3 3 0 0 0-3-3l-4 9v11h11.28a2 2 0 0 0 2-1.7l1.38-9a2 2 0 0 0-2-2.3zM7 22H4a2 2 0 0 1-2-2v-7a2 2 0 0 1 2-2h3"></path></svg>
                    <div class="pl-4">
                        <h4>Suitable tutor</h4>
                        <p class="m-0">Tutor's information is public, you can choose tutors that match your selection criteria</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 pb-1">
                <div class="d-flex bg-light shadow-sm border-top rounded mb-4" style="padding: 40px;">
                    <svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-dollar-sign"><line x1="12" y1="1" x2="12" y2="23"></line><path d="M17 5H9.5a3.5 3.5 0 0 0 0 7h5a3.5 3.5 0 0 1 0 7H6"></path></svg>
                    <div class="pl-4">
                        <h4>Refund if not satisfied</h4>
                        <p class="m-0">If you are not satisfied with the tutor, please rate and complain to us, we will refund you</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Facilities Start -->


<!-- Team Start -->
<div class="container-fluid pt-5">
    <div class="container">
        <div class="text-center pb-2">
            <p class="section-title px-5"><span class="px-2">Our Founders</span></p>
            <h1 class="mb-4">The founders of ABC</h1>
        </div>
        <div class="row">
            <div class="col-md-6 col-lg-1 text-center team mb-5"></div>
            <div class="col-md-6 col-lg-3 text-center team mb-5">
                <div class="position-relative overflow-hidden mb-4" style="border-radius: 100%;">
                    <img class="img-fluid w-100" src="${path}/assets/img/team-1.jpg" alt="" >
                    <div
                        class="team-social d-flex align-items-center justify-content-center w-100 h-100 position-absolute">
                        <a class="btn btn-outline-light text-center mr-2 px-0" style="width: 38px; height: 38px;"
                           href="#"><i class="fab fa-twitter"></i></a>
                        <a class="btn btn-outline-light text-center mr-2 px-0" style="width: 38px; height: 38px;"
                           href="#"><i class="fab fa-facebook-f"></i></a>
                        <a class="btn btn-outline-light text-center px-0" style="width: 38px; height: 38px;"
                           href="#"><i class="fab fa-linkedin-in"></i></a>
                    </div>
                </div>
                <h4>Bui Hoai Nam</h4>
                <i>Co-Founder</i>
            </div>
            <div class="col-md-6 col-lg-4 text-center team mb-5">
                <div class="position-relative overflow-hidden mb-4" style="border-radius: 100%;">
                    <img class="img-fluid w-100" src="${path}/assets/img/team-2.jpg" alt="" >
                    <div
                        class="team-social d-flex align-items-center justify-content-center w-100 h-100 position-absolute">
                        <a class="btn btn-outline-light text-center mr-2 px-0" style="width: 38px; height: 38px;"
                           href="#"><i class="fab fa-twitter"></i></a>
                        <a class="btn btn-outline-light text-center mr-2 px-0" style="width: 38px; height: 38px;"
                           href="#"><i class="fab fa-facebook-f"></i></a>
                        <a class="btn btn-outline-light text-center px-0" style="width: 38px; height: 38px;"
                           href="#"><i class="fab fa-linkedin-in"></i></a>
                    </div>
                </div>
                <h4>Nguyen Van Son</h4>
                <i>Leader</i>
            </div>
            <div class="col-md-6 col-lg-3 text-center team mb-5">
                <div class="position-relative overflow-hidden mb-4" style="border-radius: 100%;">
                    <img class="img-fluid w-100" src="${path}/assets/img/team-3.jpg" alt="" >
                    <div
                        class="team-social d-flex align-items-center justify-content-center w-100 h-100 position-absolute">
                        <a class="btn btn-outline-light text-center mr-2 px-0" style="width: 38px; height: 38px;"
                           href="#"><i class="fab fa-twitter"></i></a>
                        <a class="btn btn-outline-light text-center mr-2 px-0" style="width: 38px; height: 38px;"
                           href="#"><i class="fab fa-facebook-f"></i></a>
                        <a class="btn btn-outline-light text-center px-0" style="width: 38px; height: 38px;"
                           href="#"><i class="fab fa-linkedin-in"></i></a>
                    </div>
                </div>
                <h4>Nguyen Huu Quang</h4>
                <i>Co-Founder</i>
            </div>
            <div class="col-md-6 col-lg-1 text-center team mb-5"></div>
        </div>



        <div class="row">
            <div class="col-md-6 col-lg-3 text-center team mb-5"></div>
            <div class="col-md-6 col-lg-3 text-center team mb-5">
                <div class="position-relative overflow-hidden mb-4" style="border-radius: 100%;">
                    <img class="img-fluid w-100" src="${path}/assets/img/team-1.jpg" alt="" >
                    <div
                        class="team-social d-flex align-items-center justify-content-center w-100 h-100 position-absolute">
                        <a class="btn btn-outline-light text-center mr-2 px-0" style="width: 38px; height: 38px;"
                           href="#"><i class="fab fa-twitter"></i></a>
                        <a class="btn btn-outline-light text-center mr-2 px-0" style="width: 38px; height: 38px;"
                           href="#"><i class="fab fa-facebook-f"></i></a>
                        <a class="btn btn-outline-light text-center px-0" style="width: 38px; height: 38px;"
                           href="#"><i class="fab fa-linkedin-in"></i></a>
                    </div>
                </div>
                <h4>Tran Huu Minh</h4>
                <i>Co-Founder</i>
            </div>

            <div class="col-md-6 col-lg-3 text-center team mb-5">
                <div class="position-relative overflow-hidden mb-4" style="border-radius: 100%;">
                    <img class="img-fluid w-100" src="${path}/assets/img/team-2.jpg" alt="" >
                    <div
                        class="team-social d-flex align-items-center justify-content-center w-100 h-100 position-absolute">
                        <a class="btn btn-outline-light text-center mr-2 px-0" style="width: 38px; height: 38px;"
                           href="#"><i class="fab fa-twitter"></i></a>
                        <a class="btn btn-outline-light text-center mr-2 px-0" style="width: 38px; height: 38px;"
                           href="#"><i class="fab fa-facebook-f"></i></a>
                        <a class="btn btn-outline-light text-center px-0" style="width: 38px; height: 38px;"
                           href="#"><i class="fab fa-linkedin-in"></i></a>
                    </div>
                </div>
                <h4>Bui Viet Hung</h4>
                <i>Co-Founder</i>
            </div>
        </div>
        <div class="col-md-6 col-lg-3 text-center team mb-5"></div>
    </div>
</div>
<!-- Team End -->


<%@include file="./template/footer.jsp" %>
