<%@include file="./template/header.jsp" %>
<%
    int pageTh = 1;
    try {
        pageTh = Integer.parseInt(request.getParameter("page"));
    } catch (Exception e) {
        pageTh = 1;
    };
    String sql;
    int categoryID;
    try {
        categoryID = Integer.parseInt(request.getParameter("categoryID"));
        sql = "select * from Blog where CategoryID = " + categoryID;
    } catch (Exception e) {
        sql = "select * from Blog order by BlogID desc";
    };
     
    ArrayList<Blog> allItem = new BlogDAO().getBlogListBySql(sql);
    int numberOfPage = allItem.size() % 6 == 0 ? allItem.size() / 6 : allItem.size() / 6 + 1;
    ArrayList<Blog> blogs = new MyGeneric<Blog>(allItem).dataPaging(pageTh, 6);
    request.setAttribute("currentPage", pageTh);
    request.setAttribute("blogs", blogs);
    request.setAttribute("numberOfPage", numberOfPage);
%>
<!-- Header Start -->
<div class="container-fluid bg-primary mb-5">
    <div class="d-flex flex-column align-items-center justify-content-center" style="min-height: 400px">
        <h3 class="display-3 font-weight-bold text-white">Our Blog</h3>
        <div class="d-inline-flex text-white">
            <p class="m-0"><a class="text-white" href="">Home</a></p>
            <p class="m-0 px-2">/</p>
            <p class="m-0">Our Blog</p>
        </div>
    </div>
</div>
<!-- Header End -->


<!-- Blog Start -->
<div class="container-fluid pt-5">
    <div class="container">
        <div class="text-center pb-2">
            <p class="section-title px-5"><span class="px-2">Latest Blog</span></p>
            <h1 class="mb-4">Latest Articles From Blog</h1>
        </div>
        <div class="row pb-3">
            <c:forEach items="${blogs}" var="item">
                <div class="col-lg-4 mb-4">
                    <div class="card border-0 shadow-sm mb-2">
                        <img class="card-img-top mb-2" src="<c:url value="/displayImage?FileID=${item.getImage()}&Type=jpeg"/>" alt="">
                        <div class="card-body bg-light text-center p-4">
                            <h4 class="">${item.getBlogTitle()}</h4>                           
                            <p>${item.getBlogDesciption()}</p>
                            <a href="${path}/single.jsp?BlogID=${item.getBlogID()} " class="btn btn-primary px-4 mx-auto my-2">Read More</a>
                        </div>
                    </div>
                </div>
            </c:forEach>      
        </div>
        <div class="col-md-12 mb-4">
            <nav aria-label="Page navigation">
                <ul class="pagination justify-content-center mb-0">
                    <li class="page-item">
                        <c:if test="${currentPage ne 1}">
                            <a class="page-link" href="${path}/blog.jsp?page=${i - 1}" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                                <span class="sr-only">Previous</span>
                            </a>
                        </c:if>
                    </li>
                    <c:forEach begin="1" end="${numberOfPage}" var="i">                            
                        <c:choose>
                            <c:when test="${i eq currentPage}">
                                <li class="page-item active"><a class="page-link" href="${path}/blog.jsp?page=${i}">${i}</a></li>
                                </c:when>
                                <c:otherwise>
                                <li class="page-item"><a class="page-link" href="${path}/blog.jsp?page=${i}">${i}</a></li>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>

                    <c:if test="${currentPage ne numberOfPage}">
                        <li class="page-item">
                            <a class="page-link" href="${path}/blog.jsp?page=${currentPage + 1}" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                                <span class="sr-only">Next</span>
                            </a>
                        </li>
                    </c:if>

                </ul>
            </nav>
        </div>
    </div>
</div>
</div>
<!-- Blog End -->
<%@include file="./template/footer.jsp" %>
