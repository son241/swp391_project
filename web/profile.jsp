<%@include file="./template/header.jsp" %>

<style>
    .account-settings .user-profile {
        margin: 0 0 1rem 0;
        padding-bottom: 1rem;
        text-align: center;
    }
    .account-settings .user-profile .user-avatar {
        margin: 0 0 1rem 0;
    }
    .account-settings .user-profile .user-avatar img {
        width: 90px;
        height: 90px;
        -webkit-border-radius: 100px;
        -moz-border-radius: 100px;
        border-radius: 100px;
    }
    .account-settings .user-profile h5.user-name {
        margin: 0 0 0.5rem 0;
    }
    .account-settings .user-profile h6.user-email {
        margin: 0;
        font-size: 0.8rem;
        font-weight: 400;
    }
    .account-settings .about {
        margin: 1rem 0 0 0;
        font-size: 0.8rem;
        text-align: center;
    }
    .card {
        background: #2596be;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border-radius: 5px;
        border: 0;
        margin-bottom: 1rem;
    }
    .form-control {
        border: 1px solid #596280;
        -webkit-border-radius: 2px;
        -moz-border-radius: 2px;
        border-radius: 2px;
        font-size: .825rem;
        background: white;
    }
</style>
<c:choose>
    <c:when test="${Acc.getRoleID() eq 2}">
        <form action="profile" method="post">
            <div class="container">
                <div class="row gutters mt-md-3">
                    <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                        <div class="card h-100">
                            <div class="card-body">
                                <div class="account-settings">
                                    <div class="user-profile">
                                        <div class="user-avatar">
                                            <img src="<c:url value="/displayImage?FileID=${User.getProfileImage()}&Type=jpeg"/>" alt="Image">
                                        </div>
                                        <h5 class="user-name text-light"><input class="text-center change" type="text" required readonly name="FullName" value="${User.getFullName()}"/></h5>
                                    </div>
                                    <div class="about text-light">
                                        <h5 class="mb-2 text-light">About</h5>
                                        <textarea id="id" name="Description" rows="2" cols="25">${User.getDescription()}</textarea>
                                        <!--<input type="text" name="Description" class="text-center change" value="">-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">
                        <div class="card h-100">
                            <div class="card-body">
                                <div class="row gutters">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <h6 class="mb-3 text-light">Personal Details</h6>
                                    </div>
                                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                        <div class="form-group">
                                            <label for="fullName" class="text-light">Gender</label>
                                            <input type="text" class="form-control change" required name="Gender" readonly value="<c:out value="${User.getGender()}"/>">           
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                        <div class="form-group">
                                            <label for="eMail" class="text-light">Email</label>
                                            <input type="email" class="form-control change" required name="Email" readonly value="${User.getEmail()}">
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                        <div class="form-group">
                                            <label for="phone" class="text-light">Phone</label>
                                            <input type="text" class="form-control change" name="PhoneNumber" value="${User.getPhoneNumber()}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                        <div class="form-group">
                                            <label for="Birth Date" class="text-light">Birth Date</label>
                                            <input type="date" name="BirthDate" required class="form-control change" value="${User.getBirthDate()}" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="row gutters">                       
                                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                        <div class="form-group">
                                            <label for="Street" class="text-light">Address</label>
                                            <input type="name" class="form-control change" required name="Address" value="${User.getAddress()}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                        <div class="form-group">
                                            <label for="ciTy" class="text-light">Subject</label>
                                            <input type="name" class="form-control" required disabled value="${SubjectDAO.getSubject(User.getSubjectID()).getSubjectName()}">
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                        <div class="form-group">
                                            <label for="sTate" class="text-light">Level</label>
                                            <input type="text" class="form-control" disabled value="${LevelDAO.getLevel(User.getLevelID()).getLevelName()}" readonly>
                                        </div>
                                    </div>                      
                                </div>
                                <div class="row gutters">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="text-right">
                                            <button type="button" id="edit" name="submit" class="btn btn-light pl-4 pr-4 text-">Edit</button>
                                            <button type="submit" name="submit" class="btn btn-light">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </c:when>
    <c:when test="${Acc.getRoleID() eq 3 or Acc.getRoleID() eq 4}">
        <form action="profile" method="post">
            <div class="container">
                <div class="row gutters mt-md-3">
                    <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                        <div class="card h-100">
                            <div class="card-body">
                                <div class="account-settings">
                                    <div class="user-profile">
                                        <div class="user-avatar">
                                            <img src="assets/img/user.jpg" alt=""/>
                                        </div>
                                        <h5 class="user-name text-light"><input class="text-center change" required type="text" readonly name="FullName" value="${User.getFullName()}"/></h5>
                                    </div>                            
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">
                        <div class="card h-100">
                            <div class="card-body">
                                <div class="row gutters">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <h6 class="mb-3 text-light">Personal Details</h6>
                                    </div>
                                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                        <div class="form-group">
                                            <label for="fullName" class="text-light">Gender</label>
                                            <input type="text" class="form-control change" required name="Gender" readonly value="${User.getGender()}"><br/>                                           
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                        <div class="form-group">
                                            <label for="eMail" class="text-light">Email</label>
                                            <input type="email" class="form-control change" name="Email" readonly value="${User.getEmail()}">
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                        <div class="form-group">
                                            <label for="phone" class="text-light">Phone</label>
                                            <input type="text" class="form-control change" name="PhoneNumber" value="${User.getPhoneNumber()}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                        <div class="form-group">
                                            <label for="Birth Date" class="text-light">Birth Date</label>
                                            <input type="date" class="form-control change" name="BirthDate" value="${User.getBirthDate()}" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="row gutters">                       
                                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                        <div class="form-group">
                                            <label for="Street" class="text-light">Address</label>
                                            <input type="name" class="form-control change" name="Address" value="${User.getAddress()}" readonly>
                                        </div>
                                    </div>

                                </div>
                                <div class="row gutters">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="text-right">
                                            <button type="button" id="edit" name="submit" class="btn btn-light pl-4 pr-4 text-">Edit</button>
                                            <button type="submit" class="btn btn-light">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </c:when>
</c:choose>


<%@include file="../template/footer.jsp" %>
<script>
    var editBtn = document.querySelector("#edit");
    var inputBoxs = document.querySelectorAll(".change");
    editBtn.onclick = function () {
        for (var item in inputBoxs) {
            console.log(inputBoxs[item]);
            inputBoxs[item].removeAttribute("readonly");
        }
    };

</script>