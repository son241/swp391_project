<%@include file="./template/header.jsp" %>
<%
        int blogID = Integer.parseInt(request.getParameter("BlogID"));
        ArrayList<BlogPart> BlogPartList = new BlogDAO().getBlogPart(blogID);
        dal.Blog blog = new BlogDAO().getBlogBySql("Select * from Blog where BlogID = " + blogID);
        ArrayList<BlogCategory> blogCategoryList = new BlogDAO().getBlogCategoryListBySql("Select * from BlogCategory");
        ArrayList<dal.Blog> relatedPost = new BlogDAO().getBlogListBySql("select * from Blog where CategoryID = " + blog.getCategoryID() + " and BlogID != " + blog.getBlogID() + " limit 4");
        ArrayList<dal.Blog> recentPost = new BlogDAO().getBlogListBySql("select * from Blog order by BlogID desc limit 4");
        ArrayList<BlogDiscussion> comments = new BlogDAO().getBlogDiscussionListBySql("Select * from BlogDiscussion where BlogID = " + blogID);
        
        request.setAttribute("imageHelper", new ImageHelper());
        request.setAttribute("AccountDAO", new AccountDAO());
        request.setAttribute("BlogDAO", new BlogDAO());
        request.setAttribute("Comments", comments);
        request.setAttribute("RecentPost", recentPost);
        request.setAttribute("RelatedPost", relatedPost);
        request.setAttribute("BlogDAO", new BlogDAO());
        request.setAttribute("BlogCategoryList", blogCategoryList);
        request.setAttribute("Blog", blog);
        request.setAttribute("BlogParts", BlogPartList);
%>
<!-- Header Start -->
<div class="container-fluid bg-primary mb-5">
    <div class="d-flex flex-column align-items-center justify-content-center" style="min-height: 400px">
        <h3 class="display-3 font-weight-bold text-white">Blog Detail</h3>
        <div class="d-inline-flex text-white">
            <p class="m-0"><a class="text-white" href="">Home</a></p>
            <p class="m-0 px-2">/</p>
            <p class="m-0">Blog Detail</p>
        </div>
    </div>
</div>
<!-- Header End -->


<!-- Detail Start -->
<div class="container py-5">
    <div class="row pt-5">
        <div class="col-lg-8">
            <div class="d-flex flex-column text-left mb-3">
                <p class="section-title pr-5"><span class="pr-2">Blog Detail Page</span></p>
                <h1 class="mb-3">${Blog.getBlogTitle()}</h1>
                <div class="d-flex">
                    <p class="mr-3"><i class="fa fa-user text-primary"></i> Admin</p>
                    <p class="mr-3"><i class="fa fa-folder text-primary"></i> Web Design</p>
                    <p class="mr-3"><i class="fa fa-comments text-primary"></i> 15</p>
                </div>
            </div>
            <div class="mb-5">
                <img class="img-fluid rounded w-100 mb-4" src="<c:url value="/displayImage?FileID=${Blog.getImage()}&Type=jpeg"/>" alt="Image">
                <c:forEach items="${BlogParts}" var="item">
                    <h2 class="mb-4">${item.getPartHeader()}</h2>
                    <c:choose>
                        <c:when test="${item.getPart() % 2 eq 0}">
                            <img class="img-fluid rounded w-50 float-left mr-4 mb-3" src="<c:url value="/displayImage?FileID=${item.getImage()}&Type=jpeg"/>" alt="Image">
                        </c:when>
                        <c:otherwise>
                            <img class="img-fluid rounded w-50 float-right mr-4 mb-3" src="<c:url value="/displayImage?FileID=${item.getImage()}&Type=jpeg"/>" alt="Image">
                        </c:otherwise>
                    </c:choose>                    
                    <p>${item.getPartContent()}</p>
                    <!--                <p>Sadipscing labore amet rebum est et justo gubergren. Et eirmod ipsum sit diam ut magna lorem. Nonumy vero labore lorem sanctus rebum et lorem magna kasd, stet amet magna accusam consetetur eirmod. Kasd accusam sit ipsum sadipscing et at at sanctus et. Ipsum sit gubergren dolores et, consetetur justo invidunt at et aliquyam ut et vero clita. Diam sea sea no sed dolores diam nonumy, gubergren sit stet no diam kasd vero.</p>
                                    <p>Voluptua est takimata stet invidunt sed rebum nonumy stet, clita aliquyam dolores vero stet consetetur elitr takimata rebum sanctus. Sit sed accusam stet sit nonumy kasd diam dolores, sanctus lorem kasd duo dolor dolor vero sit et. Labore ipsum duo sanctus amet eos et. Consetetur no sed et aliquyam ipsum justo et, clita lorem sit vero amet amet est dolor elitr, stet et no diam sit. Dolor erat justo dolore sit invidunt.</p>-->
                </c:forEach>               
            </div>

            <!-- Related Post -->
            <div class="mb-5 mx-n3">
                <h2 class="mb-4 ml-3">Related Post</h2>
                <div class="owl-carousel post-carousel position-relative">

                    <c:forEach items="${RelatedPost}" var="item">
                        <div class="d-flex align-items-center bg-light shadow-sm rounded overflow-hidden mx-3">
                            <img class="img-fluid" src="<c:url value="/displayImage?FileID=${item.getImage()}&Type=jpeg"/>" alt="image" style="width: 80px; height: 80px;">
                            <div class="pl-3">
                                <h5 class="">${item.getBlogTitle()}</h5>
                                <div class="d-flex">
                                    <small class="mr-3"><i class="fa fa-user text-primary"></i> Admin</small>
                                    <small class="mr-3"><i class="fa fa-folder text-primary"></i> Web Design</small>
                                    <small class="mr-3"><i class="fa fa-comments text-primary"></i> 15</small>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </div>

            <!-- Comment List -->
            <div class="mb-5" id="comment">
                <h2 class="mb-4"><c:out value="${Comments.size()}"/> Comment</h2>
                <c:forEach items="${Comments}" var="item">
                    <div class="media mb-4">
                        <img src="${path}/assets/img/user.jpg" alt="Image" class="img-fluid rounded-circle mr-3 mt-1" style="width: 45px;">
                        <div class="media-body">
                            <c:choose>
                                <c:when test="${item.getAccountID() eq AccountDAO.getAccount(cookie.username.getValue()).getAccountID()}">
                                    <h6>You <small><i>at ${item.getCommentDate()}</i></small></h6>
                                </c:when>
                                <c:otherwise>
                                    <h6>${BlogDAO.getUserByAcountID(item.getAccountID()).getFullName()} <small><i>at ${item.getCommentDate()}</i></small></h6>
                                </c:otherwise>
                            </c:choose>                        
                            <p>${item.getComment()}</p>
                        </div>
                    </div>    
                </c:forEach>

            </div>

            <!-- Comment Form -->
            <div class="bg-light p-5">
                <h2 class="mb-4" id="comment-form">Leave a comment</h2>
                <form action="blog" method="get">

                    <div class="form-group">
                        <label for="message">Message <sqan class="text-danger">*</span></label>
                        <input type="text" name="BlogID" hidden value="${Blog.getBlogID()}">
                        <textarea id="message" cols="30" rows="5" class="form-control" name="comment"></textarea>
                        <c:if test="${emptyComment ne null}">
                            <p class="text-danger">Invalid comment!</p>
                        </c:if>
                    </div>
                    <div class="form-group mb-0">
                        <c:if test="${cookie.username.getValue() ne null}">
                            <input type="submit" value="Leave Comment" class="btn btn-primary px-3">
                        </c:if>
                        <c:if test="${cookie.username.getValue() eq null}">
                            <a href="signin">Signin to comment</a>
                        </c:if>
                    </div>
                </form>
            </div>
        </div>

        <div class="col-lg-4 mt-5 mt-lg-0">
            <!-- Category List -->
            <div class="mb-5">
                <h2 class="mb-4">Categories</h2>
                <ul class="list-group list-group-flush">
                    <c:forEach items="${BlogCategoryList}" var="item">
                        <li class="list-group-item d-flex justify-content-between align-items-center px-0">
                            <a href="${path}/blog.jsp?categoryID=${item.getCategoryID()}">${item.getCategoryName()}</a>                       
                        </li>
                    </c:forEach>              
                </ul>
            </div>

            <!-- Single Image -->
            <div class="mb-5">
                <img src="${path}/assets/img/blog-1.jpg" alt="" class="img-fluid rounded">
            </div>

            <!-- Recent Post -->
            <div class="mb-5">
                <h2 class="mb-4">Recent Post</h2>
                <c:forEach var="item" items="${RecentPost}">
                    <div class="d-flex align-items-center bg-light shadow-sm rounded overflow-hidden mb-3">
                        <img class="img-fluid" src="<c:url value="/displayImage?FileID=${item.getImage()}&Type=jpeg"/>" alt="imgage" style="width: 80px; height: 80px;">
                        <div class="pl-3">
                            <h5 class="">${item.getBlogTitle()}</h5>
                            <div class="d-flex">
                                <small class="mr-3"><i class="fa fa-user text-primary"></i> Admin</small>
                                <small class="mr-3"><i class="fa fa-folder text-primary"></i> Web Design</small>
                                <small class="mr-3"><i class="fa fa-comments text-primary"></i> 15</small>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </div>          
        </div>
    </div>
</div>
<!-- Detail End -->
<%@include file="./template/footer.jsp" %>
