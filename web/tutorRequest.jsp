<%@include  file="./template/header.jsp"%>

<div class="container-fluid bg-primary mb-5">
    <div class="d-flex flex-column align-items-center justify-content-center" style="min-height: 400px">
        <h3 class="display-3 font-weight-bold text-white">Tutor Request</h3>       
    </div>
</div>
<!-- Blog Start -->
<div class="container-fluid pt-5">
    <div class="container">
        <form  action="blog/manage">                      
            <input class="btn btn-light px-4 mx-auto my-2 border-primary pl-xl-5 w-75 mb-2" type="text" name="Search" placeholder="Search by name" >
            <input type="submit" class="btn btn-primary px-4 mx-auto my-2" value="Search">
        </form>
        <div class="text-center pb-2">
            <p class="section-title px-5"><span class="px-2">Latest Request</span></p>            
        </div>
        <div class="row pb-3">
            <c:forEach items="${tutors}" var="item">
                <div class="col-lg-4 mb-4">
                    <div class="card border-0 shadow-sm mb-2">
                        <img class="card-img-top mb-2" src="<c:url value="/displayImage?FileID=${item.getProfileImage()}&Type=jpeg"/>" alt="">
                        <div class="card-body bg-light text-center p-4">   
                            <h4 class="">${item.getFullName()}</h4> 
                            <p>${item.getDescription()}</p>
                            <a href="${path}/tutor/request?view=true&TutorID=${item.getTutorID()}" class="btn btn-primary px-4 mx-auto my-2">View</a>
                        </div>
                    </div>
                </div>
            </c:forEach>      
        </div>
        <div class="col-md-12 mb-4">
            <nav aria-label="Page navigation">
                <ul class="pagination justify-content-center mb-0">
                    <li class="page-item">
                        <c:if test="${currentPage ne 1}">
                            <a class="page-link" href="${path}/tutor/request?page=${i - 1}" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                                <span class="sr-only">Previous</span>
                            </a>
                        </c:if>
                    </li>
                    <c:forEach begin="1" end="${numberOfPage}" var="i">                            
                        <c:choose>
                            <c:when test="${i eq currentPage}">
                                <li class="page-item active"><a class="page-link" href="${path}/tutor/request?page=${i}">${i}</a></li>
                                </c:when>
                                <c:otherwise>
                                <li class="page-item"><a class="page-link" href="${path}/tutor/request?page=${i}">${i}</a></li>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>

                    <c:if test="${currentPage ne numberOfPage}">
                        <li class="page-item">
                            <a class="page-link" href="${path}/tutor/request?page=${currentPage + 1}" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                                <span class="sr-only">Next</span>
                            </a>
                        </li>
                    </c:if>

                </ul>
            </nav>
        </div>
    </div>
</div>

<%@include file="./template/footer.jsp" %>
