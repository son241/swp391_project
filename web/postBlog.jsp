<%@include file="./template/header.jsp" %>


<!-- Header Start -->
<div class="container-fluid bg-primary mb-5">
    <div class="d-flex flex-column align-items-center justify-content-center" style="min-height: 400px">
        <h3 class="display-3 font-weight-bold text-white">Post Blog</h3>
    </div>
</div>
<!-- Header End -->


<!-- Detail Start -->
<div class="container py-5">
    <form action="${path}/blog/manage?post=true" method="post" enctype="multipart/form-data">

        <div class="row pt-5">
            <div class="col-lg-10">
                <div class="d-flex flex-column text-left mb-3">                                                 
                    <p class="section-title pr-5"><span class="pr-2">Blog Detail Page</span></p>
                    Blog Category
                    <select name="BlogCategory" class="w-25">
                        <c:forEach items="${Categories}" var="item">
                            <option value="${item.getCategoryID()}">${item.getCategoryName()}</option>                          
                        </c:forEach>
                    </select><br/>
                    Blog title
                    <h1 class="mb-3"><textarea onchange="commitPart()" id="start-post" required id="id" name="BlogTitle" rows="1" cols="35"></textarea></h1>            
                    <h1 class="mb-3"></h1>                    
                </div>

                Blog description
                <h2 class="mb-3" style="font-size: 24px"><textarea onchange="commitPart()" required id="id" name="BlogDescription" rows="1" cols="80"></textarea> 
                    <div class="mb-5">
                        <br/>
                        Blog image<br/>
                        <input type="file" onchange="commitPart()" required accept="image/jpeg, image/png" name="BlogImage">
                        <br/><br/>
                        <c:forEach begin="1" end="${Parts}" var="item">
                            <p style="font-size: 32px; font-weight: 800">Part ${item}</p>
                            <p style="font-size: 20px">Part ${item} header</p>
                            <h2 class="mb-4"><textarea onchange="commitPart()" id="id" name="part-${item}-header" rows="1" cols="60"></textarea></h2>                                           
                            <p style="font-size: 20px">Part ${item} image</p>
                            <input type="file" onchange="commitPart()" accept="image/jpeg, image/png" name="part-${item}-image">
                            <br/><br/>                             
                            <p style="font-size: 20px">Part ${item} content</p>
                            <p style="font-size:24px"><textarea onchange="commitPart()" <c:out value="${item eq 1 ? 'required' : ''}"/> class="" id="id" name="part-${item}-content" rows="10" cols="80"></textarea></p>
                            </c:forEach>               
                    </div>  
                    <a href="<c:url value="/blog/manage?post=true&addPart=true"/>" class=" change-part btn btn-light px-4 mx-auto my-2 text-primary">&#9535; Add Part</a>
                    <c:if test="${Parts gt 1}">
                        <a href="<c:url value="/blog/manage?post=true&removePart=true"/>" class="change-part btn btn-light px-4 mx-auto my-2 text-danger">&#9644; Remove Part</a>
                    </c:if>
                    <p class="text-danger mt-2" style="font-size: 12px">Commit your number of part before writing blog!</p>
            </div>

        </div>

        <input class="btn btn-primary px-4 mx-auto w-25 ml-3" type="submit" value="Post">
        <a href="<c:url value="/manageBlog.jsp"/>" class="btn btn-primary px-4 mx-25 w-25 ml-3">Cancel<a/>

    </form>
</div>

<!-- Detail End -->
<%@include file="./template/footer.jsp" %>
<script>
    
    function commitPart() {
        var changeParts = document.querySelectorAll(".change-part");
        for (var item in changeParts) {
            console.log(changeParts[item]);
            changeParts[item].style.display = "none";
        }
    };
</script>
