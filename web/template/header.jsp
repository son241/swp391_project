<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.util.*" %>
<%@page import="dal.*" %>
<%@page import="model.*" %>
<%@page import="helper.*" %>
<%
    request.setAttribute("AccountDAO", new AccountDAO());
%>
<c:if test="${AccountDAO.getAccount(cookie.username.getValue()) ne null}">
    <c:choose>
        <c:when test="${AccountDAO.getAccount(cookie.username.getValue()).getRoleID() eq 3 or AccountDAO.getAccount(cookie.username.getValue()).getRoleID() eq 4}">
            <%@include file="../template/customerHeader.jsp" %>
        </c:when>
        <c:when test="${AccountDAO.getAccount(cookie.username.getValue()).getRoleID() eq 2}">
            <%@include file="../template/tutorHeader.jsp" %>
        </c:when> 
         <c:when test="${AccountDAO.getAccount(cookie.username.getValue()).getRoleID() eq 1}">
             <%@include file="../template/adminHeader.jsp" %>
        </c:when>
    </c:choose>
</c:if>
<c:if test="${AccountDAO.getAccount(cookie.username.getValue()) eq null}">
    <%@include file="../template/guestHeader.jsp" %>
</c:if>

