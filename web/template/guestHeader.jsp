<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="dal.*" %>
<%@page import="model.*" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>

        <!-- Favicon -->
        <link href="img/favicon.ico" rel="icon">

        <!-- Google Web Fonts -->
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Handlee&family=Nunito&display=swap" rel="stylesheet">

        <!-- Font Awesome -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">

        <!-- Flaticon Font -->
        <link href="lib/flaticon/font/flaticon.css" rel="stylesheet">

        <!-- Customized Bootstrap Stylesheet -->
        <%
                    String path = request.getContextPath();
                    request.setAttribute("path", path);
        %>
        <link href="<%=path%>/assets/css/style.css" rel="stylesheet">

        <!-- Libraries Stylesheet -->
        <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
        <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">

        <!-- Customized Bootstrap Stylesheet -->

    </head>

    <body>
        <!-- Navbar Start -->
        <div class="container-fluid bg-light position-relative shadow">
            <nav class="navbar navbar-expand-lg bg-light navbar-light py-3 py-lg-0 px-0 px-lg-5">
                <a href="" class="navbar-brand font-weight-bold text-secondary" style="font-size: 50px;">
                    <i class="flaticon-032-book"></i>
                    <span class="text-primary">Tutor</span>
                </a>
                <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-between" id="navbarCollapse">
                    <div class="navbar-nav font-weight-bold mx-auto py-0">
                        <a href="<c:url value="/index.jsp"/>" class="nav-item nav-link active">Home</a>
                        <a href="<c:url value="/blog.jsp"/>" class="nav-item nav-link">Blog</a>
                        <a href="<c:url value="/about.jsp"/>" class="nav-item nav-link">About</a>                      
                        <a href="<c:url value="/contact.jsp"/>" class="nav-item nav-link">Contact</a>
                    </div>
                        <a href="<c:url value="/signin.jsp"/>" class="btn btn-primary px-4 mr-5">Sign in</a>
                        <a href="<c:url value="/signupRole.jsp"/>" class="btn btn-primary px-4">Sign up</a>
                    <!--<a href="" class="btn btn-primary px-4">Join Class</a>-->
                </div>
            </nav>
        </div>
        <!-- Navbar End -->