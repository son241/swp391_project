
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Delete product</title>
        <style>
            form {
                display: inline-block;
            }
            
            .error-msg {
                color: red;
            }
        </style>
    </head>
    <body>
        <h2>Are you sure to delete product id 1</h2>
        <form action="manage-crudPro?action=delete" method="POST">
            <input type="hidden" name="id" value="1">
            <input type="submit" value="Submit delete">
        </form>
        <a href="manage-product">
            <button type="button">Cancel</button>
        </a>
        <br>
        <p class="error-msg"></p>
    </body>
</html>
