

<%@include file="./template/header.jsp" %>
<%@page import="model.*" %>
<c:if test="${AccountDAO.getAccount(cookie.username.getValue()).getRoleID() ne 1}">
    <c:redirect url="/401Page.jsp"/>
</c:if>
        <div class="container-fluid bg-primary mb-5">
            <div class="d-flex flex-column align-items-center justify-content-center" style="min-height: 150px">
                <h3 class="display-4 font-weight-bold text-white">User List Manager</h3>
                <!--        <div class="d-inline-flex text-white">
                            <p class="m-0"><a class="text-white" href="">Home</a></p>
                            <p class="m-0 px-2">/</p>
                            <p class="m-0">Our Blog</p>
                        </div>-->
            </div>
        </div>
        <!-- Header End -->


        <!-- Blog Start -->
        <div class="container-fluid">
            <div class="container">
                <!--                <div class="text-center pb-2">
                                    <p class="section-title px-5"><span class="px-2">User List Manager</span></p>
                                    
                                </div>-->
                <div class="content-main">
                    <h3 class="display-8 font-weight-bold text-black-50">Filter by Role:</h3>
                    </br>
                    <div class="row">
                        <div class="col-lg-3">
                            <form action="userList">
                                <select class="oFilter" name="RoleID" >
                                    <!--<option value="-1" selected>All</option>-->
                                    <option value="-1" >All</option>


                                    <c:forEach items="${roleList}" var="c" >
                                        <c:choose>
                                            <c:when test="${c.roleID eq role.roleID}">
                                                <c:if test="${c.roleID ne 1}">
                                                    <option selected value="${c.roleID}">${c.roleName}</option>
                                                </c:if>
                                            </c:when>
                                            <c:otherwise>
                                                <c:if test="${c.roleID ne 1}">
                                                    <option value="${c.roleID}">${c.roleName}</option>
                                                </c:if>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>

                                </select>



                                <input class="filter" type="submit" value="Filter" style="color: white; font-weight: bolder">

                                </div>

                                <div class="col-lg-6">
                                    <div class="wrap">
                                        <div class="search">
                                            <input type="text" name="keyword" class="searchTerm" placeholder="Enter account to search?" value="${keyword}">
                                            <button type="submit" class="searchButton">
                                                <i class="fa fa-search"></i>
                                            </button>


                                        </div>
                                    </div>
                                </div>
                                <div id="col-lg-3" style="width: 20%;">
                                    <input class="filter" type="submit" value="Sort by Date" style="color: white; font-weight: bolder">

                                    <select class="oFilter" name="sort" >
                                        <!--<option value="-1" selected>All</option>-->
                                        <!--                                        <option value="1" >Asc</option>
                                                                                <option value="2" >Desc</option>-->

                                        <c:choose>
                                            <c:when test="${sort==0}">
                                                <option selected value="1">Asc</option>
                                                <option  value="2">Desc</option>
                                            </c:when>
                                            <c:when test="${sort==1}">
                                                <option selected value="1">Asc</option>
                                                <option  value="2">Desc</option>
                                            </c:when>
                                            <c:when test="${sort==2}">
                                                <option  value="1">Asc</option>
                                                <option selected value="2">Desc</option>
                                            </c:when>
                                        </c:choose>

                                    </select>

                                </div>
                            </form>
                            <!--                                <div class="col-lg-3">
                                                                <a href="manage-crudPro?action=create">Create a new Product</a>
                                                                <form action>
                                                                    <label for="upload-file">Import .xls or .xlsx file</label>
                                                                    <input type="file" name="file" id="upload-file" />
                                                                </form>
                                                            </div>-->

                        </div>

                        <div id="order-table-admin" style="margin-top: 100px;">
                            <table style="width:100%;">
                                <tr style="background-color: #00B4CC;">
                                    <th>Role</th>
                                    <th>ID</th>
                                    <th>User Name</th>
                                    <th>Creation Date</th>
                                    <th>Active</th>
                                    <th>CRUD</th>
                                </tr>
                                <c:forEach items="${accAPage}" var="a">
                                    <tr>
                                        <c:choose>
                                            <c:when test="${a.roleID==1}">
                                            </c:when>
                                            <c:otherwise>
                                                <c:if test="${a.roleID==2}">
                                                    <td>Tutor</td>
                                                    <td>${a.tutorID}</td>
                                                </c:if>
                                                <c:if test="${a.roleID==3}">
                                                    <td>Parent</td>
                                                    <td>${a.parentID}</td>
                                                </c:if>
                                                <c:if test="${a.roleID==4}">
                                                    <td>Student</td>
                                                    <td>${a.studentID}</td>
                                                </c:if>

                                                <td>${a.username}</td>
                                                <td>${a.creationDate}</td>
                                                <c:if test="${a.active==true}">
                                                    <td style="color: blue; font-weight: bold">ON</td>
                                                </c:if>
                                                <c:if test="${a.active==false}">
                                                    <td style="color: red; font-weight: bold">OFF</td>
                                                </c:if>

                                                <td>
                                                    <a class="crudButton" href="./editUserList?roleId=${a.roleID}&userName=${a.username}">Edit</a> &nbsp; | &nbsp; 
                                                    <a class="crudButton deleteBtn" style="cursor: pointer;">Delete</a></br>
                                                    <!--mmm-->
                                                    <div class="delete" style="margin-top: 10px; display: none;">
                                                        <div  style="display: flex; align-content: space-around">
                                                            <div class="Mess">Are you sure to delete this account?</div></br>
                                                        </div>

                                                        <div>
                                                            <a class="ynButton" href="<c:url value="/deleteUser?roleId=${a.roleID}&userName=${a.username}"/>">Yes</a> &nbsp; | &nbsp;
                                                            <a class="ynButton no" >No</a>
                                                        </div>
                                                    </div>
                                                    <!--mmm-->
                                                </td>
                                            </c:otherwise>
                                        </c:choose>

                                    </tr>
                                </c:forEach>

                            </table>

                            <!--                            <table style="width:100%">
                                                            <tr>
                                                                <th>Firstname</th>
                                                                <th>Lastname</th> 
                                                                <th>Age</th>
                                                            </tr>
                                                            <tr>
                                                                <td>Jill</td>
                                                                <td>Smith</td>
                                                                <td>50</td>
                                                            </tr>
                            
                                                        </table>-->
                        </div><br>
                        <script>
                            const delBtns = document.querySelectorAll('.crudButton.deleteBtn')
                            const noBtns = document.querySelectorAll('.ynButton.no')
                            const comfirms = document.querySelectorAll('.delete')
                            noBtns.forEach((btn, index) => {
                                btn.onclick = () => {

                                    comfirms[index].style.display = 'none'

                                }
                            })
                            delBtns.forEach((btn, index) => {
                                btn.onclick = () => {
                                    if (comfirms[index].style.display === 'none') {
                                        comfirms[index].style.display = 'block'
                                    } else {
                                        comfirms[index].style.display = 'none'
                                    }
                                }
                            })
//                            function show() {
//                                var x = document.getElementsByClassName("delete");
//                                if (x.style.display === "none") {
//                                    x.style.display = "block";
//                                } else {
//                                    x.style.display = "none";
//                                }
//                            }
                        </script>
                        <style>
                            .cancel-order:hover{
                                opacity: 0.7;
                                cursor: pointer;
                            }

                            .delete{
                                animation: show ease-out 1s;
                            }
                            @keyframes show {
                                from {
                                    opacity: 0.1;
                                }

                                to {
                                    opacity: 1;
                                }
                            }


                            .ynButton{
                                border: 3px solid orange;
                                color: white;
                                font-weight: bolder;
                                background-color: orange;
                                border-radius: 5px;
                            }
                            .crudButton{
                                border: 3px solid #00B4CC;
                                color: white;
                                font-weight: bolder;
                                background-color: #00B4CC;
                                height: 36px;
                                border-radius: 5px;
                            }
                            table, th, td {
                                border: 1px solid #00B4CC;
                            }
                            th{
                                color: white;
                                font-weight: bolder;
                            }

                            .oFilter{
                                height: 36px;
                                border-radius: 5px;
                            }

                            body{
                                background: #f2f2f2;
                                font-family: 'Open Sans', sans-serif;
                            }

                            .search {
                                width: 100%;
                                position: relative;
                                display: flex;
                            }

                            .searchTerm {
                                width: 100%;
                                border: 3px solid #00B4CC;
                                border-right: none;
                                height: 36px;
                                border-radius: 5px 0 0 5px;
                                outline: none;
                                color: #9DBFAF;
                            }
                            .filter{
                                border: 3px solid #00B4CC;
                                height: 36px;
                                border-radius: 5px;
                                outline: none;
                                color: #9DBFAF;
                                background-color: #00B4CC;
                            }

                            .searchTerm:focus{
                                color: #00B4CC;
                            }

                            .searchButton {
                                width: 40px;
                                height: 36px;
                                border: 1px solid #00B4CC;
                                background: #00B4CC;
                                text-align: center;
                                color: #fff;
                                border-radius: 0 5px 5px 0;
                                cursor: pointer;
                                font-size: 20px;
                            }

                            /*Resize the wrap to see the search bar change!*/
                            .wrap{
                                width: 100%;
                                position: absolute;
                                top: 0%;
                                left: 40%;
                                transform: translate(-50%, -50%);
                            }




                            #product-title-header{

                            }
                            .container {
                                width: 100%;
                                position: relative;
                            }

                            .page-wrapper {
                                margin: 0 auto;
                                width: fit-content;
                            }

                            .page {
                                padding: 4px 16px;
                                border-radius: 4px;
                                border: 1px solid sienna;
                                cursor: pointer;
                                text-decoration: none;
                                color: black;
                            }

                            .page.selected {
                                background-color: sienna;
                                color: white;
                            }

                            .director {
                                padding: 0 8px;
                                cursor: pointer;
                                font-size: 140%;
                                font-weight: bold;
                                text-decoration: none;
                                color: sienna;
                            }

                            .search {
                                position: absolute;
                                right: 0;
                                top: 0;
                            }

                            th, td {
                                padding: 2px 6px;
                            }

                            a {
                                text-decoration: none;
                            }
                        </style>
                    </div>
                    <!--xx-->



                    <div class="page-wrapper">
                        <c:if test="${pageQuantity > 1}">
                            <c:if test="${pageNumber != 1}">
                                <a href="?page=${pageNumber - 1}&keyword=${keyword}&RoleID=${RoleID}&sort=${sort}" style="color: #138496" class="director"><</a>
                            </c:if>
                            <c:forEach begin="1" end="${pageQuantity}" var="page">
                                <a href="?page=${page}&keyword=${keyword}&RoleID=${RoleID}&sort=${sort}" <c:if test="${pageNumber == page}">style="background-color: #138496"</c:if> class="page <c:if test="${pageNumber == page}">selected</c:if>">${page}</a>
                            </c:forEach>
                            <c:if test="${pageNumber != pageQuantity}">
                                <a href="?page=${pageNumber + 1}&keyword=${keyword}&RoleID=${RoleID}&sort=${sort}" style="color: #138496" class="director">></a>
                            </c:if>
                        </c:if>
                    </div><br>
                </div>
            </div>

            <!-- Blog End -->
            <!-- Footer Start -->
            <div class="container-fluid bg-secondary text-white mt-5 py-5 px-sm-3 px-md-5">
                <div class="row pt-5">
                    <div class="col-lg-3 col-md-6 mb-5">
                        <a href="" class="navbar-brand font-weight-bold text-primary m-0 mb-4 p-0" style="font-size: 40px; line-height: 40px;">
                            <i class="flaticon-032-book"></i>
                            <span class="text-white">Tutor</span>
                        </a>
                        <p>Coming to the Tutor application will help users learn and interact with the center's famous tutors, being a user-oriented website so it will be easy for parents-students to access.</p>
                        <div class="d-flex justify-content-start mt-4">
                            <a class="btn btn-outline-primary rounded-circle text-center mr-2 px-0"
                               style="width: 38px; height: 38px;" href="#"><i class="fab fa-twitter"></i></a>
                            <a class="btn btn-outline-primary rounded-circle text-center mr-2 px-0"
                               style="width: 38px; height: 38px;" href="#"><i class="fab fa-facebook-f"></i></a>
                            <a class="btn btn-outline-primary rounded-circle text-center mr-2 px-0"
                               style="width: 38px; height: 38px;" href="#"><i class="fab fa-linkedin-in"></i></a>
                            <a class="btn btn-outline-primary rounded-circle text-center mr-2 px-0"
                               style="width: 38px; height: 38px;" href="#"><i class="fab fa-instagram"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 mb-5">
                        <h3 class="text-primary mb-4">Get In Touch</h3>
                        <div class="d-flex">
                            <h4 class="fa fa-map-marker-alt text-primary"></h4>
                            <div class="pl-3">
                                <h5 class="text-white">Address</h5>
                                <p>C404L, Dorm C, FPT University</p>
                            </div>
                        </div>
                        <div class="d-flex">
                            <h4 class="fa fa-envelope text-primary"></h4>
                            <div class="pl-3">
                                <h5 class="text-white">Email</h5>
                                <p>Sonnv2412002@gmail.com</p>
                            </div>
                        </div>
                        <div class="d-flex">
                            <h4 class="fa fa-phone-alt text-primary"></h4>
                            <div class="pl-3">
                                <h5 class="text-white">Phone</h5>
                                <p>+84799263459</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 mb-5">
                        <h3 class="text-primary mb-4">Quick Links</h3>
                        <div class="d-flex flex-column justify-content-start">
                            <a class="text-white mb-2" href="#"><i class="fa fa-angle-right mr-2"></i>Home</a>
                            <a class="text-white mb-2" href="#"><i class="fa fa-angle-right mr-2"></i>About Us</a>
                            <a class="text-white mb-2" href="#"><i class="fa fa-angle-right mr-2"></i>Our Courses</a>
                            <a class="text-white mb-2" href="#"><i class="fa fa-angle-right mr-2"></i>Our Teachers</a>
                            <a class="text-white mb-2" href="#"><i class="fa fa-angle-right mr-2"></i>Our Blog</a>
                            <a class="text-white" href="#"><i class="fa fa-angle-right mr-2"></i>Contact Us</a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 mb-5">
                        <h3 class="text-primary mb-4">Letter</h3>
                        <form action="">
                            <div class="form-group">
                                <input type="text" class="form-control border-0 py-4" placeholder="Your Message" required="required" />
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control border-0 py-4" placeholder="Your Email"
                                       required="required" />
                            </div>
                            <div>
                                <button class="btn btn-primary btn-block border-0 py-3" type="submit">Submit Now</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="container-fluid pt-5" style="border-top: 1px solid rgba(23, 162, 184, .2);;">
                    <p class="m-0 text-center text-white">
                        &copy; <a class="text-primary font-weight-bold" href="#">Project</a> SWP391 By Team 5
                        <a class="text-primary font-weight-bold" href="https://htmlcodex.com">SE1641</a>
                    </p>
                </div>
            </div>
            <!-- Footer End -->




            <!-- Back to Top -->
            <a href="#" class="btn btn-primary p-3 back-to-top"><i class="fa fa-angle-double-up"></i></a>


            <!-- JavaScript Libraries -->
            <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
            <script src="lib/easing/easing.min.js"></script>
            <script src="lib/owlcarousel/owl.carousel.min.js"></script>
            <script src="lib/isotope/isotope.pkgd.min.js"></script>
            <script src="lib/lightbox/js/lightbox.min.js"></script>

            <!-- Contact Javascript File -->
            <script src="mail/jqBootstrapValidation.min.js"></script>
            <script src="mail/contact.js"></script>

            <!-- Template Javascript -->
            <script src="js/main.js"></script>
    </body>

</html>


<!-- Back to Top -->
<a href="#" class="btn btn-primary p-3 back-to-top"><i class="fa fa-angle-double-up"></i></a>


<!-- JavaScript Libraries -->
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
<script src="lib/easing/easing.min.js"></script>
<script src="lib/owlcarousel/owl.carousel.min.js"></script>
<script src="lib/isotope/isotope.pkgd.min.js"></script>
<script src="lib/lightbox/js/lightbox.min.js"></script>

<!-- Contact Javascript File -->
<script src="mail/jqBootstrapValidation.min.js"></script>
<script src="mail/contact.js"></script>

<!-- Template Javascript -->
<script src="js/main.js"></script>
</body>

</html>