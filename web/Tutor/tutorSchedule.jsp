<%@include file="../template/tutorHeader.jsp"%>

<div class="schedule">
    <div class="month">      
        <ul>
            <li class="prev">&#10094;</li>
            <li class="next">&#10095;</li>
            <li>
                August<br>
                <span style="font-size:18px">2021</span>
            </li>
        </ul>
    </div>

    <ul class="weekdays">
        <li>Mo</li>
        <li>Tu</li>
        <li>We</li>
        <li>Th</li>
        <li>Fr</li>
        <li>Sa</li>
        <li>Su</li>
    </ul>

    <ul class="days">  
        <li>1</li>
        <li>2</li>
        <li>3</li>
        <li>4</li>
        <li>5</li>
        <li>6</li>
        <li>7</li>
        <li>8</li>
        <li>9</li>
        <li><span class="active">10</span></li>
        <li>11</li>
        <li>12</li>
        <li>13</li>
        <li>14</li>
        <li>15</li>
        <li>16</li>
        <li>17</li>
        <li>18</li>
        <li>19</li>
        <li>20</li>
        <li>21</li>
        <li>22</li>
        <li>23</li>
        <li>24</li>
        <li>25</li>
        <li>26</li>
        <li>27</li>
        <li>28</li>
        <li>29</li>
        <li>30</li>
        <li>31</li>
    </ul>

    <table class="responstable w-100 mt-2" >

        <tr>
            <th>Slot</th>
            <th data-th="Driver details"><span>Student</span></th>
            <th>Learning Location</th>
            <th>Course</th>
            <th>Note</th>        
        </tr>

        <tr>
            <td>Slot 1</td>
            <td><a href="#">Steve Foo</a></td>
            <td>FPT, Thach That, Ha Noi</td>
            <td><a href="#">Math 10</a></td>
            <td><form action="action"><input type="text" name="name" required> <input type="submit" name="name" value="Add" class="btn-light" ></form></td>           
        </tr>

        <tr>
            <td>Slot 2</td>
            <td>-</td>
            <td>-</td>
            <td>-</td>
            <td><form action="action"><input type="text" name="name" required> <input type="submit" name="name" value="Add" class="btn-light" ></form></td>          
        </tr>

        <tr>
            <td>Slot 3</td>
            <td><a href="#">Stan Foo</a></td>
            <td>FPT, Thach That, Ha Noi</td>
             <td><a href="#">Physic 10</a></td>
            <td><form action="action"><input type="text" name="name" required> <input type="submit" name="name" value="Add" class="btn-light" ></form></td>          
        </tr>

        <tr>
            <td>Slot 4</td>
            <td>-</td>
            <td>-</td>
            <td>-</td>
            <td><form action="action"><input type="text" name="name" required> <input type="submit" name="name" value="Add" class="btn-light" ></form></td>  
        </tr>
    </table>
</div>

<%@include  file="../template/footer.jsp"%>