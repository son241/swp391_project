<html>
    <head>
        <title>title</title>
        <style>
            /*signin*/
            @import url('https://fonts.googleapis.com/css?family=Montserrat:400,800');

            * {
                box-sizing: border-box;
            }

            body {
                font-family: 'Montserrat', sans-serif;
                background-color: rgb(20,163,184);
                display: flex;
                flex-direction: column;
                justify-content: center;
                align-items: center;
                margin: -20px 0 50px;
                margin-top: 20px;
            }

            h1 {
                font-weight: bold;
                margin: 0;
            }

            p {
                font-size: 14px;
                font-weight: 100;
                line-height: 20px;
                letter-spacing: .5px;
                margin: 20px 0 30px;
            }

            span {
                font-size: 12px;
            }

            a {
                color: #333;
                font-size: 14px;
                text-decoration: none;
                margin: 15px 0;
            }

            .container {
                background: #fff;
                border-radius: 10px;
                box-shadow: 0 14px 28px rgba(0, 0, 0, .2), 0 10px 10px rgba(0, 0, 0, .2);
                position: relative;
                overflow: hidden;
                width: 768px;
                max-width: 100%;
                min-height: 480px;
            }

            .form-container form {
                background: #fff;
                display: flex;
                flex-direction: column;
                padding:  0 50px;
                height: 100%;
                justify-content: center;
                align-items: center;
                text-align: center;
            }

            .social-container {
                margin: 20px 0;
            }

            .social-container a {
                border: 1px solid #ddd;
                border-radius: 50%;
                display: inline-flex;
                justify-content: center;
                align-items: center;
                margin: 0 5px;
                height: 40px;
                width: 40px;
            }

            .form-container input {
                background: #eee;
                border: none;
                padding: 12px 15px;
                margin: 8px 0;
                width: 100%;
            }

            button {
                border-radius: 20px;
                border: 1px solid #ff4b2b;
                background: rgb(1,57,79);
                color: #fff;
                font-size: 12px;
                font-weight: bold;
                padding: 12px 45px;
                letter-spacing: 1px;
                text-transform: uppercase;
                transition: transform 80ms ease-in;
            }



            .footer {
                margin-top: 25px;
                text-align: center;
            }


            .icons {
                display: flex;
                width: 30px;
                height: 30px;
                letter-spacing: 15px;
                align-items: center;
            }
            .signupRole{
                font-family: "Handlee", cursive;
                justify-content: center;
                margin: auto 0;
                padding-top: 70px;
            }
            .choose-role p{
                font-size: 20px;
            }
            .choose-role a{
                font-size: 35px;
            }
            .form-container form label{
                text-align:left;
            }

            #level, #subject{
                background: #eee;
                border: none;
                padding: 12px 15px;
                margin: 8px 0;
                width: 100%;
            }
        </style>
    </head>
    <body>
        <%
            String path = request.getContextPath();
            request.setAttribute("path", path);
        %>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
              integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf"
              crossorigin="anonymous">

        <div class="container" id="container">

            <div class="form-container sign-up-container">
                <form action="${path}/signuptutor" method="post" enctype="multipart/form-data">
                    <h1>Create Account For Tutor</h1>                                
                    <input type="text" placeholder="Name" name="txtName" value="${Name}" required/>
                    <input type="date" placeholder="Birth of Date" name="txtDate" value="${BOD}" required>
                    <select style="background: #eee;
                            border: none;
                            padding: 12px 15px;
                            margin: 8px 0;
                            width: 100%;" name="txtGender" required>
                        <option value="M">Male</option>
                        <option value="F">Female</option>
                    </select>
                    <input type="text" placeholder="Address" name="txtAddress" value="${Address}"  required/>
                    <input type="text" placeholder="Phone Number" name="txtPhone" value="${Phone}"  required/>
                    <%
                       if((String)request.getAttribute("error-Phone") != null){
                    %>
                    <span class="msg-error">Numbers Phone must be 10 digits long</span><br/>
                    <%
                        }
                       if((String)request.getAttribute("error-phone-exist") != null){
                    %>
                    <span class="msg-error">Numbers Phone is exist !</span><br/>
                    <%
                        }
                    %>
                    <input type="email" placeholder="Email" name="txtEmail" value="${Email}" required/>
                    <%
                       if((String)request.getAttribute("error-email") != null){
                    %>
                    <span class="msg-error">Email is exist!</span><br/>
                    <%
                        }
                    %>
                    <input type="text" placeholder="Username" name="txtUsername" value="${Username}"  required/>
                    <%
                        if((String)request.getAttribute("error-username") != null){
                    %>
                    <span class="msg-error">Username is exist!</span><br/>
                    <%
                        }
                    %>
                    <input type="password" placeholder="Password" name="txtPassword" value="${Password}"  required/>
                    <input type="password" placeholder="Re-Password" name="txtRePassword" value="${RePassword}"  required/>
                    <%
                        if((String)request.getAttribute("error-Re-Password") != null){
                    %>
                    <span class="msg-error">Re-Password is wrong</span><br/>
                    <%
                        }
                    %>
                    <select id="level" name="txtLevel" value="${Level}" required>
                        <option value="1">Primary School</option>
                        <option value="2">Junior High School</option>
                        <option value="3">High School</option>
                    </select>
                    <select id="subject" name="txtSubject" value="${Subject}" required>
                        <option value="1">Math</option>
                        <option value="2">Literature</option>
                        <option value="3">English</option>
                        <option value="4">Physic</option>
                        <option value="5">Chemistry</option>
                        <option value="6">History</option>
                        <option value="7">Geography</option>
                        <option value="8">Civic education</option>
                    </select>
                        <div style="width: 100%">Import Image Profile<input required type="file" name="txtImageProfile" value="${ProfileImage}"></div>
                        <div style="width: 100%">Import CV<input required type="file" accept="application/pdf" value="${CVFile}" name="txtImageCV"></div>
                    <textarea  name="txtDescription" placeholder="Personal Description" rows="20" cols="82" ></textarea>
                    <input style="background-color: rgb(1,57,79); color: white; width: 50%; border-radius:5px" type="submit" value="Sign up" />
                    <a href="<%=path%>/signin.jsp">Sign in</a>
                </form>
            </div>
        </div>

        <div class="footer">
            <b>	Follow me on </b>
            <div class="icons">
                <a href="https://github.com/kvaibhav01" target="_blank" class="social"><i class="fab fa-github"></i></a>
                <a href="https://www.instagram.com/vaibhavkhulbe143/" target="_blank" class="social"><i class="fab fa-instagram"></i></a>
                <a href="https://medium.com/@vaibhavkhulbe" target="_blank" class="social"><i class="fab fa-medium"></i></a>
                <a href="https://twitter.com/vaibhav_khulbe" target="_blank" class="social"><i class="fab fa-twitter-square"></i></a>
                <a href="https://linkedin.com/in/vaibhav-khulbe/" target="_blank" class="social"><i class="fab fa-linkedin"></i></a>
            </div>
        </div>
    </body>
</html>
