drop database Tutor;
Create database Tutor;
Use Tutor;

Create table File(
	FileID int primary key auto_increment,
    FileData longblob
);

CREATE TABLE Parent (
	ParentID int primary key auto_increment,
	FullName varchar(255) not null,
    Gender varchar(1) not null,
    Address nvarchar(255) not null,
    BirthDate Date not null,
    Email Varchar(255) not null unique,
    PhoneNumber Varchar(10) not null unique,
    check (Gender like 'F' or Gender like 'M')
);

Create Table Student (
	StudentID int primary key auto_increment,
    ParentID int not null,
    FullName nvarchar(255) not null,
    Gender varchar(1) not null,
    Address nvarchar(255) not null,
    BirthDate Date not null,
    Email Varchar(255) unique,
    PhoneNumber Varchar(10) unique,
    check (Gender like 'F' or Gender like 'M'),
    foreign key(ParentID) references Parent(ParentID)
);

Create table Level (
	LevelID int primary key auto_increment,
    LevelName Nvarchar(255)
);

Create table Subject(
	SubjectID int primary key auto_increment,
    SubjectName NVarchar(255) not null
);

Create Table Tutor (
	TutorID int primary key auto_increment,
    FullName nvarchar(255) not null,
    Gender varchar(1) not null,
    Address nvarchar(255) not null,
    BirthDate Date not null,
    Email Varchar(255) unique,
    PhoneNumber Varchar(10) unique,
    ProfileImage int,
    CVFile int,
    Description longtext,
    SubjectID int not null,
    LevelID int not null,
    check (Gender like 'F' or Gender like 'M'),
    foreign key(ProfileImage) references File(FileID),
    foreign key(CVFile) references File(FileID),
    foreign key(SubjectID) references Subject(SubjectID),
    foreign key(LevelID) references Level(LevelID)
);

Create table Role(
	RoleID int primary key,
    RoleName varchar(255) not null
);

Create table Account(
	AccountID int primary key auto_increment,
    Username varchar(255) not null unique,
    Password varchar(255) not null,
    RoleID int not null,
    TutorID int,
    ParentID int,
    StudentID int,
    CreationDate Date,
    Active boolean,
    Foreign key(RoleID) references Role(RoleID),
    Foreign key(ParentID) references Parent(ParentID),
    Foreign key(StudentID) references Student(StudentID),
    Foreign key(TutorID) references Tutor(TutorID)
);

create table Payment(
	PaymentID int primary key auto_increment,
    AccountID int not null,
    StudentID int not null,
    PaymentDate Date not null,
    SubjectID int not null,
    TutorID int not null,
    NumberOfSlot int not null,
    Cofirmation boolean default false,
    foreign key(AccountID) references Account(AccountID),
    foreign key(StudentID) references Student(StudentID),
    foreign key(TutorID) references Tutor(TutorID)
);
create table Slot(
SlotID int primary key,
SlotName nvarchar(255)
);
create table Schedule(
	ScheduleID int primary key auto_increment,
    PaymentID int,
    StudentID int,
    TutorID int,
    Start datetime,
    NumberOfSlot int,
    SubjectID int,
    SlotID int,
    TimeOfSlot datetime,
    Location Nvarchar(255),
    Attachment longblob,
    Attend boolean default false,
    foreign key(PaymentID) references Payment(PaymentID),
    foreign key(StudentID) references Student(StudentID),
    foreign key(TutorID) references Tutor(TutorID),
    foreign key(SubjectID) references Subject(SubjectID),
    foreign key(SlotID) references Slot(SlotID)
);


create table Feedback (
	FeedbackID int primary key auto_increment,
    StudentID int,
    TutorID int,
    Rate int,
    Comment longtext,
    ScheduleID int,
    FeedbackRole int,
    foreign key(StudentID) references Student(StudentID),
    foreign key(TutorID) references Tutor(TutorID),
    foreign key(ScheduleID) references Schedule(ScheduleID),
    foreign key(FeedbackRole) references Role(RoleID)
);

create table Contact(
	ContactID int primary key auto_increment,
    GuestName Nvarchar(255),
    GuestEmail nvarchar(255),
    GuestSubject nvarchar(255),
    Message longtext,
    Date datetime
);

create table Gift(
	GiftID int primary key auto_increment,
    GiftImage int,
    Gift nvarchar(255),
    GiftDescription longtext,
    Point double,
     foreign key(GiftImage) references File(FileID)
);

create table RewardPoint(
	AccountID int primary key,
    Point double default 0,
    foreign key(AccountID) references Account(AccountID)
);

create table RewardExchange(
	RewardExchangeID int primary key auto_increment,
	AccountID int,
    GiftID int,
    ExchangeDate datetime,
    foreign key(AccountID) references Account(AccountID),
    foreign key(GiftID) references Gift(GiftID)
);

create table BlogCategory(
	CategoryID int primary key auto_increment,
    CategoryName nvarchar(255)
);

create table Blog(
	BlogID int primary key auto_increment,
    BlogTitle mediumtext,
    BlogDescription longtext,
    CategoryID int,
    Image int,
    foreign key(Image) references File(FileID),
    foreign key(CategoryID) references BlogCategory(CategoryID)
);

create table BlogPart(
	BlogPartID int primary key auto_increment,
	BlogID int,
    Part int,
    PartHeader tinytext,
    PartContent longtext,
    Image int,
     foreign key(Image) references File(FileID),
    foreign key(BlogID) references Blog(BlogID)
);


create table BlogDiscussion(
	CommentID int primary key auto_increment,
    BlogID int,
    AccountID int,
    Comment longtext,
    CommentDate datetime,
    foreign key(BlogID) references Blog(BlogID),
    foreign key(AccountID) references Account(AccountID)
);
insert into Slot
value(1,'Slot 1'),(2,'Slot 2'),(3,'Slot 3'),(4,'Slot 4'),(5,'Slot 5'),(6,'Slot 6');

insert into Role(RoleID, RoleName)
values(1, 'Admin'),(2, 'Tutor'), (3, 'Parent'), (4, 'Student');

insert into Level(LevelName)
values('Primary School'), ('Junior High School'), ('High School');

insert into Subject(SubjectName)
Value('Math'), 
('Literature'),
('English'), 
('Physics'),
('Chemistry'),
('History'), 
('Geography'),
('Civic education');

insert into BlogCategory(CategoryName)
values('Learning method'), ('Primary Education'), ('Junior High School'), ('High School');
















